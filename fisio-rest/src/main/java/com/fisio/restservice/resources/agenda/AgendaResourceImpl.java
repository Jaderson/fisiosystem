package com.fisio.restservice.resources.agenda;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.agenda.AgendaFacade;
import com.fisio.facade.solicitacaoAgenda.SolicitacaoAgendaFacade;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.json.JsonUtil;
import com.fisio.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.fisio.framework.util.DateFormatterUtil;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.empresa.Empresa;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.dataFalta.DataFaltaRepository;
import com.fisio.repository.horario.HorarioRepository;
import com.fisio.service.empresa.EmpresaService;
import com.fisio.service.especialidade.EspecialidadeService;
import com.fisio.service.pessoa.PessoaService;

@Component
@Path("/agendas")
@Transactional
@Produces(MediaType.APPLICATION_JSON)
public class AgendaResourceImpl {

	@Autowired AgendaFacade agendaFacade;
	@Autowired SolicitacaoAgendaFacade solicitacaoAgendaFacade;
	@Autowired PessoaService pessoaService;
	@Autowired EmpresaService empresaService;
	@Autowired EspecialidadeService especialidadeService;
	@Autowired HorarioRepository horarioRepository;
	@Autowired DataFaltaRepository dataFaltaRepository;
	
	@GET
	@Path("listar/{idEmpresa}/{idPessoa}")
	public Response listaAgendaPorPessoa(@PathParam("idEmpresa")Integer idEmpresa, @PathParam("idPessoa")Integer idPessoa) {
		Date dateMenosDezDias = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(dateMenosDezDias);

		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + 30);
		c.set(Calendar.MONTH, c.get(Calendar.MONTH));
		c.set(Calendar.YEAR, c.get(Calendar.YEAR));
		
		String dataInicio = new SimpleDateFormat("yyyy/MM/dd").format(new Date()); 
		Date date = DateFormatterUtil.formatarStringParaData(dataInicio, "yyyy/MM/dd");
		
		String dataFim = new SimpleDateFormat("yyyy/MM/dd").format(c.getTime());
		Date dateFim = DateFormatterUtil.formatarStringParaData(dataFim, "yyyy/MM/dd");
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", idEmpresa));
		listaRestricoes.add(Restricoes.igual("pessoa.id", idPessoa));
		listaRestricoes.add(Restricoes.between("dataInicio", date, dateFim));
		listaRestricoes.add(Restricoes.igual("cancelou", false));
	
		
		List<Agenda> listaPessoas = agendaFacade.consultarTodos(new Agenda(), listaRestricoes, null);
		String jsonObject = JsonUtil.convertListOfObjectsToJsonString(listaPessoas);
		return Response
				.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@GET
	@Path("listar/{idEmpresa}")
	public Response listaAgenda(@PathParam("idEmpresa")Integer idEmpresa) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", idEmpresa));
		
		List<Agenda> listaPessoas = agendaFacade.consultarTodos(new Agenda(), listaRestricoes, null);
		String jsonObject = JsonUtil.convertListOfObjectsToJsonString(listaPessoas);
		return Response
				.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@GET
	@Path("listar/horario/{idFuncionario}/{dataInicio}")
	public Response listarHorarioLivre(@PathParam("idFuncionario")Integer idFuncionario,
			@PathParam("dataInicio")String dataInicio){
		
		dataInicio = dataInicio.replace("-", "");
		dataInicio = dataInicio.substring(0, 2) + "/" + dataInicio.substring(2, 4) + "/"
					+ dataInicio.substring(4, 8);
		
		Date date = DateFormatterUtil.formatarStringParaData(dataInicio, "dd/MM/yyyy");
		
		List<Horario> listaHorarioDisponivel = horarioRepository.consultarHorarioPorData(idFuncionario, date);
		String jsonObject = JsonUtil.convertListOfObjectsToJsonString(listaHorarioDisponivel);
		return Response
				.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
		
	@GET
	@Path("cadastrar/{idPessoa}/{idEspecialidade}/{idFuncionario}/{data}/{idHorario}/{idEmpresa}")
	public Response cadastrarAgenda(@PathParam("idPessoa")Integer idPessoa,
			@PathParam("idEspecialidade")Integer idEspecialidade,
			@PathParam("idFuncionario")Integer idFuncionario,
			@PathParam("data")String dataInicio,
			@PathParam("idHorario")Integer idHorario,
			@PathParam("idEmpresa")Integer idEmpresa){
		
		
		Horario horario = this.horarioRepository.consultarPorId(idHorario);
		dataInicio = dataInicio.replace("-", "");
		dataInicio = dataInicio.substring(0, 2) + "/" + dataInicio.substring(2, 4) + "/"
				+ dataInicio.substring(4, 8);
		
		String dataFim = dataInicio;
		dataInicio = dataInicio + " " + horario.getHorarioInicio();
		dataFim = dataFim + " " + horario.getHorarioFim();
		
		Date dateInicio = DateFormatterUtil.formatarStringParaData(dataInicio, "dd/MM/yyyy HH:mm");
		Date dateFim = DateFormatterUtil.formatarStringParaData(dataFim, "dd/MM/yyyy HH:mm");
		Pessoa pessoa = this.pessoaService.consultarPorId(idPessoa);
		Pessoa funcionario = this.pessoaService.consultarPorId(idFuncionario);
		Empresa empresa = this.empresaService.consultarPorId(idEmpresa);
		Especialidade especialidade = this.especialidadeService.consultarPorId(idEspecialidade);
		
		
		Agenda entidade = new Agenda();
		entidade.setPessoa(pessoa);
		entidade.setEmpresa(empresa);
		entidade.setDataInicio(dateInicio);
		entidade.setDataFim(dateFim);
		entidade.setEspecialidade(especialidade);
		entidade.setHorario(horario);
		entidade.setFuncionario(funcionario);
		
		this.agendaFacade.cadastrar(entidade);
		
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		
		return Response.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@GET
	@Path("cadastrar/solicitacao/{idPessoa}/{idEspecialidade}/{idFuncionario}/{data}/{idHorario}/{idEmpresa}")
	public Response cadastrarSolicitacao(@PathParam("idPessoa")Integer idPessoa,
			@PathParam("idEspecialidade")Integer idEspecialidade,
			@PathParam("idFuncionario")Integer idFuncionario,
			@PathParam("data")String dataInicio,
			@PathParam("idHorario")Integer idHorario,
			@PathParam("idEmpresa")Integer idEmpresa){
		
		
		Horario horario = this.horarioRepository.consultarPorId(idHorario);
		dataInicio = dataInicio.replace("-", "");
		dataInicio = dataInicio.substring(0, 2) + "/" + dataInicio.substring(2, 4) + "/"
				+ dataInicio.substring(4, 8);
		
		String dataFim = dataInicio;
		dataInicio = dataInicio + " " + horario.getHorarioInicio();
		dataFim = dataFim + " " + horario.getHorarioFim();
		
		Date dateInicio = DateFormatterUtil.formatarStringParaData(dataInicio, "dd/MM/yyyy HH:mm");
		Date dateFim = DateFormatterUtil.formatarStringParaData(dataFim, "dd/MM/yyyy HH:mm");
		Pessoa pessoa = this.pessoaService.consultarPorId(idPessoa);
		Pessoa funcionario = this.pessoaService.consultarPorId(idFuncionario);
		Empresa empresa = this.empresaService.consultarPorId(idEmpresa);
		Especialidade especialidade = this.especialidadeService.consultarPorId(idEspecialidade);
		
		
		SolicitacaoAgenda entidade = new SolicitacaoAgenda();
		entidade.setPessoa(pessoa);
		entidade.setEmpresa(empresa);
		entidade.setDataInicio(dateInicio);
		entidade.setDataFim(dateFim);
		entidade.setEspecialidade(especialidade);
		entidade.setHorario(horario);
		entidade.setFuncionario(funcionario);
		
		this.solicitacaoAgendaFacade.cadastrar(entidade);
		
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		
		return Response.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	

	@GET
	@Path("listar/solicitacao/{idEmpresa}/{idPessoa}")
	public Response listaSolicitacaoPorPessoa(@PathParam("idEmpresa")Integer idEmpresa, @PathParam("idPessoa")Integer idPessoa) {
		Date dateMenosDezDias = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(dateMenosDezDias);

		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + 30);
		c.set(Calendar.MONTH, c.get(Calendar.MONTH));
		c.set(Calendar.YEAR, c.get(Calendar.YEAR));
		
		String dataInicio = new SimpleDateFormat("yyyy/MM/dd").format(new Date()); 
		Date date = DateFormatterUtil.formatarStringParaData(dataInicio, "yyyy/MM/dd");
		
		String dataFim = new SimpleDateFormat("yyyy/MM/dd").format(c.getTime());
		Date dateFim = DateFormatterUtil.formatarStringParaData(dataFim, "yyyy/MM/dd");
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", idEmpresa));
		listaRestricoes.add(Restricoes.igual("pessoa.id", idPessoa));
		listaRestricoes.add(Restricoes.between("dataInicio", date, dateFim));
		listaRestricoes.add(Restricoes.isNull("confirmado"));
	
		
		List<SolicitacaoAgenda> listaPessoas = solicitacaoAgendaFacade.consultarTodos(new SolicitacaoAgenda(), listaRestricoes, null);
		String jsonObject = JsonUtil.convertListOfObjectsToJsonString(listaPessoas);
		return Response
				.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@GET
	@Path("delete/solicitacao/{idSolicitacao}")
	public Response deleteSolicitacao(@PathParam("idSolicitacao")Integer idSolicitacao){
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", idSolicitacao));
		SolicitacaoAgenda entidade = this.solicitacaoAgendaFacade.consultarEntidade(new SolicitacaoAgenda(), listaRestricoes);
		if(VerificadorUtil.naoEstaNulo(entidade)){
			this.solicitacaoAgendaFacade.excluir(entidade);
		}
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return Response.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@GET
	@Path("delete/{idAgenda}")
	public Response deleteAgenda(@PathParam("idAgenda")Integer idAgenda){
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", new Long(idAgenda)));
		Agenda entidade = this.agendaFacade.consultarEntidade(new Agenda(), listaRestricoes);
		entidade.setCancelou(true);
		if(VerificadorUtil.naoEstaNulo(entidade)){
			this.agendaFacade.alterar(entidade);
		}
		String jsonObject = JsonUtil.convertObjectToJsonString(entidade);
		return Response.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
}
