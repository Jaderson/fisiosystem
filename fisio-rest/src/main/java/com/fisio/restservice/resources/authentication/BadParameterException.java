package com.fisio.restservice.resources.authentication;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ch.qos.logback.core.status.Status;


public class BadParameterException extends WebApplicationException {

	private static final long serialVersionUID = 1L;

	public BadParameterException(String message) {
        super(Response.status(Status.ERROR).entity(new ErrorItem(Response.Status.BAD_REQUEST.getStatusCode(), "bad_parameter", message)).type(MediaType.APPLICATION_JSON_TYPE).build());
    }
}
