package com.fisio.restservice.resources.especialidade;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.json.JsonUtil;
import com.fisio.service.especialidade.EspecialidadeService;

@Component
@Path("/especialidade")
@Transactional
@Produces(MediaType.APPLICATION_JSON)
public class EspecialidadeResourceImpl {

	@Autowired EspecialidadeService especialidadeService;
	
	@GET
	@Path("listar")
	public Response listaEspecialidade() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		
		List<Especialidade> listaPessoas = especialidadeService.consultarTodos(new Especialidade(), listaRestricoes, null);
		String jsonObject = JsonUtil.convertListOfObjectsToJsonString(listaPessoas);
		return Response
				.ok(jsonObject, MediaType.APPLICATION_JSON_TYPE)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@GET
	@Path("cadastrar/{descricao}")
	public Response cadastrarEspecialidade(@PathParam("descricao")String descricao) {
		Especialidade entidade = new Especialidade();
		entidade.setDescricao(descricao);
		this.especialidadeService.cadastrar(entidade);
		return Response.status(200)
				.header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	} 
}
