package com.fisio.restservice.resources.authentication;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fisio.framework.json.JsonUtil;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.autenticacao.UsuarioDTO;
import com.fisio.service.usuario.UsuarioService;

@Component
@Path("/auth")
@Transactional
@Produces(MediaType.APPLICATION_JSON)
public class AutenticacaoResourceImpl {

	@Autowired UsuarioService usuarioService;
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response login(@FormParam("username")String username, @FormParam("senha")String senha) throws JsonProcessingException {
		try {
			Usuario usuarioLogado = this.usuarioService.login(username, senha);
			UsuarioDTO usuarioDTO = new UsuarioDTO(usuarioLogado.getId(), usuarioLogado.getPessoa().getNome(), 
					usuarioLogado.getPessoa().getId(), usuarioLogado.getLogin(), 
					usuarioLogado.getTokenAcesso(), usuarioLogado.getAtivo(), 
					usuarioLogado.getEmpresa(), usuarioLogado.getAuthorities());
			String jsonUsuario = JsonUtil.convertObjectToJsonString(usuarioDTO);
			return Response.ok(jsonUsuario, MediaType.APPLICATION_JSON_TYPE)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
					.header("Access-Control-Allow-Credentials", "true")
					.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
					.header("Access-Control-Max-Age", "1209600")
					.build();
			
		} catch (Exception e) {
			Response response = Response.status(Response.Status.BAD_REQUEST.getStatusCode()).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
			throw new WebApplicationException(response);
		}
	}

}
