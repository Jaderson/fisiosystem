package com.fisio.restservice.application;

import org.glassfish.jersey.server.ResourceConfig;

import com.fisio.restservice.resources.agenda.AgendaResourceImpl;
import com.fisio.restservice.resources.authentication.AutenticacaoResourceImpl;
import com.fisio.restservice.resources.especialidade.EspecialidadeResourceImpl;
import com.fisio.restservice.resources.pessoa.PessoaResourceImpl;

public class FisioApplication extends ResourceConfig {

	public FisioApplication() {
		register(AutenticacaoResourceImpl.class);
		register(PessoaResourceImpl.class);
		register(AgendaResourceImpl.class);
		register(EspecialidadeResourceImpl.class);
		
		// packages("com.thramos.restservice.resources");
	}
}
