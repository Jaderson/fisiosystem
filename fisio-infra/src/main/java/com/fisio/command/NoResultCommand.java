package com.fisio.command;


public interface NoResultCommand {

	Object execute();
	
}
