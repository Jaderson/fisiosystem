package com.fisio.repository.especialidade.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.especialidade.Especialidade;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.especialidade.EspecialidadeRepository;

@Component
@SuppressWarnings("unchecked")
public class EspecialidadeRepositoryImpl extends AbstractRepository<Especialidade> implements EspecialidadeRepository{

}
