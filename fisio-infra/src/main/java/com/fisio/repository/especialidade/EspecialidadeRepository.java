package com.fisio.repository.especialidade;

import com.fisio.framework.especialidade.Especialidade;
import com.fisio.repository.Repository;

public interface EspecialidadeRepository extends Repository<Especialidade>{

}
