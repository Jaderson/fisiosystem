package com.fisio.repository.solicitacaoAgenda.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.solicitacaoAgenda.SolicitacaoAgendaRepository;

@Component
@SuppressWarnings("unchecked")
public class SolicitacaoAgendaRepositoryImpl extends AbstractRepository<SolicitacaoAgenda> implements SolicitacaoAgendaRepository{

}
