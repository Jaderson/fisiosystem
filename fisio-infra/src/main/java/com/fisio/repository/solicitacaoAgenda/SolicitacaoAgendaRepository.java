package com.fisio.repository.solicitacaoAgenda;

import com.fisio.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.fisio.repository.Repository;

public interface SolicitacaoAgendaRepository extends Repository<SolicitacaoAgenda>{

}
