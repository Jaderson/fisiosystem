package com.fisio.repository.categoria;

import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.repository.Repository;

public interface CategoriaRepository extends Repository<Categoria>{

}
