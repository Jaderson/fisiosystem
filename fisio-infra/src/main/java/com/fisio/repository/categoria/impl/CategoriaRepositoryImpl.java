package com.fisio.repository.categoria.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.categoria.CategoriaRepository;

@Component
@SuppressWarnings("unchecked")
public class CategoriaRepositoryImpl extends AbstractRepository<Categoria> implements CategoriaRepository{

}
