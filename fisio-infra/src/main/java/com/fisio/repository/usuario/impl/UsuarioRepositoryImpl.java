package com.fisio.repository.usuario.impl;

import org.springframework.stereotype.Component;

import com.fisio.command.CommandRepositoryExecuter;
import com.fisio.command.NoResultCommand;
import com.fisio.framework.consulta.ConsultaJPA;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.usuario.UsuarioRepository;

@Component
public class UsuarioRepositoryImpl extends AbstractRepository<Usuario>
		implements UsuarioRepository {

	@Override
	public Usuario consultarUsuarioPorLogin(final String username) {
		return (Usuario) CommandRepositoryExecuter
				.executar(new NoResultCommand() {

					@Override
					public Object execute() {
						return new ConsultaJPA<Usuario>(getEntityManager(),
								new Usuario().getClass().getSimpleName())
								.addParametroQuery(
										Restricoes.igual("login", username))
								.singleResult();
					}
				});
	}

}
