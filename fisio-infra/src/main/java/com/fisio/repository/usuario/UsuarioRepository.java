package com.fisio.repository.usuario;

import com.fisio.model.autenticacao.Usuario;
import com.fisio.repository.Repository;

public interface UsuarioRepository extends Repository<Usuario>{

	Usuario consultarUsuarioPorLogin(String username);
	
}
