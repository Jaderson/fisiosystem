package com.fisio.repository.acompanhamentoFilho;

import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.repository.Repository;

public interface AcompanhamentoFilhoRepository extends Repository<AcompanhamentoFilho>{

}
