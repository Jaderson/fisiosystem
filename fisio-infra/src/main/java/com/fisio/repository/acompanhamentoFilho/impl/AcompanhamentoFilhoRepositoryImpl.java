package com.fisio.repository.acompanhamentoFilho.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.acompanhamentoFilho.AcompanhamentoFilhoRepository;

@Component
@SuppressWarnings("unchecked")
public class AcompanhamentoFilhoRepositoryImpl extends AbstractRepository<AcompanhamentoFilho> implements AcompanhamentoFilhoRepository{

}
