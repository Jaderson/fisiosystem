package com.fisio.repository.empresa;

import com.fisio.model.empresa.Empresa;
import com.fisio.repository.Repository;

public interface EmpresaRepository extends Repository<Empresa>{

}
