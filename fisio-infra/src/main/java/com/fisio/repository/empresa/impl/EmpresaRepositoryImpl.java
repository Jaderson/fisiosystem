package com.fisio.repository.empresa.impl;

import org.springframework.stereotype.Component;

import com.fisio.model.empresa.Empresa;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.empresa.EmpresaRepository;

@Component
@SuppressWarnings("unchecked")
public class EmpresaRepositoryImpl extends AbstractRepository<Empresa> implements EmpresaRepository{

}
