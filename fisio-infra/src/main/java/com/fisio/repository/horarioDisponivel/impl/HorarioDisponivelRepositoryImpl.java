package com.fisio.repository.horarioDisponivel.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.horarioDisponivel.HorarioDisponivelRepository;

@Component
@SuppressWarnings("unchecked")
public class HorarioDisponivelRepositoryImpl extends AbstractRepository<HorarioDisponivel> implements HorarioDisponivelRepository{

}
