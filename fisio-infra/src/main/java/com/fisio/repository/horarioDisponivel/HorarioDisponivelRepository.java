package com.fisio.repository.horarioDisponivel;

import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.repository.Repository;

public interface HorarioDisponivelRepository extends Repository<HorarioDisponivel>{

}
