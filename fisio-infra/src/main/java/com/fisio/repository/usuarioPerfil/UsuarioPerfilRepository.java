package com.fisio.repository.usuarioPerfil;

import java.util.List;

import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.Repository;

public interface UsuarioPerfilRepository extends Repository<UsuarioPerfil> {
	
	List<UsuarioPerfil> consultarPerfilPorUsuario(Pessoa entidade);

}
