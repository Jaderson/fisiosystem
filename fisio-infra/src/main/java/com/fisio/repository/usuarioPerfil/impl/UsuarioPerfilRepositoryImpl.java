package com.fisio.repository.usuarioPerfil.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.usuarioPerfil.UsuarioPerfilRepository;

@Component
@SuppressWarnings("unchecked")
public class UsuarioPerfilRepositoryImpl extends AbstractRepository<UsuarioPerfil> implements UsuarioPerfilRepository{

	@Override
	public List<UsuarioPerfil> consultarPerfilPorUsuario(Pessoa entidade) {
		return this.getEntityManager().createQuery("select up from UsuarioPerfil up "
				+ " inner join up.perfil p"
				+ " inner join up.usuario u"
				+ " inner join u.pessoa pe "
				+ " where  pe.id = :pessoaId "
				+ " and p.id = up.perfil.id "
				+ " and pe.id = u.pessoa.id "
				+ " and u.id = up.usuario.id "
				+ " and u.ativo = 1 ")
				.setParameter("pessoaId", entidade.getId())
				.getResultList();
	}
	

}

