package com.fisio.repository.fluxoCaixa;

import java.math.BigDecimal;
import java.util.Date;

import com.fisio.framework.caixa.FluxoCaixa;
import com.fisio.framework.caixa.TipoLancamento;
import com.fisio.repository.Repository;

public interface FluxoCaixaRepository extends Repository<FluxoCaixa>{

	BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia,Date geraDataComUltimoDia, Integer idEmpresa, TipoLancamento tipoLancamento);

}
