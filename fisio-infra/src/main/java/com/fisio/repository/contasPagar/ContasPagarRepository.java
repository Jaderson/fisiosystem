package com.fisio.repository.contasPagar;

import com.fisio.framework.contasPagar.ContasPagar;
import com.fisio.repository.Repository;

public interface ContasPagarRepository extends Repository<ContasPagar>{

}
