package com.fisio.repository.contasPagar.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.contasPagar.ContasPagar;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.contasPagar.ContasPagarRepository;

@Component
@SuppressWarnings("unchecked")
public class ContasPagarRepositoryImpl extends AbstractRepository<ContasPagar> implements ContasPagarRepository{

}
