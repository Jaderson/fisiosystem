package com.fisio.repository.biomecanicaPalmilha;

import com.fisio.framework.avaliacoes.BiomecanicaPalmilha;
import com.fisio.repository.Repository;

public interface BiomecanicaPalmilhaRepository extends Repository<BiomecanicaPalmilha>{

}
