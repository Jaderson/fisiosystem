package com.fisio.repository.biomecanicaPalmilha.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.avaliacoes.BiomecanicaPalmilha;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.biomecanicaPalmilha.BiomecanicaPalmilhaRepository;

@Component
@SuppressWarnings("unchecked")
public class BiomecanicaPalmilhaRepositoryImpl extends AbstractRepository<BiomecanicaPalmilha> implements BiomecanicaPalmilhaRepository{

}
