package com.fisio.repository.configuracao;

import java.util.List;

import com.fisio.framework.configuracao.Configuracao;
import com.fisio.repository.Repository;

public interface ConfiguracaoRepository extends Repository<Configuracao>{

	List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int idSemana, Integer idEmpresa);

}
