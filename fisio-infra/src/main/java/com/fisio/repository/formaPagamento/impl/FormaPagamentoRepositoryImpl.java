package com.fisio.repository.formaPagamento.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.formaPagamento.FormaPagamentoRepository;

@Component
@SuppressWarnings("unchecked")
public class FormaPagamentoRepositoryImpl extends AbstractRepository<FormaPagamento> implements FormaPagamentoRepository{

}
