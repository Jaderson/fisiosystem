package com.fisio.repository.formaPagamento;

import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.repository.Repository;

public interface FormaPagamentoRepository extends Repository<FormaPagamento>{

}
