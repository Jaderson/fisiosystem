package com.fisio.repository.agenda;

import java.util.Date;
import java.util.List;

import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.repository.Repository;

public interface AgendaRepository extends Repository<Agenda>{

	List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente);
	
	void consultarSeExisteEspecialidade(Especialidade entidade);
	
	List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresa);
}
