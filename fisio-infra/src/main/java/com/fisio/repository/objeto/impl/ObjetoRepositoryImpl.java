package com.fisio.repository.objeto.impl;

import org.springframework.stereotype.Component;

import com.fisio.model.autenticacao.Objeto;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.objeto.ObjetoRepository;

@Component
@SuppressWarnings("unchecked")
public class ObjetoRepositoryImpl extends AbstractRepository<Objeto> implements ObjetoRepository{

}
