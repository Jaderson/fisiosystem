package com.fisio.repository.objeto;

import com.fisio.model.autenticacao.Objeto;
import com.fisio.repository.Repository;

public interface ObjetoRepository extends Repository<Objeto>{

}
