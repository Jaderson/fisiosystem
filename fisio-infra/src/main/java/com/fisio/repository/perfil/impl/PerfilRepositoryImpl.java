package com.fisio.repository.perfil.impl;

import org.springframework.stereotype.Component;

import com.fisio.model.autenticacao.Perfil;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.perfil.PerfilRepository;

@Component
@SuppressWarnings("unchecked")
public class PerfilRepositoryImpl extends AbstractRepository<Perfil> implements PerfilRepository{

}
