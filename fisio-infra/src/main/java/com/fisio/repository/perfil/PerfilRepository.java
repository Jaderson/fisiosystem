package com.fisio.repository.perfil;

import com.fisio.model.autenticacao.Perfil;
import com.fisio.repository.Repository;

public interface PerfilRepository extends Repository<Perfil>{

}
