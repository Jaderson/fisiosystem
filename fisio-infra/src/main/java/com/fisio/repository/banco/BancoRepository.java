package com.fisio.repository.banco;

import com.fisio.framework.banco.Banco;
import com.fisio.repository.Repository;

public interface BancoRepository extends Repository<Banco>{

}
