package com.fisio.repository.banco.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.banco.Banco;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.banco.BancoRepository;

@Component
@SuppressWarnings("unchecked")
public class BancoRepositoryImpl extends AbstractRepository<Banco> implements BancoRepository{

}
