package com.fisio.repository.dataFalta.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fisio.framework.agenda.DataFalta;
import com.fisio.framework.configuracao.Configuracao;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.dataFalta.DataFaltaRepository;

@Component
@SuppressWarnings("unchecked")
public class DataFaltaRepositoryImpl extends AbstractRepository<DataFalta> implements DataFaltaRepository{
	
	@Override
	public List<DataFalta> consultarDatasPorConfiguracao(Configuracao entidade) {
		return getEntityManager().createQuery(" select d from DataFalta d "
	            + " inner join d.configuracao c "
	            + " where d.configuracao.id = c.id"
	            + " and c.id = :idConfiguracao "
	            + " and d.id not in (select hr.dataFalta.id from HorarioDisponivel hr)")
                .setParameter("idConfiguracao", entidade.getId())
                .getResultList();
	}

	@Override
	public List<DataFalta> consultarDatasPorConfiguracaoComHorario(Configuracao entidade) {
		return getEntityManager().createQuery(" select d from DataFalta d "
	            + " inner join d.configuracao c "
	            + " where d.configuracao.id = c.id"
	            + " and c.id = :idConfiguracao "
	            + " and d.id in (select hr.dataFalta.id from HorarioDisponivel hr)"
	            + " order by d.dataFalta desc")
                .setParameter("idConfiguracao", entidade.getId())
                .getResultList();
	}

}
