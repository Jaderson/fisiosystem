package com.fisio.repository.dataFalta;

import java.util.List;

import com.fisio.framework.agenda.DataFalta;
import com.fisio.framework.configuracao.Configuracao;
import com.fisio.repository.Repository;

public interface DataFaltaRepository extends Repository<DataFalta>{

	List<DataFalta> consultarDatasPorConfiguracao(Configuracao entidade);
	
	List<DataFalta> consultarDatasPorConfiguracaoComHorario(Configuracao entidade);

}
