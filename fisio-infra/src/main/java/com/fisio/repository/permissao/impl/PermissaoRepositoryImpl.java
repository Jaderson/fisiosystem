package com.fisio.repository.permissao.impl;

import org.springframework.stereotype.Component;

import com.fisio.model.autenticacao.Permissao;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.permissao.PermissaoRepository;

@Component
@SuppressWarnings("unchecked")
public class PermissaoRepositoryImpl extends AbstractRepository<Permissao> implements PermissaoRepository{

}
