package com.fisio.repository.permissao;

import com.fisio.model.autenticacao.Permissao;
import com.fisio.repository.Repository;

public interface PermissaoRepository extends Repository<Permissao>{

}
