package com.fisio.repository.perfilPermissao;

import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.repository.Repository;

public interface PerfilPermissaoRepository extends Repository<PerfilPermissao> {

	PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(Perfil perfil,
			Permissao permissao);
}
