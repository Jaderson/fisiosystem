package com.fisio.repository.perfilPermissao.impl;

import org.springframework.stereotype.Component;

import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.perfilPermissao.PerfilPermissaoRepository;

@Component
@SuppressWarnings("unchecked")
public class PerfilPermissaoRepositoryImpl extends
		AbstractRepository<PerfilPermissao> implements
		PerfilPermissaoRepository {

	@Override
	public PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(
			Perfil perfil, Permissao permissao) {
		return (PerfilPermissao) getEntityManager()
				.createQuery(
						"select pf from PerfilPermissao pf where pf.perfil.id = :perfil "
								+ "and pf.permissao.id = :permissao")
				.setParameter("perfil", perfil.getId())
				.setParameter("permissao", permissao.getId()).getSingleResult();
	}

}
