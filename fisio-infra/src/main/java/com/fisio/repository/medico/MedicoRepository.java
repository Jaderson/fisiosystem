package com.fisio.repository.medico;

import com.fisio.framework.medico.Medico;
import com.fisio.repository.Repository;

public interface MedicoRepository extends Repository<Medico>{

}
