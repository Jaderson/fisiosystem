package com.fisio.repository.medico.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.medico.Medico;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.medico.MedicoRepository;

@Component
@SuppressWarnings("unchecked")
public class MedicoRepositoryImpl extends AbstractRepository<Medico> implements MedicoRepository{

}
