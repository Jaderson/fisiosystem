package com.fisio.repository.acao.impl;

import org.springframework.stereotype.Component;

import com.fisio.model.autenticacao.Acao;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.acao.AcaoRepository;

@Component
@SuppressWarnings("unchecked")
public class AcaoRepositoryImpl extends AbstractRepository<Acao> implements AcaoRepository{

}
