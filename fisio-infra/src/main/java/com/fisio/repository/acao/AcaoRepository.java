package com.fisio.repository.acao;

import com.fisio.model.autenticacao.Acao;
import com.fisio.repository.Repository;

public interface AcaoRepository extends Repository<Acao>{

}
