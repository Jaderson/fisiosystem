package com.fisio.repository.biomecanica;

import com.fisio.framework.avaliacoes.Biomecanica;
import com.fisio.repository.Repository;

public interface BiomecanicaRepository extends Repository<Biomecanica>{

}
