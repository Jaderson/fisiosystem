package com.fisio.repository.biomecanica.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.avaliacoes.Biomecanica;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.biomecanica.BiomecanicaRepository;

@Component
@SuppressWarnings("unchecked")
public class BiomecanicaRepositoryImpl extends AbstractRepository<Biomecanica> implements BiomecanicaRepository{

}
