package com.fisio.repository.palmilha.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.avaliacoes.Palmilha;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.palmilha.PalmilhaRepository;

@Component
@SuppressWarnings("unchecked")
public class PalmilhaRepositoryImpl extends AbstractRepository<Palmilha> implements PalmilhaRepository{

}
