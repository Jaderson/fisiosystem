package com.fisio.repository.fluxoBancario;

import com.fisio.framework.fluxoBancario.FluxoBancario;
import com.fisio.repository.Repository;

public interface FluxoBancarioRepository extends Repository<FluxoBancario>{

}
