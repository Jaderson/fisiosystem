package com.fisio.repository.fluxoBancario.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.fluxoBancario.FluxoBancario;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.fluxoBancario.FluxoBancarioRepository;

@Component
@SuppressWarnings("unchecked")
public class FluxoBancarioRepositoryImpl extends AbstractRepository<FluxoBancario> implements FluxoBancarioRepository{

}
