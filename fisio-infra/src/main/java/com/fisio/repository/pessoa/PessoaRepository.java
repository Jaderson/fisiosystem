package com.fisio.repository.pessoa;

import java.util.List;

import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.Repository;

public interface PessoaRepository extends Repository<Pessoa>{

	List<Pessoa> consultarAniversariante();
}
