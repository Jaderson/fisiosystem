package com.fisio.repository.pessoa.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.pessoa.PessoaRepository;

@Component
@SuppressWarnings("unchecked")
public class PessoaRepositoryImpl extends AbstractRepository<Pessoa> implements PessoaRepository{

	@Override
	public List<Pessoa> consultarAniversariante() {
		return this.getEntityManager().createQuery("SELECT p FROM Pessoa p "
				+ " WHERE MONTH(p.dataNascimento) = MONTH(CURDATE())"
				+ " AND DAY(p.dataNascimento) = DAY(CURDATE()) ")
				.getResultList();
	}

}
