package com.fisio.repository.acompanhamento;

import com.fisio.framework.acompanhamento.AcompanhamentoPessoa;
import com.fisio.repository.Repository;

public interface AcompanhamentoRepository extends Repository<AcompanhamentoPessoa>{

}
