package com.fisio.repository.acompanhamento.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.acompanhamento.AcompanhamentoPessoa;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.acompanhamento.AcompanhamentoRepository;

@Component
@SuppressWarnings("unchecked")
public class AcompanhamentoRepositoryImpl extends AbstractRepository<AcompanhamentoPessoa> implements AcompanhamentoRepository{

}
