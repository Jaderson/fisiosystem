package com.fisio.repository.horario;

import java.util.Date;
import java.util.List;

import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.repository.Repository;

public interface HorarioRepository extends Repository<Horario>{

	List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade);
	
	List<Horario> consultarHorarioPorData(Integer idFuncionario, Date dataInicio);
}
