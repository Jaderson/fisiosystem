package com.fisio.repository.profissao;

import com.fisio.framework.profissao.Profissao;
import com.fisio.repository.Repository;

public interface ProfissaoRepository extends Repository<Profissao>{

}
