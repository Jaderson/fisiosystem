package com.fisio.repository.profissao.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.profissao.Profissao;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.profissao.ProfissaoRepository;

@Component
@SuppressWarnings("unchecked")
public class ProfissaoRepositoryImpl extends AbstractRepository<Profissao> implements ProfissaoRepository{

}
