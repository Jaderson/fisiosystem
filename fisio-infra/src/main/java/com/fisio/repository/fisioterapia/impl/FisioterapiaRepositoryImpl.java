package com.fisio.repository.fisioterapia.impl;

import org.springframework.stereotype.Component;

import com.fisio.framework.avaliacoes.Fisioterapia;
import com.fisio.repository.AbstractRepository;
import com.fisio.repository.fisioterapia.FisioterapiaRepository;

@Component
@SuppressWarnings("unchecked")
public class FisioterapiaRepositoryImpl extends AbstractRepository<Fisioterapia> implements FisioterapiaRepository{

}
