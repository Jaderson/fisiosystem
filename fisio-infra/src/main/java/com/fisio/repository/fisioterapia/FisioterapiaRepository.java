package com.fisio.repository.fisioterapia;

import com.fisio.framework.avaliacoes.Fisioterapia;
import com.fisio.repository.Repository;

public interface FisioterapiaRepository extends Repository<Fisioterapia>{

}
