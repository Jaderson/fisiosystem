package com.fisio.email;

import com.fisio.model.autenticacao.Usuario;

public interface EnviadorEmail {

	void enviarEmail(Object... parameters);
	
	void enviarEmailNovaEmpresa(Usuario usuarioAdminNovaEmpresa, Usuario usuarioLogado, String senha);
	
	void enviarNovaSenha(Object... parameters);
}
