package com.fisio.converter.empresa;

import com.fisio.model.empresa.Empresa;
import com.fisio.web.framework.converter.ConversorGeneric;

public class EmpresaConversor extends ConversorGeneric{

	protected static final String key = "jsf.EntityConverter.Empresa";
	
	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if(value == null) {
			return EMPTY;
		}
		return ((Empresa) value).getId().toString();
	}

}
