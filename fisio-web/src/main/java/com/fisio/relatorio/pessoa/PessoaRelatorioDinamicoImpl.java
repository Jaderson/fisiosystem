package com.fisio.relatorio.pessoa;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.relatorio.GeradorRelatorioDinamicoImpl;
import com.fisio.repository.pessoa.PessoaRepository;

@Component
public class PessoaRelatorioDinamicoImpl extends GeradorRelatorioDinamicoImpl<Pessoa> implements PessoaRelatorioDinamico {
	
	private PessoaRepository pessoaRepository;

	@Autowired
	public PessoaRelatorioDinamicoImpl(PessoaRepository pessoaRepository) {
		this.pessoaRepository = pessoaRepository;
	}
	
	@Override
	protected List<Pessoa> popularLista() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuario().getEmpresa().getId()));
		return this.pessoaRepository.consultarTodos(new Pessoa(), listaRestricoes, "nome");
	}

	@Override
	protected String getTitulo() {
		return "Paciente";
	}
}
