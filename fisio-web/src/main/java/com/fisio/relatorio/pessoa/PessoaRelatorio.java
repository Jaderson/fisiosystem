package com.fisio.relatorio.pessoa;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.swing.ImageIcon;

import net.sf.jasperreports.engine.JRException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.facade.profissao.ProfissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.profissao.Profissao;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.Sexo;
import com.fisio.relatorio.GeradorRelatorioParametrosImpl;
import com.fisio.web.converter.profissao.ProfissaoConversor;
import com.fisio.web.framework.combomodel.ItemCombo;

@Controller
@Component
@Scope("view")
public class PessoaRelatorio extends GeradorRelatorioParametrosImpl<Pessoa> {

	private PessoaFacade pessoaFacade;
	private ProfissaoConversor profissoesConversor;
	private ProfissaoFacade profissaoFacade;

	@Autowired
	public PessoaRelatorio(PessoaFacade pessoaFacade,
			ProfissaoFacade profissaoFacade) {
		this.pessoaFacade = pessoaFacade;
		this.profissaoFacade = profissaoFacade;
		this.profissoesConversor = new ProfissaoConversor();
	}

	public void gerarRelatorio(Pessoa entidade) throws JRException,
			IOException, SQLException {
		ImageIcon gto = new ImageIcon(getClass().getResource("/relatorios/LOGO.jpg"));
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		URL resource = getClass().getResource("/relatorios/pessoaRelatorio.jasper");
		parametros.put("logo", gto.getImage());
		parametros.put("p_nome", entidade.getNome().equals("") ? null : entidade.getNome());
		parametros.put("p_sexo", entidade.getSexo() == null ? null : entidade.getSexo().toString());
		parametros.put("p_data_nascimento",	entidade.getDataNascimento() == null ? null : getDataFormatada(
						entidade.getDataNascimento(), "yyyy/MM/dd"));
		parametros.put("p_profissao", entidade.getProfissao() == null ? null : entidade.getProfissao().getId().toString());
		PDF(parametros, resource.getPath());
	}

	public List<String> completeText(String query) {
		List<String> listaPessoa = new ArrayList<String>();
		Pessoa pessoaConsulta = new Pessoa();

		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.like("nome", query));
		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes,pessoaConsulta.getCampoOrderBy());
		for (Pessoa pessoa : listaPessoas) {
			listaPessoa.add(pessoa.getNome());
		}
		return listaPessoa;
	}

	public List<ItemCombo> getListaProfissoes() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Profissao profissoesConsulta = new Profissao();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();

		List<Profissao> listaProfissoes = this.profissaoFacade.consultarTodos(
				profissoesConsulta, listaRestricoes,
				profissoesConsulta.getCampoOrderBy());
		for (Profissao profissoes : listaProfissoes) {
			listaItemCombo.add(new ItemCombo(profissoes.getDescricao(),
					profissoes));
		}
		return listaItemCombo;
	}

	public SelectItem[] getSexoValues() {
		int i = 0;
		SelectItem[] items = new SelectItem[Sexo.values().length];
		for (Sexo sexo : Sexo.values()) {
			items[i++] = new SelectItem(sexo, sexo.getDescricao());
		}
		return items;
	}

	@Override
	public Pessoa getEntidade() {
		if (VerificadorUtil.estaNulo(entidade)) {
			entidade = new Pessoa();
		}
		return entidade;
	}

	public ProfissaoConversor getProfissoesConversor() {
		return profissoesConversor;
	}

	public void setProfissoesConversor(ProfissaoConversor profissoesConversor) {
		this.profissoesConversor = profissoesConversor;
	}
}
