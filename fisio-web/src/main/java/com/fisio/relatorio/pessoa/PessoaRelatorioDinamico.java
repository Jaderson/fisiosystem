package com.fisio.relatorio.pessoa;

import com.fisio.model.pessoa.Pessoa;
import com.fisio.relatorio.GeradorRelatorioDinamico;

public interface PessoaRelatorioDinamico extends GeradorRelatorioDinamico<Pessoa>{

}
