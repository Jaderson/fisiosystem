package com.fisio.relatorio.fluxoCaixa;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.swing.ImageIcon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.fisio.facade.especialidade.EspecialidadeFacade;
import com.fisio.facade.formaPagamento.FormaPagamentoFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.caixa.FluxoCaixa;
import com.fisio.framework.caixa.TipoLancamento;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.relatorio.GeradorRelatorioParametrosImpl;
import com.fisio.web.converter.especialidade.EspecialidadeConversor;
import com.fisio.web.converter.formaPagamento.FormaPagamentoConversor;
import com.fisio.web.framework.combomodel.ItemCombo;

import net.sf.jasperreports.engine.JRException;

@Controller
@Component
@Scope("view")
public class FluxoCaixaRelatorio extends GeradorRelatorioParametrosImpl<FluxoCaixa> {

	private PessoaFacade pessoaFacade;
	private EspecialidadeConversor especialidadeConversor;
	private EspecialidadeFacade especialidadeFacade;
	private FormaPagamentoFacade formaPagamentoFacade;
	private FormaPagamentoConversor formaPagamentoConversor;
	private Date primeiraData;
	private Date segundaData;
	
	@Autowired
	public FluxoCaixaRelatorio(PessoaFacade pessoaFacade, EspecialidadeFacade especialidadeFacade,
			FormaPagamentoFacade formaPagamentoFacade){
		this.pessoaFacade = pessoaFacade;
		this.especialidadeFacade = especialidadeFacade;
		this.formaPagamentoFacade = formaPagamentoFacade;
		this.especialidadeConversor = new EspecialidadeConversor();
		this.formaPagamentoConversor =  new FormaPagamentoConversor();
	}
	
	public void gerarRelatorio(FluxoCaixa entidade) throws JRException, IOException, SQLException{
		ImageIcon gto = new ImageIcon(getClass().getResource("/relatorios/LOGO.jpg"));
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		URL resource;
		if(getEntidade().getTipoLancamento().equals(TipoLancamento.Despesa)){
			resource = getClass().getResource("/relatorios/fluxoCaixaDespesaRelatorio.jasper");
		}else{
			resource = getClass().getResource("/relatorios/fluxoCaixaRelatorio.jasper");
		}
		
		parametros.put("logo", gto.getImage());
		parametros.put("p_dataInicio",getDataFormatada(getPrimeiraData() == null ? new Date("2016/01/01") : getPrimeiraData(), "yyyy/MM/dd"));
		parametros.put("p_dataFim",	getDataFormatada(getSegundaData() == null ? new Date() : getSegundaData(), "yyyy/MM/dd"));
		parametros.put("p_idFormaPagamento",getEntidade().getFormaPagamento() == null ? null : getEntidade().getFormaPagamento().getId().toString());
		parametros.put("p_idPaciente", getEntidade().getPessoa() == null ? null : getEntidade().getPessoa().getId().toString());
		parametros.put("p_tipoLancamento",getEntidade().getTipoLancamento().toString());
		parametros.put("p_idEspecialidade", getEntidade().getEspecialidade() == null ? null : getEntidade().getEspecialidade().getId().toString());
		parametros.put("p_numeroRecibo", getEntidade().getNumeroRecibo().toString().equals("") ? null : getEntidade().getNumeroRecibo().toString());
		parametros.put("p_empresa", getUsuarioLogado().getEmpresa().getId());
		
		PDF(parametros, resource.getPath());
	}
	
	public List<Pessoa> completeText(String query) {
		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		Pessoa pessoaConsulta = new Pessoa();

		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.like("nome", query));
		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		for (Pessoa pessoa : listaPessoas) {
			listaPessoa.add(pessoa);
		}
		return listaPessoas;
	}
	
	@Override
	public FluxoCaixa getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new FluxoCaixa();
		}
		return entidade;
	}

	public SelectItem[] getTipoLancamentoValues() {
		int i = 0;
		SelectItem[] items = new SelectItem[TipoLancamento.values().length];
		for(TipoLancamento tipoLancamento : TipoLancamento.values()) {
			items[i++] = new SelectItem(tipoLancamento, tipoLancamento.getValue());
		}
		
		return items;
	}
	
	public List<ItemCombo> getListaFormaPagamento() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		FormaPagamento formaPagamentoConsulta = new FormaPagamento();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();

		List<FormaPagamento> listaFormaPagamento = this.formaPagamentoFacade.consultarTodos(formaPagamentoConsulta, listaRestricoes, formaPagamentoConsulta.getCampoOrderBy());
		for (FormaPagamento formaPagamento : listaFormaPagamento) {
				listaItemCombo.add(new ItemCombo(formaPagamento.getDescricao(), formaPagamento));
		}
		return listaItemCombo;
	}
	
	public List<ItemCombo> getListaEspecialidade() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Especialidade especialidadeConsulta = new Especialidade();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		List<Especialidade> listaEspecialidade = this.especialidadeFacade.consultarTodos(especialidadeConsulta, listaRestricoes, especialidadeConsulta.getCampoOrderBy());
		for (Especialidade especialidade : listaEspecialidade) {
				listaItemCombo.add(new ItemCombo(especialidade.getDescricao(), especialidade));
		}
		return listaItemCombo;
	}
	
	public EspecialidadeConversor getEspecialidadeConversor() {
		return especialidadeConversor;
	}
	
	public void setEspecialidadeConversor(
			EspecialidadeConversor especialidadeConversor) {
		this.especialidadeConversor = especialidadeConversor;
	}
	
	public Date getPrimeiraData() {
		return primeiraData;
	}
	
	public void setPrimeiraData(Date primeiraData) {
		this.primeiraData = primeiraData;
	}
	
	public Date getSegundaData() {
		return segundaData;
	}
	
	public void setSegundaData(Date segundaData) {
		this.segundaData = segundaData;
	}
	
	public FormaPagamentoConversor getFormaPagamentoConversor() {
		return formaPagamentoConversor;
	}
	
	public void setFormaPagamentoConversor(
			FormaPagamentoConversor formaPagamentoConversor) {
		this.formaPagamentoConversor = formaPagamentoConversor;
	}
}
