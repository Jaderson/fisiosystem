package com.fisio.relatorio.faltas;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.swing.ImageIcon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.caixa.TipoLancamento;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.relatorio.GeradorRelatorioParametrosImpl;

import net.sf.jasperreports.engine.JRException;

@Controller
@Component
@Scope("view")
public class faltasRelatorio extends GeradorRelatorioParametrosImpl<Agenda> {

	private PessoaFacade pessoaFacade;
	private Date primeiraData;
	private Date segundaData;
	
	@Autowired
	public faltasRelatorio(PessoaFacade pessoaFacade){
		this.pessoaFacade = pessoaFacade;
	}
	
	public void gerarRelatorio(Agenda entidade) throws JRException, IOException, SQLException{
		ImageIcon gto = new ImageIcon(getClass().getResource("/relatorios/LOGO.jpg"));
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		URL resource;
		resource = getClass().getResource("/relatorios/relatoriofaltas.jasper");
		
		parametros.put("logo", gto.getImage()); 
		parametros.put("p_dataInicio",getDataFormatada(getPrimeiraData() == null ? new Date("2016/01/01") : getPrimeiraData(), "yyyy/MM/dd"));
		parametros.put("p_dataFim",getDataFormatada(getSegundaData() == null ? new Date() : getSegundaData(), "yyyy/MM/dd"));
		parametros.put("p_idPaciente", getEntidade().getPessoa() == null ? null : getEntidade().getPessoa().getId().toString()); 
		parametros.put("p_idEmpresa", getUsuarioLogado().getEmpresa().getId());
		
		PDF(parametros, resource.getPath());
	}
	
	public List<Pessoa> completeText(String query) {
		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		Pessoa pessoaConsulta = new Pessoa();

		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.like("nome", query));
		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		for (Pessoa pessoa : listaPessoas) {
			listaPessoa.add(pessoa);
		}
		return listaPessoas;
	}
	
	@Override
	public Agenda getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Agenda();
		}
		return entidade;
	}

	public SelectItem[] getTipoLancamentoValues() {
		int i = 0;
		SelectItem[] items = new SelectItem[TipoLancamento.values().length];
		for(TipoLancamento tipoLancamento : TipoLancamento.values()) {
			items[i++] = new SelectItem(tipoLancamento, tipoLancamento.getValue());
		}
		
		return items;
	}
	
	public Date getPrimeiraData() {
		return primeiraData;
	}
	
	public void setPrimeiraData(Date primeiraData) {
		this.primeiraData = primeiraData;
	}
	
	public Date getSegundaData() {
		return segundaData;
	}
	
	public void setSegundaData(Date segundaData) {
		this.segundaData = segundaData;
	}
}
