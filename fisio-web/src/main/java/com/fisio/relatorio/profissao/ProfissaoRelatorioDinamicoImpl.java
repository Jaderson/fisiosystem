package com.fisio.relatorio.profissao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.profissao.Profissao;
import com.fisio.relatorio.GeradorRelatorioDinamicoImpl;
import com.fisio.repository.profissao.ProfissaoRepository;

@Component
public class ProfissaoRelatorioDinamicoImpl extends GeradorRelatorioDinamicoImpl<Profissao> implements ProfissaoRelatorioDinamico {

	private ProfissaoRepository profissaoRepository;

	@Autowired
	public ProfissaoRelatorioDinamicoImpl(ProfissaoRepository profissaoRepository) {
		this.profissaoRepository = profissaoRepository;
	}

	@Override
	protected List<Profissao> popularLista() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		return this.profissaoRepository.consultarTodos(new Profissao(), listaRestricoes, "id");
	}


	@Override
	protected String getTitulo() {
		return "Profissões";
	}
}
