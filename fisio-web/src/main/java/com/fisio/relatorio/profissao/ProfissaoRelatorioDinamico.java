package com.fisio.relatorio.profissao;

import com.fisio.framework.profissao.Profissao;
import com.fisio.relatorio.GeradorRelatorioDinamico;

public interface ProfissaoRelatorioDinamico extends GeradorRelatorioDinamico<Profissao>{

}
