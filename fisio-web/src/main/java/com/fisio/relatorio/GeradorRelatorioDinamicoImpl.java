package com.fisio.relatorio;

import java.awt.Color;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.view.jasperreports.AbstractJasperReportsSingleFormatView;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.core.layout.LayoutManager;
import ar.com.fdvs.dj.core.layout.ListLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;

import com.fisio.framework.security.authentication.FisioSystem;
import com.fisio.model.annotations.relatorio.Relatorio;
import com.fisio.model.autenticacao.Usuario;

public abstract class GeradorRelatorioDinamicoImpl<E> extends AbstractJasperReportsSingleFormatView implements GeradorRelatorioDinamico<E> {

	private TipoRelatorio tipoRelatorio;

	@Override
	public void gerarRelatorio(HttpServletResponse response) {
		List<E> listaEntidade = popularLista();
		DynamicReportBuilder drb = criarReportBuilder();
		criarColunas(drb);
		DynamicReport dr = drb.build();
		imprimirRelatorio(response, dr, listaEntidade);
	}

	public TipoRelatorio getTipoRelatorio() {
		return tipoRelatorio;
	}

	@Override
	public void setTipoRelatorio(String tipoRelatorio) {
		this.tipoRelatorio = TipoRelatorio.getTipoRelatorio(tipoRelatorio);
	}

	public Usuario getUsuario() {
		UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder
				.getContext().getAuthentication();
		FisioSystem principal = (FisioSystem) authentication.getPrincipal();
		return principal.getUsuario();
	}

	@Override
	protected boolean useWriter() {
		return false;
	}

	@Override
	protected JRExporter createExporter() {
		return getTipoRelatorio().getJrExporter();
	}

	protected abstract List<E> popularLista();

	protected abstract String getTitulo();

	@SuppressWarnings("unchecked")
	private void criarColunas(DynamicReportBuilder drb) {
		Type type = ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		Class<E> classe = (Class<E>) type;

		Method[] methods = classe.getMethods();
		List<RelatorioModel> listaRelatorioModel = new ArrayList<RelatorioModel>();
		for (Method method : methods) {
			if (method.isAnnotationPresent(Relatorio.class)) {
				Relatorio annotation = method.getAnnotation(Relatorio.class);
				listaRelatorioModel.add(new RelatorioModel(annotation.titulo(),
						annotation.name(), annotation.className(), annotation
								.tamanho(), annotation.ordenacao()));
			}
		}

		Collections.sort(listaRelatorioModel);
		for (RelatorioModel relatorioModel : listaRelatorioModel) {
			AbstractColumn column = ColumnBuilder
					.getNew()
					.setColumnProperty(relatorioModel.name,
							relatorioModel.className.getName())
					.setTitle(relatorioModel.titulo)
					.setStyle(getStyle(relatorioModel.className))
					.setWidth(relatorioModel.tamanho).build();
			drb.addColumn(column);
		}
	}

	private Style getStyle(Class<?> clazz) {
		Style style = ESTILO_TEXTO;
		if (Integer.class.equals(clazz)) {
			style = ESTILO_NUMERO;
		} else if (Double.class.equals(clazz)) {
			style = ESTILO_VALOR;
		} else if (BigDecimal.class.equals(clazz)) {
			style = ESTILO_VALOR_MOEDA_REAL;
		} else if (Date.class.equals(clazz)) {
			style = ESTILO_DATA;
		}

		if (!TipoRelatorio.PDF.equals(getTipoRelatorio())) {
			style.setBorder(Border.NO_BORDER());
		}
		return style;
	}

	private DynamicReportBuilder criarReportBuilder() {
		DynamicReportBuilder drb = new DynamicReportBuilder()
				.setUseFullPageWidth(true)
				.setAllowDetailSplit(false)
				.setMargins(0, 0, 0, 0)
				.setWhenNoData("Não há dados para este relatório", null, true,
						true);

		if (TipoRelatorio.PDF.equals(getTipoRelatorio())) {
			URL resource = getClass().getResource(
					"/relatorios/header_templates.jrxml");
			drb.setTemplateFile(resource.getPath())
					.setPrintBackgroundOnOddRows(true);
		} else {
			drb.setIgnorePagination(true);
			drb.setPrintColumnNames(true);
			drb.setReportName(getTitulo());
			JRExporter exporter = getTipoRelatorio().getJrExporter();
			exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,
					Boolean.FALSE);
			exporter.setParameter(
					JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
					Boolean.TRUE);
			exporter.setParameter(
					JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,
					Boolean.FALSE);
		}

		return drb;
	}
	
	private void imprimirRelatorio(HttpServletResponse response,
			DynamicReport dr, List<E> lista) {
		JRDataSource ds = new JRBeanCollectionDataSource(lista);
		JasperPrint jp;
		try {
			ImageIcon gto = new ImageIcon(getClass().getResource("/relatorios/LOGO.jpg"));
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("logo", gto.getImage());
			parametros.put("titulo", getTitulo());
			jp = DynamicJasperHelper.generateJasperPrint(dr,
					getLayoutManager(), ds, parametros);

			setContentType(getTipoRelatorio().getContentType());
			setHeader(response);
			response.setHeader("Pragma", "no-cache");
			renderReport(jp, new HashMap<String, Object>(), response);

		} catch (Exception e) {
			throw new RuntimeException("Erro ao imprimir o relatório", e);
		} finally {
			FacesContext.getCurrentInstance().responseComplete();
		}
	}

	private LayoutManager getLayoutManager() {
		if (TipoRelatorio.PDF.equals(getTipoRelatorio())) {
			return new ClassicLayoutManager();
		}
		return new ListLayoutManager();
	}

	private void setHeader(HttpServletResponse response) {
		if (TipoRelatorio.PDF.equals(getTipoRelatorio())) {
			response.setHeader("Content-Disposition", "attachment; filename="
					+ getTitulo() + ".pdf");
		} else if (TipoRelatorio.EXCEL.equals(getTipoRelatorio())) {
			response.setHeader("Content-Disposition", "attachment; filename="
					+ getTitulo() + ".xls");
		} else if (TipoRelatorio.CSV.equals(getTipoRelatorio())) {
			response.setHeader("Content-Disposition", "attachment; filename="
					+ getTitulo() + ".csv");
		}
	}

	static {
		Border border = Border.PEN_1_POINT();
		border.setColor(Color.BLACK);

		ESTILO_TEXTO.setBorder(border);
		ESTILO_TEXTO.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_TEXTO.setHorizontalAlign(HorizontalAlign.LEFT);
		ESTILO_TEXTO.setFont(Font.TIMES_NEW_ROMAN_SMALL);

		ESTILO_NUMERO.setBorder(border);
		ESTILO_NUMERO.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_NUMERO.setHorizontalAlign(HorizontalAlign.RIGHT);
		ESTILO_NUMERO.setFont(Font.TIMES_NEW_ROMAN_SMALL);

		ESTILO_VALOR.setBorder(border);
		ESTILO_VALOR.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_VALOR.setHorizontalAlign(HorizontalAlign.RIGHT);
		ESTILO_VALOR.setFont(Font.TIMES_NEW_ROMAN_SMALL);
		ESTILO_VALOR.setPattern(FORMATO_VALOR);

		ESTILO_VALOR_MOEDA_REAL.setBorder(border);
		ESTILO_VALOR_MOEDA_REAL.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_VALOR_MOEDA_REAL.setHorizontalAlign(HorizontalAlign.RIGHT);
		ESTILO_VALOR_MOEDA_REAL.setFont(Font.TIMES_NEW_ROMAN_SMALL);
		ESTILO_VALOR_MOEDA_REAL.setPattern(FORMATO_VALOR_MOEDA_REAL);

		ESTILO_VALOR_MOEDA_DOLAR.setBorder(border);
		ESTILO_VALOR_MOEDA_DOLAR.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_VALOR_MOEDA_DOLAR.setHorizontalAlign(HorizontalAlign.RIGHT);
		ESTILO_VALOR_MOEDA_DOLAR.setFont(Font.TIMES_NEW_ROMAN_SMALL);
		ESTILO_VALOR_MOEDA_DOLAR.setPattern(FORMATO_VALOR_MOEDA_DOLAR);

		ESTILO_VALOR_QUATRO_CASAS_DECIMAIS.setBorder(border);
		ESTILO_VALOR_QUATRO_CASAS_DECIMAIS.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_VALOR_QUATRO_CASAS_DECIMAIS
				.setHorizontalAlign(HorizontalAlign.RIGHT);
		ESTILO_VALOR_QUATRO_CASAS_DECIMAIS.setFont(Font.TIMES_NEW_ROMAN_SMALL);
		ESTILO_VALOR_QUATRO_CASAS_DECIMAIS
				.setPattern(FORMATO_VALOR_QUATRO_CASAS_DECIMAIS);

		ESTILO_DATA.setBorder(border);
		ESTILO_DATA.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_DATA.setHorizontalAlign(HorizontalAlign.CENTER);
		ESTILO_DATA.setFont(Font.TIMES_NEW_ROMAN_SMALL);
		ESTILO_DATA.setPattern(FORMATO_DATA);

		ESTILO_MES_ANO.setBorder(border);
		ESTILO_MES_ANO.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_MES_ANO.setHorizontalAlign(HorizontalAlign.CENTER);
		ESTILO_MES_ANO.setFont(Font.TIMES_NEW_ROMAN_SMALL);
		ESTILO_MES_ANO.setPattern(FORMATO_MES_ANO);

		ESTILO_DATA_HORA.setBorder(border);
		ESTILO_DATA_HORA.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_DATA_HORA.setHorizontalAlign(HorizontalAlign.CENTER);
		ESTILO_DATA_HORA.setFont(Font.TIMES_NEW_ROMAN_SMALL);
		ESTILO_DATA_HORA.setPattern(FORMATO_DATA_HORA);

		ESTILO_HORA.setBorder(border);
		ESTILO_HORA.setVerticalAlign(VerticalAlign.TOP);
		ESTILO_HORA.setHorizontalAlign(HorizontalAlign.CENTER);
		ESTILO_HORA.setFont(Font.TIMES_NEW_ROMAN_SMALL);
		ESTILO_HORA.setPattern(FORMATO_HORA);
	}

	protected class RelatorioModel implements Comparable<RelatorioModel> {

		String titulo;
		String name;
		Class<?> className;
		Integer tamanho;
		Integer ordenacao;

		public RelatorioModel(String titulo, String name, Class<?> className,
				Integer tamanho, Integer ordenacao) {
			this.titulo = titulo;
			this.name = name;
			this.className = className;
			this.tamanho = tamanho;
			this.ordenacao = ordenacao;
		}

		@Override
		public int compareTo(RelatorioModel model) {
			return this.ordenacao.compareTo(model.ordenacao);
		}

	}
}
