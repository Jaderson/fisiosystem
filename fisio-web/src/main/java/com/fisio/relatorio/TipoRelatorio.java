package com.fisio.relatorio;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;

public enum TipoRelatorio {

	PDF("PDF", new JRPdfExporter(), "application/pdf"), 
	EXCEL("EXCEL", new JRXlsExporter(), "application/vnd.ms-excel"), 
	CSV("CSV", new JRCsvExporter(), "text/csv");
	
	private String value;
	private JRExporter jrExporter;
	private String contentType;

	private TipoRelatorio(String value, JRExporter jrExporter, String contentType) {
		this.value = value;
		this.jrExporter = jrExporter;
		this.contentType = contentType;
	}
	
	public String getValue() {
		return value;
	}
	
	public JRExporter getJrExporter() {
		return jrExporter;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public static TipoRelatorio getTipoRelatorio(String tipoRelatorioValue) {
		for (TipoRelatorio tipoRelatorio : TipoRelatorio.values()) {
			if (tipoRelatorio.getValue().equals(tipoRelatorioValue)) {
				return tipoRelatorio;
			}
		}
		
		return TipoRelatorio.PDF;
	}
}
