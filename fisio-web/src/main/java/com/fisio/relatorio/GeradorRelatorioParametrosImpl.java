package com.fisio.relatorio;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fisio.framework.security.authentication.FisioSystem;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.web.bean.locale.UtilLocale;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

public abstract class GeradorRelatorioParametrosImpl<E> implements GeradorRelatorioParametros<E>{

	public E entidade;
	protected abstract E getEntidade();
	
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void PDF(HashMap<String, Object> parametros,String resource) throws JRException, IOException, SQLException{
		JasperPrint jp;
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.responseComplete();
			jp = JasperFillManager.fillReport(
							resource,parametros,getConnection());
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			byte[] bytes = baos.toByteArray();

			if (bytes != null && bytes.length > 0) {
				HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
				response.setContentType("application/pdf");

				response.setHeader("Content-disposition","inline; filename=\"+"+ resource +".pdf\"");
				response.setContentLength(bytes.length);
				ServletOutputStream outputStream = response.getOutputStream();
				outputStream.write(bytes, 0, bytes.length);
				outputStream.flush();
				outputStream.close();

			}

		} catch (Exception e) {
			throw new RuntimeException("Erro ao imprimir o relatório", e);
		} finally {
			getConnection().close();
			FacesContext.getCurrentInstance().responseComplete();
		}
	}
	
	private FisioSystem getUserFisioSystem() {
		UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
		FisioSystem userFisio = (FisioSystem) authentication.getPrincipal();
		return userFisio;
	}
	
	public Usuario getUsuarioLogado() {
		FisioSystem userFisio = getUserFisioSystem();
		return userFisio.getUsuario();
	}
	
	public void setEntidade(E entidade) {
		this.entidade = entidade;
	}
	
	public String getDataFormatada(Date data, String format){
	    SimpleDateFormat dataFormat = new SimpleDateFormat(format);
	    return dataFormat.format(data);
	}
	
	protected void lancarErro(String mensagem) {
		if(mensagem.contains(";")) {
			String[] mensagens = mensagem.split(";");
			for (String mensagemDeErro : mensagens) {
				try {
					getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, UtilLocale.getMensagemI18n(mensagemDeErro)));
				} catch (IOException e) {
					e.printStackTrace();
				}  
			}
		} else {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getMensagemI18n(mensagem)));  
		}
    }  
	
	public String getMensagemI18n(String chave){
		return getContext().getApplication().getResourceBundle(getContext(), "msg").getString(chave);
	}
	
	public FacesContext getContext() {
		return FacesContext.getCurrentInstance();
	}
	
	public static Connection getConnection() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/cinemat_cinematica_prod", "cinemat_root", "24LMr;H7KN,*");
			//"jdbc:mysql://localhost:3306/cinemat_cinematica_prod", "root", ""
		} catch (ClassNotFoundException e) {
			throw new SQLException(e.getMessage());
		}
	}  
}
