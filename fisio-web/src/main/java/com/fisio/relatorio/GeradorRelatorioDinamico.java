package com.fisio.relatorio;


import javax.servlet.http.HttpServletResponse;

import ar.com.fdvs.dj.domain.Style;

public interface GeradorRelatorioDinamico<E> {

	Style ESTILO_TEXTO = new Style();
	Style ESTILO_NUMERO = new Style();
	Style ESTILO_VALOR = new Style();
	Style ESTILO_VALOR_MOEDA_REAL = new Style();
	Style ESTILO_VALOR_MOEDA_DOLAR = new Style();
	Style ESTILO_VALOR_QUATRO_CASAS_DECIMAIS = new Style();
	Style ESTILO_DATA = new Style();
	Style ESTILO_MES_ANO = new Style();
	Style ESTILO_DATA_HORA = new Style();
	Style ESTILO_HORA = new Style();
	
	String FORMATO_DATA = "dd/MM/yyyy";
	String FORMATO_MES_ANO = "MM/yyyy";
	String FORMATO_DATA_HORA = "dd/MM/yyyy HH:mm";
	String FORMATO_HORA = "HH:mm";
	String FORMATO_VALOR = "###,###,##0.00";
	String FORMATO_VALOR_MOEDA_REAL = "R$ 0.00";
	String FORMATO_VALOR_MOEDA_DOLAR = "$ 0.00";
	String FORMATO_VALOR_QUATRO_CASAS_DECIMAIS = "###,###,##0.0000";


	void gerarRelatorio(HttpServletResponse response);

	void setTipoRelatorio(String tipoRelatorio);
}


