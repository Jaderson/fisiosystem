package com.fisio.relatorio.usuario;

import com.fisio.model.autenticacao.Usuario;
import com.fisio.relatorio.GeradorRelatorioDinamico;

public interface UsuarioRelatorioDinamico extends GeradorRelatorioDinamico<Usuario>{

}
