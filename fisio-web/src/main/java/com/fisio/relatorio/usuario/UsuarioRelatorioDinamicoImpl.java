package com.fisio.relatorio.usuario;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.relatorio.GeradorRelatorioDinamicoImpl;
import com.fisio.repository.usuario.UsuarioRepository;

@Component
public class UsuarioRelatorioDinamicoImpl extends GeradorRelatorioDinamicoImpl<Usuario> implements UsuarioRelatorioDinamico {

	private UsuarioRepository usuarioRepository;

	@Autowired
	public UsuarioRelatorioDinamicoImpl(UsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}
	
	@Override
	protected List<Usuario> popularLista() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("pessoa.empresa.id", getUsuario().getEmpresa().getId()));
		return this.usuarioRepository.consultarTodos(new Usuario(), listaRestricoes, "pessoa.nome");
	}

	@Override
	protected String getTitulo() {
		return "Usuarios";
	}
}
