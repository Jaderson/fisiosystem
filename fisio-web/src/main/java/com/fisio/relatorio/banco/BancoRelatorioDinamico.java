package com.fisio.relatorio.banco;

import com.fisio.framework.banco.Banco;
import com.fisio.relatorio.GeradorRelatorioDinamico;

public interface BancoRelatorioDinamico extends GeradorRelatorioDinamico<Banco> {

}
