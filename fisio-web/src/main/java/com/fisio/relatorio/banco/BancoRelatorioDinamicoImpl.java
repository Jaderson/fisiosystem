package com.fisio.relatorio.banco;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.banco.Banco;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.relatorio.GeradorRelatorioDinamicoImpl;
import com.fisio.repository.banco.BancoRepository;

@Component
public class BancoRelatorioDinamicoImpl extends GeradorRelatorioDinamicoImpl<Banco> implements BancoRelatorioDinamico {

	private BancoRepository bancoRepository;

	@Autowired
	public BancoRelatorioDinamicoImpl(BancoRepository bancoRepository) {
		this.bancoRepository = bancoRepository;
	}
	
	@Override
	protected List<Banco> popularLista() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuario().getEmpresa().getId()));
		return this.bancoRepository.consultarTodos(new Banco(), listaRestricoes, "id");
	}
	@Override
	protected String getTitulo() {
		return "Bancos";
	}

}
