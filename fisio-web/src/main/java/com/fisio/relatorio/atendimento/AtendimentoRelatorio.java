package com.fisio.relatorio.atendimento;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.fisio.facade.especialidade.EspecialidadeFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.caixa.FluxoCaixa;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.relatorio.GeradorRelatorioParametrosImpl;
import com.fisio.web.converter.especialidade.EspecialidadeConversor;
import com.fisio.web.framework.combomodel.ItemCombo;

import net.sf.jasperreports.engine.JRException;

@Controller
@Component
@Scope("view")
public class AtendimentoRelatorio extends GeradorRelatorioParametrosImpl<FluxoCaixa> {

	private PessoaFacade pessoaFacade;
	private EspecialidadeConversor especialidadeConversor;
	private EspecialidadeFacade especialidadeFacade;
	private Date primeiraData;
	private Date segundaData;
	
	@Autowired
	public AtendimentoRelatorio(PessoaFacade pessoaFacade, EspecialidadeFacade especialidadeFacade){
		this.pessoaFacade = pessoaFacade;
		this.especialidadeFacade = especialidadeFacade;
		this.especialidadeConversor = new EspecialidadeConversor();
	}
	
	public void gerarRelatorio(FluxoCaixa entidade) throws JRException, IOException, SQLException{
		ImageIcon gto = new ImageIcon(getClass().getResource("/relatorios/LOGO.jpg"));
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		URL resource;
		URL resourceSum;
		
		resource = getClass().getResource("/relatorios/atendimentos.jasper");
		resourceSum = getClass().getResource("/relatorios/sumAtendimento.jasper");
		
		parametros.put("logo", gto.getImage());
		parametros.put("p_dataInicio",getDataFormatada(getPrimeiraData() == null ? new Date("2016/01/01") : getPrimeiraData(), "yyyy/MM/dd"));
		parametros.put("p_dataFim",	getDataFormatada(getSegundaData() == null ? new Date() : getSegundaData(), "yyyy/MM/dd"));
		parametros.put("p_idFuncionario", getEntidade().getPessoa() == null ? null : getEntidade().getPessoa().getId().toString());
		parametros.put("p_idEspecialidade", getEntidade().getEspecialidade() == null ? null : getEntidade().getEspecialidade().getId().toString());
		parametros.put("p_empresa", getUsuarioLogado().getEmpresa().getId());
		
		PDF(parametros, resource.getPath());
		PDF(parametros, resourceSum.getPath());
	}
	
	public void gerarSomaRelatorio(FluxoCaixa entidade) throws JRException, IOException, SQLException{
		ImageIcon gto = new ImageIcon(getClass().getResource("/relatorios/LOGO.jpg"));
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		URL resourceSum;
		
		resourceSum = getClass().getResource("/relatorios/sumAtendimento.jasper");
		
		parametros.put("logo", gto.getImage());
		parametros.put("p_dataInicio",getDataFormatada(getPrimeiraData() == null ? new Date("2016/01/01") : getPrimeiraData(), "yyyy/MM/dd"));
		parametros.put("p_dataFim",	getDataFormatada(getSegundaData() == null ? new Date() : getSegundaData(), "yyyy/MM/dd"));
		parametros.put("p_idFuncionario", getEntidade().getPessoa() == null ? null : getEntidade().getPessoa().getId().toString());
		parametros.put("p_idEspecialidade", getEntidade().getEspecialidade() == null ? null : getEntidade().getEspecialidade().getId().toString());
		parametros.put("p_empresa", getUsuarioLogado().getEmpresa().getId());
		
		PDF(parametros, resourceSum.getPath());
	}
	
	public List<Pessoa> listaFuncionario(String query) {
		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		Pessoa pessoaConsulta = new Pessoa();

		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Sim));
		listaRestricoes.add(Restricoes.like("nome", query));
		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		for (Pessoa pessoa : listaPessoas) {
			listaPessoa.add(pessoa);
		}
		return listaPessoas;
	}
	
	@Override
	public FluxoCaixa getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new FluxoCaixa();
		}
		return entidade;
	}

	public List<ItemCombo> getListaEspecialidade() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Especialidade especialidadeConsulta = new Especialidade();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		List<Especialidade> listaEspecialidade = this.especialidadeFacade.consultarTodos(especialidadeConsulta, listaRestricoes, especialidadeConsulta.getCampoOrderBy());
		for (Especialidade especialidade : listaEspecialidade) {
				listaItemCombo.add(new ItemCombo(especialidade.getDescricao(), especialidade));
		}
		return listaItemCombo;
	}
	
	public EspecialidadeConversor getEspecialidadeConversor() {
		return especialidadeConversor;
	}
	
	public void setEspecialidadeConversor(
			EspecialidadeConversor especialidadeConversor) {
		this.especialidadeConversor = especialidadeConversor;
	}
	
	public Date getPrimeiraData() {
		return primeiraData;
	}
	
	public void setPrimeiraData(Date primeiraData) {
		this.primeiraData = primeiraData;
	}
	
	public Date getSegundaData() {
		return segundaData;
	}
	
	public void setSegundaData(Date segundaData) {
		this.segundaData = segundaData;
	}
}