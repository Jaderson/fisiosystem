package com.fisio.relatorio.especialidade;

import com.fisio.framework.especialidade.Especialidade;
import com.fisio.relatorio.GeradorRelatorioDinamico;

public interface EspecialidadeRelatorioDinamico extends GeradorRelatorioDinamico<Especialidade>{

}
