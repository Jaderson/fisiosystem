package com.fisio.relatorio.especialidade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.relatorio.GeradorRelatorioDinamicoImpl;
import com.fisio.repository.especialidade.EspecialidadeRepository;

@Component
public class EspecialidadeRelatorioDinamicoImpl extends GeradorRelatorioDinamicoImpl<Especialidade> implements EspecialidadeRelatorioDinamico{
	
	private EspecialidadeRepository especialidadeRepository;

	@Autowired
	public EspecialidadeRelatorioDinamicoImpl(EspecialidadeRepository especialidadeRepository) {
		this.especialidadeRepository = especialidadeRepository;
	}

	@Override
	protected List<Especialidade> popularLista() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		return this.especialidadeRepository.consultarTodos(new Especialidade(), listaRestricoes, "id");
	}

	@Override
	protected String getTitulo() {
		return "Especialidades";
	}

}
