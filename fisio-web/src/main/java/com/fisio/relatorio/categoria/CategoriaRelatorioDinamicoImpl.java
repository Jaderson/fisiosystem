package com.fisio.relatorio.categoria;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.relatorio.GeradorRelatorioDinamicoImpl;
import com.fisio.repository.categoria.CategoriaRepository;

@Component
public class CategoriaRelatorioDinamicoImpl extends GeradorRelatorioDinamicoImpl<Categoria> implements CategoriaRelatorioDinamico {
	
	private CategoriaRepository categoriaRepository;

	@Autowired
	public CategoriaRelatorioDinamicoImpl(CategoriaRepository categoriaRepository) {
		this.categoriaRepository = categoriaRepository;
	}

	@Override
	protected List<Categoria> popularLista() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		return this.categoriaRepository.consultarTodos(new Categoria(), listaRestricoes, "id");
	}


	@Override
	protected String getTitulo() {
		return "Categorias";
	}

}
