package com.fisio.relatorio.categoria;

import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.relatorio.GeradorRelatorioDinamico;

public interface CategoriaRelatorioDinamico extends GeradorRelatorioDinamico<Categoria> {

}
