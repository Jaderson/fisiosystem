package com.fisio.web.consulta.perfil;

import com.fisio.model.autenticacao.Usuario;

public interface PerfilConsulta {
	
	void setUsuario(Usuario usuario);
}
