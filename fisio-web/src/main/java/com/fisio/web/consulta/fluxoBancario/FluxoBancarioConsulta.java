package com.fisio.web.consulta.fluxoBancario;

import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.Empresa;

public interface FluxoBancarioConsulta {

	void setUsuario(Usuario usuario);

	void setEmpresa(Empresa empresa);
}
