package com.fisio.web.consulta.agenda.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.agenda.AgendaFacade;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.consulta.agenda.AgendaConsulta;

@Component
public class AgendaConsultaImpl extends LazyDataModel<Agenda> implements AgendaConsulta{

	private static final long serialVersionUID = 1L;
	
	private AgendaFacade agendaFacade;
	private Empresa empresa;
	
	@Autowired
	public AgendaConsultaImpl(AgendaFacade agendaFacade) {
		this.agendaFacade = agendaFacade;
	}

	@Override
	public List<Agenda> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Agenda agenda = new Agenda();
		Integer quantidadeRegistros = this.agendaFacade.consultarQuantidadeRegistros(agenda, new ArrayList<Restricoes>(), agenda.getCampoOrderBy());
		List<Agenda> listaAgenda = this.agendaFacade.consultarTodosPaginado(agenda, first, pageSize, new ArrayList<Restricoes>(), agenda.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaAgenda;
	}
	
	@Override
	public void setEmpresaUsuarioLogado(Empresa empresa) {
		this.empresa = empresa;
	}
}
