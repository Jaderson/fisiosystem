package com.fisio.web.consulta.palmilha.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.palmilha.PalmilhaFacade;
import com.fisio.framework.avaliacoes.Palmilha;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.web.consulta.palmilha.PalmilhaConsulta;

@Component
public class PalmilhaConsultaImpl extends LazyDataModel<Palmilha> implements PalmilhaConsulta{

	private static final long serialVersionUID = 1L;
	
	private PalmilhaFacade palmilhaFacade;
	
	@Autowired
	public PalmilhaConsultaImpl(PalmilhaFacade palmilhaFacade) {
		this.palmilhaFacade = palmilhaFacade;
	}

	@Override
	public List<Palmilha> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Palmilha palmilha = new Palmilha();
		Integer quantidadeRegistros = this.palmilhaFacade.consultarQuantidadeRegistros(palmilha, new ArrayList<Restricoes>(), palmilha.getCampoOrderBy());
		List<Palmilha> listaPalmilha = this.palmilhaFacade.consultarTodosPaginado(palmilha, first, pageSize, new ArrayList<Restricoes>(), palmilha.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaPalmilha;
	}
}
