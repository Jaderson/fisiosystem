package com.fisio.web.consulta.fisioterapia.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.fisioterapia.FisioterapiaFacade;
import com.fisio.framework.avaliacoes.Fisioterapia;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.consulta.fisioterapia.FisioterapiaConsulta;

@Component
public class FisioterapiaConsultaImpl extends LazyDataModel<Fisioterapia> implements FisioterapiaConsulta{

	private static final long serialVersionUID = 1L;
	
	private FisioterapiaFacade fisioterapiaFacade;
	private Usuario usuario;
	private Empresa empresa;
	
	@Autowired
	public FisioterapiaConsultaImpl(FisioterapiaFacade fisioterapiaFacade) {
		this.fisioterapiaFacade = fisioterapiaFacade;
	}

	@Override
	public List<Fisioterapia> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Fisioterapia fisioterapia = new Fisioterapia();
		Integer quantidadeRegistros = this.fisioterapiaFacade.consultarQuantidadeRegistros(fisioterapia, new ArrayList<Restricoes>(), fisioterapia.getCampoOrderBy());
		List<Fisioterapia> listaFisioterapia = this.fisioterapiaFacade.consultarTodosPaginado(fisioterapia, first, pageSize, new ArrayList<Restricoes>(), fisioterapia.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaFisioterapia;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
