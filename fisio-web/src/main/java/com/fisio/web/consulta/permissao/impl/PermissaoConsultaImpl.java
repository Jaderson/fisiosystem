package com.fisio.web.consulta.permissao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.permissao.PermissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.web.consulta.permissao.PermissaoConsulta;

@Component
public class PermissaoConsultaImpl extends LazyDataModel<Permissao> implements PermissaoConsulta{

	private static final long serialVersionUID = 1L;
	
	private PermissaoFacade permissaoFacade;
	
	@Autowired
	public PermissaoConsultaImpl(PermissaoFacade permissaoFacade) {
		this.permissaoFacade = permissaoFacade;
	}

	@Override
	public List<Permissao> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Permissao permissao = new Permissao();
		Integer quantidadeRegistros = this.permissaoFacade.consultarQuantidadeRegistros(permissao, new ArrayList<Restricoes>(), permissao.getCampoOrderBy());
		List<Permissao> listaPermissao = this.permissaoFacade.consultarTodosPaginado(permissao, first, pageSize, new ArrayList<Restricoes>(), permissao.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaPermissao;
	}
}
