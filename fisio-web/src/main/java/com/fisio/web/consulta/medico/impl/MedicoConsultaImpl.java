package com.fisio.web.consulta.medico.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.medico.MedicoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.medico.Medico;
import com.fisio.web.consulta.medico.MedicoConsulta;

@Component
public class MedicoConsultaImpl extends LazyDataModel<Medico> implements MedicoConsulta{

	private static final long serialVersionUID = 1L;
	
	private MedicoFacade medicoFacade;
	
	@Autowired
	public MedicoConsultaImpl(MedicoFacade medicoFacade) {
		this.medicoFacade = medicoFacade;
	}

	@Override
	public List<Medico> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Medico medico = new Medico();
		Integer quantidadeRegistros = this.medicoFacade.consultarQuantidadeRegistros(medico, new ArrayList<Restricoes>(), medico.getCampoOrderBy());
		List<Medico> listaMedico = this.medicoFacade.consultarTodosPaginado(medico, first, pageSize, new ArrayList<Restricoes>(), medico.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaMedico;
	}
}
