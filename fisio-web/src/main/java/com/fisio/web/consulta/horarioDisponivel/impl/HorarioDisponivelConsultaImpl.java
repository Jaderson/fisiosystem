package com.fisio.web.consulta.horarioDisponivel.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.horarioDisponivel.HorarioDisponivelFacade;
import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.web.consulta.horarioDisponivel.HorarioDisponivelConsulta;

@Component
public class HorarioDisponivelConsultaImpl extends LazyDataModel<HorarioDisponivel> implements HorarioDisponivelConsulta{

	private static final long serialVersionUID = 1L;
	
	private HorarioDisponivelFacade horarioDisponivelFacade;
	
	@Autowired
	public HorarioDisponivelConsultaImpl(HorarioDisponivelFacade horarioDisponivelFacade) {
		this.horarioDisponivelFacade = horarioDisponivelFacade;
	}

	@Override
	public List<HorarioDisponivel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		HorarioDisponivel horarioDisponivel = new HorarioDisponivel();
		Integer quantidadeRegistros = this.horarioDisponivelFacade.consultarQuantidadeRegistros(horarioDisponivel, new ArrayList<Restricoes>(), horarioDisponivel.getCampoOrderBy());
		List<HorarioDisponivel> listaHorarioDisponivel = this.horarioDisponivelFacade.consultarTodosPaginado(horarioDisponivel, first, pageSize, new ArrayList<Restricoes>(), horarioDisponivel.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaHorarioDisponivel;
	}
}
