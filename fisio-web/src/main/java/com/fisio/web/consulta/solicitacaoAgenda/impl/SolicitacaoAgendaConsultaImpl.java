package com.fisio.web.consulta.solicitacaoAgenda.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.solicitacaoAgenda.SolicitacaoAgendaFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.fisio.web.consulta.solicitacaoAgenda.SolicitacaoAgendaConsulta;

@Component
public class SolicitacaoAgendaConsultaImpl extends LazyDataModel<SolicitacaoAgenda> implements SolicitacaoAgendaConsulta{

	private static final long serialVersionUID = 1L;
	
	private SolicitacaoAgendaFacade solicitacaoAgendaFacade;
	
	@Autowired
	public SolicitacaoAgendaConsultaImpl(SolicitacaoAgendaFacade solicitacaoAgendaFacade) {
		this.solicitacaoAgendaFacade = solicitacaoAgendaFacade;
	}

	@Override
	public List<SolicitacaoAgenda> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		SolicitacaoAgenda solicitacaoAgenda = new SolicitacaoAgenda();
		Integer quantidadeRegistros = this.solicitacaoAgendaFacade.consultarQuantidadeRegistros(solicitacaoAgenda, new ArrayList<Restricoes>(), solicitacaoAgenda.getCampoOrderBy());
		List<SolicitacaoAgenda> listaSolicitacaoAgenda = this.solicitacaoAgendaFacade.consultarTodosPaginado(solicitacaoAgenda, first, pageSize, new ArrayList<Restricoes>(), solicitacaoAgenda.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaSolicitacaoAgenda;
	}
}
