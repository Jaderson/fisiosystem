package com.fisio.web.consulta.fisioterapia;

import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.Empresa;

public interface FisioterapiaConsulta {

	void setUsuario(Usuario usuario);

	void setEmpresa(Empresa empresa);
}
