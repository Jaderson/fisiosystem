package com.fisio.web.consulta.formaPagamento.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.formaPagamento.FormaPagamentoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.web.consulta.formaPagamento.FormaPagamentoConsulta;

@Component
public class FormaPagamentoConsultaImpl extends LazyDataModel<FormaPagamento> implements FormaPagamentoConsulta{

	private static final long serialVersionUID = 1L;
	
	private FormaPagamentoFacade formaPagamentoFacade;
	
	@Autowired
	public FormaPagamentoConsultaImpl(FormaPagamentoFacade formaPagamentoFacade) {
		this.formaPagamentoFacade = formaPagamentoFacade;
	}

	@Override
	public List<FormaPagamento> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		FormaPagamento formaPagamento = new FormaPagamento();
		Integer quantidadeRegistros = this.formaPagamentoFacade.consultarQuantidadeRegistros(formaPagamento, new ArrayList<Restricoes>(), formaPagamento.getCampoOrderBy());
		List<FormaPagamento> listaFormaPagamento = this.formaPagamentoFacade.consultarTodosPaginado(formaPagamento, first, pageSize, new ArrayList<Restricoes>(), formaPagamento.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaFormaPagamento;
	}
}
