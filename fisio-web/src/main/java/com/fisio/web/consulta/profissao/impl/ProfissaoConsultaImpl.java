package com.fisio.web.consulta.profissao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.profissao.ProfissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.profissao.Profissao;
import com.fisio.web.consulta.profissao.ProfissaoConsulta;

@Component
public class ProfissaoConsultaImpl extends LazyDataModel<Profissao> implements ProfissaoConsulta{

	private static final long serialVersionUID = 1L;
	
	private ProfissaoFacade profissoesFacade;
	
	@Autowired
	public ProfissaoConsultaImpl(ProfissaoFacade profissoesFacade) {
		this.profissoesFacade = profissoesFacade;
	}

	@Override
	public List<Profissao> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Profissao profissoes = new Profissao();
		Integer quantidadeRegistros = this.profissoesFacade.consultarQuantidadeRegistros(profissoes, new ArrayList<Restricoes>(), profissoes.getCampoOrderBy());
		List<Profissao> listaProfissoes = this.profissoesFacade.consultarTodosPaginado(profissoes, first, pageSize, new ArrayList<Restricoes>(), profissoes.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaProfissoes;
	}
}
