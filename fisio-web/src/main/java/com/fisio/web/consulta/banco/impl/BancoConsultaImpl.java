package com.fisio.web.consulta.banco.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.banco.BancoFacade;
import com.fisio.framework.banco.Banco;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.web.consulta.banco.BancoConsulta;

@Component
public class BancoConsultaImpl extends LazyDataModel<Banco> implements BancoConsulta{

	private static final long serialVersionUID = 1L;
	
	private BancoFacade bancoFacade;
	
	@Autowired
	public BancoConsultaImpl(BancoFacade bancoFacade) {
		this.bancoFacade = bancoFacade;
	}

	@Override
	public List<Banco> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Banco banco = new Banco();
		Integer quantidadeRegistros = this.bancoFacade.consultarQuantidadeRegistros(banco, new ArrayList<Restricoes>(), banco.getCampoOrderBy());
		List<Banco> listaBanco = this.bancoFacade.consultarTodosPaginado(banco, first, pageSize, new ArrayList<Restricoes>(), banco.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaBanco;
	}
}
