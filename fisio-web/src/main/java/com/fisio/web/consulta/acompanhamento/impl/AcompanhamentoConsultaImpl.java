package com.fisio.web.consulta.acompanhamento.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.acompanhamento.AcompanhamentoFacade;
import com.fisio.framework.acompanhamento.AcompanhamentoPessoa;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.consulta.acompanhamento.AcompanhamentoConsulta;

@Component
public class AcompanhamentoConsultaImpl extends LazyDataModel<AcompanhamentoPessoa> implements AcompanhamentoConsulta{

	private static final long serialVersionUID = 1L;
	
	private AcompanhamentoFacade acompanhamentoFacade;
	private Empresa empresa;
	
	@Autowired
	public AcompanhamentoConsultaImpl(AcompanhamentoFacade acompanhamentoFacade) {
		this.acompanhamentoFacade = acompanhamentoFacade;
	}

	@Override
	public List<AcompanhamentoPessoa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		AcompanhamentoPessoa acompanhamento = new AcompanhamentoPessoa();
		Integer quantidadeRegistros = this.acompanhamentoFacade.consultarQuantidadeRegistros(acompanhamento, new ArrayList<Restricoes>(), acompanhamento.getCampoOrderBy());
		List<AcompanhamentoPessoa> listaAcompanhamento = this.acompanhamentoFacade.consultarTodosPaginado(acompanhamento, first, pageSize, new ArrayList<Restricoes>(), acompanhamento.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaAcompanhamento;
	}
	
	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
