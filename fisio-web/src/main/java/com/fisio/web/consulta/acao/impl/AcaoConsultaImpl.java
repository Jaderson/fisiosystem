package com.fisio.web.consulta.acao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.acao.AcaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Acao;
import com.fisio.web.consulta.acao.AcaoConsulta;

@Component
public class AcaoConsultaImpl extends LazyDataModel<Acao> implements AcaoConsulta{

	private static final long serialVersionUID = 1L;
	
	private AcaoFacade acaoFacade;
	
	@Autowired
	public AcaoConsultaImpl(AcaoFacade acaoFacade) {
		this.acaoFacade = acaoFacade;
	}

	@Override
	public List<Acao> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Acao acao = new Acao();
		Integer quantidadeRegistros = this.acaoFacade.consultarQuantidadeRegistros(acao, new ArrayList<Restricoes>(), null);
		List<Acao> listaAcao = this.acaoFacade.consultarTodosPaginado(acao, first, pageSize, new ArrayList<Restricoes>(), null);
		setRowCount(quantidadeRegistros);	
		return listaAcao;
	}
}
