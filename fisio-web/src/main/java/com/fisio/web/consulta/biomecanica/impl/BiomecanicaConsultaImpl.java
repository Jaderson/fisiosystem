package com.fisio.web.consulta.biomecanica.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.biomecanica.BiomecanicaFacade;
import com.fisio.framework.avaliacoes.Biomecanica;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.consulta.biomecanica.BiomecanicaConsulta;

@Component
public class BiomecanicaConsultaImpl extends LazyDataModel<Biomecanica> implements BiomecanicaConsulta{

	private static final long serialVersionUID = 1L;
	
	private BiomecanicaFacade biomecanicaFacade;
	private Usuario usuario;
	private Empresa empresa;
	
	@Autowired
	public BiomecanicaConsultaImpl(BiomecanicaFacade biomecanicaFacade) {
		this.biomecanicaFacade = biomecanicaFacade;
	}

	@Override
	public List<Biomecanica> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Biomecanica biomecanica = new Biomecanica();
		Integer quantidadeRegistros = this.biomecanicaFacade.consultarQuantidadeRegistros(biomecanica, new ArrayList<Restricoes>(), biomecanica.getCampoOrderBy());
		List<Biomecanica> listaBiomecanica = this.biomecanicaFacade.consultarTodosPaginado(biomecanica, first, pageSize, new ArrayList<Restricoes>(), biomecanica.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaBiomecanica;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
