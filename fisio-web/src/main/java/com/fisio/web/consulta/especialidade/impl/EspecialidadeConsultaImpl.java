package com.fisio.web.consulta.especialidade.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.especialidade.EspecialidadeFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.web.consulta.especialidade.EspecialidadeConsulta;

@Component
public class EspecialidadeConsultaImpl extends LazyDataModel<Especialidade> implements EspecialidadeConsulta{

	private static final long serialVersionUID = 1L;
	
	private EspecialidadeFacade especialidadeFacade;
	
	@Autowired
	public EspecialidadeConsultaImpl(EspecialidadeFacade especialidadeFacade) {
		this.especialidadeFacade = especialidadeFacade;
	}

	@Override
	public List<Especialidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Especialidade especialidade = new Especialidade();
		Integer quantidadeRegistros = this.especialidadeFacade.consultarQuantidadeRegistros(especialidade, new ArrayList<Restricoes>(), especialidade.getCampoOrderBy());
		List<Especialidade> listaEspecialidade = this.especialidadeFacade.consultarTodosPaginado(especialidade, first, pageSize, new ArrayList<Restricoes>(), especialidade.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaEspecialidade;
	}
}
