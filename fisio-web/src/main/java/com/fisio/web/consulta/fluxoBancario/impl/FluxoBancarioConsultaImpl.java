
package com.fisio.web.consulta.fluxoBancario.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.fluxoBancario.FluxoBancarioFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.fluxoBancario.FluxoBancario;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.consulta.fluxoBancario.FluxoBancarioConsulta;

@Component
public class FluxoBancarioConsultaImpl extends LazyDataModel<FluxoBancario> implements FluxoBancarioConsulta{

	private static final long serialVersionUID = 1L;
	
	private FluxoBancarioFacade fluxoBancarioFacade;
	private Usuario usuario;
	private Empresa empresa;
	
	
	@Autowired
	public FluxoBancarioConsultaImpl(FluxoBancarioFacade fluxoBancarioFacade) {
		this.fluxoBancarioFacade = fluxoBancarioFacade;
	}

	@Override
	public List<FluxoBancario> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		FluxoBancario fluxoBancario = new FluxoBancario();
		Integer quantidadeRegistros = this.fluxoBancarioFacade.consultarQuantidadeRegistros(fluxoBancario, new ArrayList<Restricoes>(), fluxoBancario.getCampoOrderBy());
		List<FluxoBancario> listaFluxoBancario = this.fluxoBancarioFacade.consultarTodosPaginado(fluxoBancario, first, pageSize, new ArrayList<Restricoes>(), fluxoBancario.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaFluxoBancario;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
