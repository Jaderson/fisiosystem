package com.fisio.web.consulta.acompanhamentoFilho.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.acompanhamentoFilho.AcompanhamentoFilhoFacade;
import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.web.consulta.acompanhamentoFilho.AcompanhamentoFilhoConsulta;

@Component
public class AcompanhamentoFilhoConsultaImpl extends LazyDataModel<AcompanhamentoFilho> implements AcompanhamentoFilhoConsulta{

	private static final long serialVersionUID = 1L;
	
	private AcompanhamentoFilhoFacade acompanhamentoFilhoFacade;
	
	@Autowired
	public AcompanhamentoFilhoConsultaImpl(AcompanhamentoFilhoFacade acompanhamentoFilhoFacade) {
		this.acompanhamentoFilhoFacade = acompanhamentoFilhoFacade;
	}

	@Override
	public List<AcompanhamentoFilho> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		AcompanhamentoFilho acompanhamentoFilho = new AcompanhamentoFilho();
		Integer quantidadeRegistros = this.acompanhamentoFilhoFacade.consultarQuantidadeRegistros(acompanhamentoFilho, new ArrayList<Restricoes>(), acompanhamentoFilho.getCampoOrderBy());
		List<AcompanhamentoFilho> listaAcompanhamentoFilho = this.acompanhamentoFilhoFacade.consultarTodosPaginado(acompanhamentoFilho, first, pageSize, new ArrayList<Restricoes>(), acompanhamentoFilho.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaAcompanhamentoFilho;
	}
}
