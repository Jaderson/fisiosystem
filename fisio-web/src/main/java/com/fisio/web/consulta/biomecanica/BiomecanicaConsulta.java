package com.fisio.web.consulta.biomecanica;

import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.Empresa;

public interface BiomecanicaConsulta {
	
	void setUsuario(Usuario usuario);
	void setEmpresa(Empresa empresa);
}
