package com.fisio.web.consulta.dataFalta.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.dataFalta.DataFaltaFacade;
import com.fisio.framework.agenda.DataFalta;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.web.consulta.dataFalta.DataFaltaConsulta;

@Component
public class DataFaltaConsultaImpl extends LazyDataModel<DataFalta> implements DataFaltaConsulta{

	private static final long serialVersionUID = 1L;
	
	private DataFaltaFacade dataFaltaFacade;
	
	@Autowired
	public DataFaltaConsultaImpl(DataFaltaFacade dataFaltaFacade) {
		this.dataFaltaFacade = dataFaltaFacade;
	}

	@Override
	public List<DataFalta> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		DataFalta dataFalta = new DataFalta();
		Integer quantidadeRegistros = this.dataFaltaFacade.consultarQuantidadeRegistros(dataFalta, new ArrayList<Restricoes>(), dataFalta.getCampoOrderBy());
		List<DataFalta> listaDataFalta = this.dataFaltaFacade.consultarTodosPaginado(dataFalta, first, pageSize, new ArrayList<Restricoes>(), dataFalta.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaDataFalta;
	}
}
