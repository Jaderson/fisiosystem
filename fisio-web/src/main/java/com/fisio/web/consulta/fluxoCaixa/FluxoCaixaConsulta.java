package com.fisio.web.consulta.fluxoCaixa;

import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.Empresa;

public interface FluxoCaixaConsulta {

	void setUsuario(Usuario usuario);
	
	void setEmpresa(Empresa empresa);
}
