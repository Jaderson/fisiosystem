package com.fisio.web.consulta.perfil.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.perfil.PerfilFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.web.consulta.perfil.PerfilConsulta;

@Component
public class PerfilConsultaImpl extends LazyDataModel<Perfil> implements PerfilConsulta{

	private static final long serialVersionUID = 1L;
	
	private PerfilFacade perfilFacade;
	private Usuario usuario;
	
	@Autowired
	public PerfilConsultaImpl(PerfilFacade perfilFacade) {
		this.perfilFacade = perfilFacade;
	}

	@Override
	public List<Perfil> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Perfil perfil = new Perfil();
		Integer quantidadeRegistros = this.perfilFacade.consultarQuantidadeRegistros(perfil, new ArrayList<Restricoes>(), perfil.getCampoOrderBy());
		List<Perfil> listaPerfil = this.perfilFacade.consultarTodosPaginado(perfil, first, pageSize, new ArrayList<Restricoes>(), perfil.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaPerfil;
	}
	
	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
