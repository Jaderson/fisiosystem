package com.fisio.web.consulta.contasPagar;

import com.fisio.model.empresa.Empresa;

public interface ContasPagarConsulta {

	void setEmpresa(Empresa empresa);
}
