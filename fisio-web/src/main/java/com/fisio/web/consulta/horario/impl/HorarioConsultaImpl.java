package com.fisio.web.consulta.horario.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.horario.HorarioFacade;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.web.consulta.horario.HorarioConsulta;

@Component
public class HorarioConsultaImpl extends LazyDataModel<Horario> implements HorarioConsulta{

	private static final long serialVersionUID = 1L;
	
	private HorarioFacade horarioFacade;
	
	@Autowired
	public HorarioConsultaImpl(HorarioFacade horarioFacade) {
		this.horarioFacade = horarioFacade;
	}

	@Override
	public List<Horario> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Horario horario = new Horario();
		Integer quantidadeRegistros = this.horarioFacade.consultarQuantidadeRegistros(horario, new ArrayList<Restricoes>(), horario.getCampoOrderBy());
		List<Horario> listaHorario = this.horarioFacade.consultarTodosPaginado(horario, first, pageSize, new ArrayList<Restricoes>(), horario.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaHorario;
	}
}
