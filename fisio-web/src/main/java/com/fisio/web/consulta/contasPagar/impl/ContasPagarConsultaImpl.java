package com.fisio.web.consulta.contasPagar.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.contasPagar.ContasPagarFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.contasPagar.ContasPagar;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.consulta.contasPagar.ContasPagarConsulta;

@Component
public class ContasPagarConsultaImpl extends LazyDataModel<ContasPagar> implements ContasPagarConsulta{

	private static final long serialVersionUID = 1L;
	
	private ContasPagarFacade contasPagarFacade;
	private Empresa empresa;
	
	@Autowired
	public ContasPagarConsultaImpl(ContasPagarFacade contasPagarFacade) {
		this.contasPagarFacade = contasPagarFacade;
	}

	@Override
	public List<ContasPagar> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		ContasPagar contasPagar = new ContasPagar();
		Integer quantidadeRegistros = this.contasPagarFacade.consultarQuantidadeRegistros(contasPagar, new ArrayList<Restricoes>(), contasPagar.getCampoOrderBy());
		List<ContasPagar> listaContasPagar = this.contasPagarFacade.consultarTodosPaginado(contasPagar, first, pageSize, new ArrayList<Restricoes>(), contasPagar.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaContasPagar;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
