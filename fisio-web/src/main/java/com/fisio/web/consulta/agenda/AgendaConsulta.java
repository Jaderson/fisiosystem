package com.fisio.web.consulta.agenda;

import com.fisio.model.empresa.Empresa;

public interface AgendaConsulta {

	void setEmpresaUsuarioLogado(Empresa empresa);
}
