package com.fisio.web.consulta.usuarioPerfil.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.usuarioPerfil.UsuarioPerfilFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.web.consulta.usuarioPerfil.UsuarioPerfilConsulta;

@Component
public class UsuarioPerfilConsultaImpl extends LazyDataModel<UsuarioPerfil> implements UsuarioPerfilConsulta{

	private static final long serialVersionUID = 1L;
	
	private UsuarioPerfilFacade usuarioPerfilFacade;
	
	@Autowired
	public UsuarioPerfilConsultaImpl(UsuarioPerfilFacade usuarioPerfilFacade) {
		this.usuarioPerfilFacade = usuarioPerfilFacade;
	}

	@Override
	public List<UsuarioPerfil> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		UsuarioPerfil usuarioPerfil = new UsuarioPerfil();
		Integer quantidadeRegistros = this.usuarioPerfilFacade.consultarQuantidadeRegistros(usuarioPerfil, new ArrayList<Restricoes>(), usuarioPerfil.getCampoOrderBy());
		List<UsuarioPerfil> listaUsuarioPerfil = this.usuarioPerfilFacade.consultarTodosPaginado(usuarioPerfil, first, pageSize, new ArrayList<Restricoes>(), usuarioPerfil.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaUsuarioPerfil;
	}
}
