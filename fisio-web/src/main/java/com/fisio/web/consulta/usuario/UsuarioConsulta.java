package com.fisio.web.consulta.usuario;

import com.fisio.model.empresa.Empresa;

public interface UsuarioConsulta {

	void setEmpresaUsuarioLogado(Empresa empresa);

}
