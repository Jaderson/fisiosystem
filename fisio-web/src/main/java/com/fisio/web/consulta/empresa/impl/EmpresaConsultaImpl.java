package com.fisio.web.consulta.empresa.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.empresa.EmpresaFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.consulta.empresa.EmpresaConsulta;

@Component
public class EmpresaConsultaImpl extends LazyDataModel<Empresa> implements EmpresaConsulta{

	private static final long serialVersionUID = 1L;
	
	private EmpresaFacade empresaFacade;
	
	@Autowired
	public EmpresaConsultaImpl(EmpresaFacade empresaFacade) {
		this.empresaFacade = empresaFacade;
	}

	@Override
	public List<Empresa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Empresa empresa = new Empresa();
		Integer quantidadeRegistros = this.empresaFacade.consultarQuantidadeRegistros(empresa, new ArrayList<Restricoes>(), empresa.getCampoOrderBy());
		List<Empresa> listaEmpresa = this.empresaFacade.consultarTodosPaginado(empresa, first, pageSize, new ArrayList<Restricoes>(), empresa.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaEmpresa;
	}
}
