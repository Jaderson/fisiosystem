package com.fisio.web.consulta.acompanhamento;

import com.fisio.model.empresa.Empresa;

public interface AcompanhamentoConsulta {

	void setEmpresa(Empresa empresa);
}
