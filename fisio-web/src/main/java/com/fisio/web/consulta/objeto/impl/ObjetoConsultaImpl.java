package com.fisio.web.consulta.objeto.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.objeto.ObjetoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Objeto;
import com.fisio.web.consulta.objeto.ObjetoConsulta;

@Component
public class ObjetoConsultaImpl extends LazyDataModel<Objeto> implements ObjetoConsulta{

	private static final long serialVersionUID = 1L;
	
	private ObjetoFacade objetoFacade;
	
	@Autowired
	public ObjetoConsultaImpl(ObjetoFacade objetoFacade) {
		this.objetoFacade = objetoFacade;
	}

	@Override
	public List<Objeto> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Objeto objeto = new Objeto();
		Integer quantidadeRegistros = this.objetoFacade.consultarQuantidadeRegistros(objeto, new ArrayList<Restricoes>(), null);
		List<Objeto> listaObjeto = this.objetoFacade.consultarTodosPaginado(objeto, first, pageSize, new ArrayList<Restricoes>(), null);
		setRowCount(quantidadeRegistros);	
		return listaObjeto;
	}
}
