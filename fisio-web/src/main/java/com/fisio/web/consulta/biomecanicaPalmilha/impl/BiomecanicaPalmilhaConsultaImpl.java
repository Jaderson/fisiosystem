package com.fisio.web.consulta.biomecanicaPalmilha.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.biomecanicaPalmilha.BiomecanicaPalmilhaFacade;
import com.fisio.framework.avaliacoes.BiomecanicaPalmilha;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.web.consulta.biomecanicaPalmilha.BiomecanicaPalmilhaConsulta;

@Component
public class BiomecanicaPalmilhaConsultaImpl extends LazyDataModel<BiomecanicaPalmilha> implements BiomecanicaPalmilhaConsulta{

	private static final long serialVersionUID = 1L;
	
	private BiomecanicaPalmilhaFacade biomecanicaPalmilhaFacade;
	
	@Autowired
	public BiomecanicaPalmilhaConsultaImpl(BiomecanicaPalmilhaFacade biomecanicaPalmilhaFacade) {
		this.biomecanicaPalmilhaFacade = biomecanicaPalmilhaFacade;
	}

	@Override
	public List<BiomecanicaPalmilha> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		BiomecanicaPalmilha biomecanicaPalmilha = new BiomecanicaPalmilha();
		Integer quantidadeRegistros = this.biomecanicaPalmilhaFacade.consultarQuantidadeRegistros(biomecanicaPalmilha, new ArrayList<Restricoes>(), biomecanicaPalmilha.getCampoOrderBy());
		List<BiomecanicaPalmilha> listaBiomecanicaPalmilha = this.biomecanicaPalmilhaFacade.consultarTodosPaginado(biomecanicaPalmilha, first, pageSize, new ArrayList<Restricoes>(), biomecanicaPalmilha.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaBiomecanicaPalmilha;
	}
}
