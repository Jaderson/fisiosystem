package com.fisio.web.consulta.categoria.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.categoria.CategoriaFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.web.consulta.categoria.CategoriaConsulta;

@Component
public class CategoriaConsultaImpl extends LazyDataModel<Categoria> implements CategoriaConsulta{

	private static final long serialVersionUID = 1L;
	
	private CategoriaFacade categoriaFacade;
	
	@Autowired
	public CategoriaConsultaImpl(CategoriaFacade categoriaFacade) {
		this.categoriaFacade = categoriaFacade;
	}

	@Override
	public List<Categoria> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Categoria categoria = new Categoria();
		Integer quantidadeRegistros = this.categoriaFacade.consultarQuantidadeRegistros(categoria, new ArrayList<Restricoes>(), categoria.getCampoOrderBy());
		List<Categoria> listaCategoria = this.categoriaFacade.consultarTodosPaginado(categoria, first, pageSize, new ArrayList<Restricoes>(), categoria.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaCategoria;
	}
}
