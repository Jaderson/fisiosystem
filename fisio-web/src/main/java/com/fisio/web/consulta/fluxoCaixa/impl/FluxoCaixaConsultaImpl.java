package com.fisio.web.consulta.fluxoCaixa.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.fluxoCaixa.FluxoCaixaFacade;
import com.fisio.framework.caixa.FluxoCaixa;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.consulta.fluxoCaixa.FluxoCaixaConsulta;

@Component
public class FluxoCaixaConsultaImpl extends LazyDataModel<FluxoCaixa> implements FluxoCaixaConsulta{

	private static final long serialVersionUID = 1L;
	
	private FluxoCaixaFacade fluxoCaixaFacade;
	private Usuario usuario;
	private Empresa empresa;
	
	@Autowired
	public FluxoCaixaConsultaImpl(FluxoCaixaFacade fluxoCaixaFacade) {
		this.fluxoCaixaFacade = fluxoCaixaFacade;
	}

	@Override
	public List<FluxoCaixa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		FluxoCaixa fluxoCaixa = new FluxoCaixa();
		Integer quantidadeRegistros = this.fluxoCaixaFacade.consultarQuantidadeRegistros(fluxoCaixa, new ArrayList<Restricoes>(), fluxoCaixa.getCampoOrderBy());
		List<FluxoCaixa> listaFluxoCaixa = this.fluxoCaixaFacade.consultarTodosPaginado(fluxoCaixa, first, pageSize, new ArrayList<Restricoes>(), fluxoCaixa.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaFluxoCaixa;
	}

	@Override
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
