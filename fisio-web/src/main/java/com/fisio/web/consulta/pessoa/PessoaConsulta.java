package com.fisio.web.consulta.pessoa;

import com.fisio.model.empresa.Empresa;

public interface PessoaConsulta {

	void setEmpresa(Empresa empresa);

}
