package com.fisio.web.consulta.configuracao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.configuracao.ConfiguracaoFacade;
import com.fisio.framework.configuracao.Configuracao;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.web.consulta.configuracao.ConfiguracaoConsulta;

@Component
public class ConfiguracaoConsultaImpl extends LazyDataModel<Configuracao> implements ConfiguracaoConsulta{

	private static final long serialVersionUID = 1L;
	
	private ConfiguracaoFacade configuracaoFacade;
	
	@Autowired
	public ConfiguracaoConsultaImpl(ConfiguracaoFacade configuracaoFacade) {
		this.configuracaoFacade = configuracaoFacade;
	}

	@Override
	public List<Configuracao> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		Configuracao configuracao = new Configuracao();
		Integer quantidadeRegistros = this.configuracaoFacade.consultarQuantidadeRegistros(configuracao, new ArrayList<Restricoes>(), null);
		List<Configuracao> listaConfiguracao = this.configuracaoFacade.consultarTodosPaginado(configuracao, first, pageSize, new ArrayList<Restricoes>(), null);
		setRowCount(quantidadeRegistros);	
		return listaConfiguracao;
	}
}
