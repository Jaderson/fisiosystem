package com.fisio.web.consulta.perfilPermissao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.facade.perfilPermissao.PerfilPermissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.web.consulta.perfilPermissao.PerfilPermissaoConsulta;

@Component
public class PerfilPermissaoConsultaImpl extends LazyDataModel<PerfilPermissao> implements PerfilPermissaoConsulta{

	private static final long serialVersionUID = 1L;
	
	private PerfilPermissaoFacade perfilPermissaoFacade;
	
	@Autowired
	public PerfilPermissaoConsultaImpl(PerfilPermissaoFacade perfilPermissaoFacade) {
		this.perfilPermissaoFacade = perfilPermissaoFacade;
	}

	@Override
	public List<PerfilPermissao> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		PerfilPermissao perfilPermissao = new PerfilPermissao();
		Integer quantidadeRegistros = this.perfilPermissaoFacade.consultarQuantidadeRegistros(perfilPermissao, new ArrayList<Restricoes>(), perfilPermissao.getCampoOrderBy());
		List<PerfilPermissao> listaPerfilPermissao = this.perfilPermissaoFacade.consultarTodosPaginado(perfilPermissao, first, pageSize, new ArrayList<Restricoes>(), perfilPermissao.getCampoOrderBy());
		setRowCount(quantidadeRegistros);	
		return listaPerfilPermissao;
	}
}
