package com.fisio.web.generico;

import static com.fisio.web.constantes.ConstantesUtilWeb.REGISTRO_ALTERADO_COM_SUCESSO;
import static com.fisio.web.constantes.ConstantesUtilWeb.REGISTRO_CADASTRADO_COM_SUCESSO;
import static com.fisio.web.constantes.ConstantesUtilWeb.REGISTRO_EXCLUIDO_COM_SUCESSO;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fisio.framework.facade.Facade;
import com.fisio.framework.security.authentication.FisioSystem;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.empresa.SetadorEmpresa;
import com.fisio.relatorio.GeradorRelatorioDinamico;
import com.fisio.web.bean.locale.UtilLocale;

public abstract class AbstractBean<E> implements GenericBean<E> {

	public E entidade;
	public List<E> listFiltrado;
	public String filtro;
	public Integer filtroNumero;
	
	private boolean detalharVisivel = false;
	private boolean cadastrarVisivel = false;
	private boolean focus = false;
	
	protected abstract void limparEntidade();	
	protected abstract E getEntidade();
	protected abstract Facade<E> getFacade();
	protected GeradorRelatorioDinamico<E> getRelatorio(){return null;};
	
	@PostConstruct 
	protected void init() {
		filtro();
	};
	
	protected void modificarEntidadeAntesDaOperacaoParaCadastrar(E entidade){}
	protected void modificarEntidadeAntesDaOperacaoParaAlterar(E entidade){}
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(E entidade){}
	
	public void fecharCadastro(){
		setCadastrarVisivel(false);
	}
	
	public void setSelecionar(E entidade) {
		setDetalharVisivel(true);
		setFocus(true);
		this.setEntidade(entidade);
	}

	public void setSelecionarAlterar(E entidade) {
		setCadastrarVisivel(true);
		setFocus(true);
		this.setEntidade(entidade);
	}
	
	public void novo() {
		limparEntidade();
		setCadastrarVisivel(true);
		setFocus(true);
	}
	
	public void setEntidade(E entidade) {
		this.entidade = entidade;
	}
	
	@Override
	public void cadastrar(final E entidade) {
		new VerificadorLancamentoException().tratarIhRelancarExcecaoSemLimparEntidade(new CommandBean() {

			public void execute() {
				modificarEntidadeAntesDeCadastrarEhOuAlterar(entidade);
				modificarEntidadeAntesDaOperacaoParaCadastrar(entidade);
				adicionarEmpresaSeNecessario(entidade);
				getFacade().cadastrar(entidade);
				filtro();
				limparEntidade();
				setCadastrarVisivel(false);
				setDetalharVisivel(false);
				lancarSucesso(REGISTRO_CADASTRADO_COM_SUCESSO);
			}

			private void adicionarEmpresaSeNecessario(E entidade) {
				if(entidade instanceof SetadorEmpresa) {
					((SetadorEmpresa) entidade).setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
				}
			}
		});
	}

	@Override
	public void alterar(final E entidade) {
		new VerificadorLancamentoException().tratarIhRelancarExcecaoSemLimparEntidade(new CommandBean() {

			public void execute() {
				modificarEntidadeAntesDeCadastrarEhOuAlterar(entidade);
				modificarEntidadeAntesDaOperacaoParaAlterar(entidade);
				getFacade().alterar(entidade);
				filtro();
				limparEntidade();
				setCadastrarVisivel(false);
				setDetalharVisivel(false);
				lancarSucesso(REGISTRO_ALTERADO_COM_SUCESSO);
			}
		});
	}

	@Override
	public void excluir(final E entidade) {
		new VerificadorLancamentoException().tratarIhRelancarExcecaoSemLimparEntidade(new CommandBean() {

			public void execute() {
				getFacade().excluir(entidade);
				filtro();
				limparEntidade();
				lancarSucesso(REGISTRO_EXCLUIDO_COM_SUCESSO);
			}
		});
	}
	
	public Boolean possuiPermissao(String permissao) {
		FisioSystem userStant = getUserFisioSystem();
		for (GrantedAuthority authority : userStant.getAuthorities()) {
			if (authority.getAuthority().equals(permissao)) {
				return true;
			}
		}
		return false;
	}
	
	public void gerarRelatorio(String tipoRelatorio) {
		GeradorRelatorioDinamico<E> relatorio = getRelatorio();
		relatorio.setTipoRelatorio(tipoRelatorio);
		relatorio.gerarRelatorio((HttpServletResponse) getContext().getExternalContext().getResponse());
	}
	
	@Override
	public void filtro(){
		List<E> listaPopulada = getFacade().filtro(getEntidade());
		setListFiltrado(listaPopulada);
	}
	
	public Date converteStringToDate() {
		String textoFiltro = "";
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();
		
		if (this.filtro != null) {
			textoFiltro = this.filtro.equals("__/__/____") ? null : this.filtro;
		}
		if (textoFiltro != "" && textoFiltro != null) {
			try {
				calendar.setTime(formatter.parse(this.filtro));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return calendar.getTime();
	}
	
	@Override
	public E consultarPorId() {
		return getFacade().consultarPorId(getEntidade());
	}

	@Override
	public List<E> consultarTodos() {
		return getFacade().consultarTodos(getEntidade());
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
	}
	
	private FisioSystem getUserFisioSystem() {
		UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
		FisioSystem userFisio = (FisioSystem) authentication.getPrincipal();
		return userFisio;
	}
	
	public Usuario getUsuarioLogado() {
		FisioSystem userFisio = getUserFisioSystem();
		return userFisio.getUsuario();
	}
	
	public FacesContext getContext() {
		return FacesContext.getCurrentInstance();
	}

	public HttpServletRequest getRequest() {
		return verificarContexto() ? (HttpServletRequest) getContext().getExternalContext().getRequest() : null; 
	}

	public HttpServletResponse getResponse() {
		return verificarContexto() ? (HttpServletResponse) getContext().getExternalContext().getResponse() : null;
	}
	
	public boolean isDetalharVisivel() {
		return detalharVisivel;
	}
	
	public void setDetalharVisivel(boolean detalharVisivel) {
		this.detalharVisivel = detalharVisivel;
	}
	
	public boolean isCadastrarVisivel() {
		return cadastrarVisivel;
	}
	
	public void setCadastrarVisivel(boolean cadastrarVisivel) {
		this.cadastrarVisivel = cadastrarVisivel;
	}

	public HttpSession getSession() {
		return (HttpSession) getContext().getExternalContext().getSession(false);
	}

	public void redirecionarParaTela(String tela) {
		try {
			getContext().getExternalContext().redirect(getContext().getExternalContext().getRequestContextPath() + tela);
		} catch (IOException e) {
			throw new RuntimeException("Erro ao redirecionar a tela", e);
		}
	}
	
	protected void lancarSucesso(String mensagem) {  
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getMensagemI18n(mensagem)));  
    }  
	
	protected void lancarInformacao(String mensagem) {  
		/*getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getMensagemI18n(mensagem)));*/
		if(mensagem.contains(";")) {
			String[] mensagens = mensagem.split(";");
			for (String mensagemDeErro : mensagens) {
				try {
					getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, UtilLocale.getMensagemI18n(mensagemDeErro)));
				} catch (IOException e) {
					e.printStackTrace();
				}  
			}
		} else {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getMensagemI18n(mensagem)));  
		}
	}  
  
	protected void lancarAlerta(String mensagem) {  
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, null, getMensagemI18n(mensagem)));  
    }  
  
	protected void lancarErro(String mensagem) {
		if(mensagem.contains(";")) {
			String[] mensagens = mensagem.split(";");
			for (String mensagemDeErro : mensagens) {
				try {
					getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, UtilLocale.getMensagemI18n(mensagemDeErro)));
				} catch (IOException e) {
					e.printStackTrace();
				}  
			}
		} else {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getMensagemI18n(mensagem)));  
		}
    }  

	protected void lancarErroFatal(String mensagem) {  
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, null, getMensagemI18n(mensagem)));  
    }
	
	protected void lancarSucessoRedirecionandoTelaComCallBack(String mensagem, String urlCallBack) {
		try {
			getContext().getExternalContext().redirect(urlCallBack);
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getMensagemI18n(mensagem)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void uploadImage(InputStream inputStream, String filePath, String fileName) {
		/*ImageAndTokenUtil.uploadImages(inputStream, filePath, fileName);*/
	}
	
	private boolean verificarContexto() {
		FacesContext context = getContext();
		return VerificadorUtil.naoEstaNulo(context) && VerificadorUtil.naoEstaNulo(context.getExternalContext());
	}
	
	public String getMensagemI18n(String chave){
		return getContext().getApplication().getResourceBundle(getContext(), "msg").getString(chave);
	}
	
	/*
	 * Classe privada para tratamento de exceção nos beans
	 */
	protected class VerificadorLancamentoException {

		public VerificadorLancamentoException() {}

		public void tratarIhRelancarExcecaoLimpandoEntidade(CommandBean command) {
			try {
				command.execute();
			} catch (Exception e) {
				setarEntidadeParaNuloParaOhContextoDeCadastrar();
				lancarErro(e.getMessage());
			}
		}
		
		public void tratarIhRelancarExcecaoSemLimparEntidade(CommandBean command) {
			try {
				command.execute();
			} catch (Exception e) {
				lancarErro(e.getMessage());
			}
		}

		private void setarEntidadeParaNuloParaOhContextoDeCadastrar() {
			setEntidade(null);
		}
	}
	
	public interface CommandBean {
		
		void execute();
	}
	
	/*Gets and Sets*/
	
	public List<E> getListFiltrado() {
		return listFiltrado;
	}

	public void setListFiltrado(List<E> listFiltrado) {
		this.listFiltrado = listFiltrado;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

	public Integer getFiltroNumero() {
		return filtroNumero;
	}

	public void setFiltroNumero(Integer filtroNumero) {
		this.filtroNumero = filtroNumero;
	}
	
	public boolean isFocus() {
		return focus;
	}
	
	public void setFocus(boolean focus) {
		this.focus = focus;
	}
}