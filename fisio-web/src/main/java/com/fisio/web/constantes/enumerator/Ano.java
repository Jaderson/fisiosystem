package com.fisio.web.constantes.enumerator;

public enum Ano {
	dezesseis(2016),
	desete(2017);

	private Integer value;

	private Ano() {}
	
	private Ano(Integer value) {
		this.value = value;
	}
	
	public Integer getValue() {
		return value;
	}
}