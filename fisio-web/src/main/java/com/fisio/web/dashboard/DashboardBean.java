package com.fisio.web.dashboard;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.fisio.facade.acompanhamentoFilho.AcompanhamentoFilhoFacade;
import com.fisio.facade.agenda.AgendaFacade;
import com.fisio.facade.fluxoCaixa.FluxoCaixaFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.caixa.TipoLancamento;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.security.authentication.FisioSystem;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.agenda.impl.TipoEspecialidadeDTO;
import com.fisio.web.constantes.enumerator.Ano;

@Component
@ManagedBean
@Scope("view")
public class DashboardBean{

	private BarChartModel fluxoCaixa;
	private FluxoCaixaFacade fluxoCaixaFacade;
	private PessoaFacade pessoaFacade;
	private AgendaFacade agendaFacade;
	private AcompanhamentoFilhoFacade acompanhamentoFilhoFacade;
	private HashMap<Integer, BigDecimal> dadosReceitaCaixa = new HashMap<Integer, BigDecimal>();
	private HashMap<Integer, BigDecimal> dadosDespesaCaixa = new HashMap<Integer, BigDecimal>();
	private String ano;
	
	@Autowired
	public DashboardBean(FluxoCaixaFacade fluxoCaixaFacade, PessoaFacade pessoaFacade,
			AgendaFacade agendaFacade, AcompanhamentoFilhoFacade acompanhamentoFilhoFacade) {
		this.fluxoCaixaFacade = fluxoCaixaFacade;
		this.pessoaFacade = pessoaFacade;
		this.agendaFacade = agendaFacade;
		this.acompanhamentoFilhoFacade = acompanhamentoFilhoFacade;
	}

	@PostConstruct
	public void init() {
		try {
			if(getAno() == null){
				Calendar c = Calendar.getInstance();
				setAno(Integer.toString(c.get(Calendar.YEAR)));
				
			}
			createAnimatedModels();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	
	public List<TipoEspecialidadeDTO>tipoConsultas(){
		List<TipoEspecialidadeDTO> listTipoEspecialidade = new ArrayList<TipoEspecialidadeDTO>();
		List<Object[]> results = agendaFacade.consultaTipoEspecialidade(pegarPrimerioDataMesCorrente(),pegarUltimaDataMesCorrente(), getUsuarioLogado().getEmpresa().getId());
		 Iterator<Object[]> ite = results.iterator();
		 while (ite.hasNext()) {
			 TipoEspecialidadeDTO  tipoEspecialidadeDTO = new TipoEspecialidadeDTO();
             Object[] result = (Object[]) ite.next();
             tipoEspecialidadeDTO.setNome((String) result[0]);
             tipoEspecialidadeDTO.setContador((Long) result[1]);
             listTipoEspecialidade.add(tipoEspecialidadeDTO);
		 }
		return listTipoEspecialidade;
	}
	
	public Integer consultaMes(){
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("falta", false));
		listaRestricoes.add(Restricoes.between("dataInicio", pegarPrimerioDataMesCorrente(), pegarUltimaDataMesCorrente()));
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		List<Agenda> listAgendaMes = agendaFacade.consultarTodos(new Agenda(), listaRestricoes, null);
		return listAgendaMes.size();
	}
	
	
	public List<Pessoa> aniversariateMes(){
		List<Pessoa> listAniversarioMes = pessoaFacade.consultarAniversariante();
		return listAniversarioMes;
	}
	
	public Integer cadastroEvolucaoMes(){
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.between("dataSecao", pegarPrimerioDataMesCorrente(), pegarUltimaDataMesCorrente()));
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		List<AcompanhamentoFilho> listEvolucaoMes = acompanhamentoFilhoFacade.consultarTodos(new AcompanhamentoFilho(), listaRestricoes, null);
		return listEvolucaoMes.size();
	}
	
	public Integer cadastroPacienteMes(){
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.between("criadoEm", pegarPrimerioDataMesCorrente(), pegarUltimaDataMesCorrente()));
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		List<Pessoa> listPacienteMes = pessoaFacade.consultarTodos(new Pessoa(), listaRestricoes, null);
		return listPacienteMes.size();
	}
	
	public int calculaIdade(Date dataNasc) {

	    Calendar dataNascimento = Calendar.getInstance();  
	    dataNascimento.setTime(dataNasc); 
	    Calendar hoje = Calendar.getInstance();  

	    int idade = hoje.get(Calendar.YEAR) - dataNascimento.get(Calendar.YEAR); 

	    if (hoje.get(Calendar.MONTH) < dataNascimento.get(Calendar.MONTH)) {
	      idade--;  
	    } 
	    else 
	    { 
	        if (hoje.get(Calendar.MONTH) == dataNascimento.get(Calendar.MONTH) && hoje.get(Calendar.DAY_OF_MONTH) < dataNascimento.get(Calendar.DAY_OF_MONTH)) {
	            idade--; 
	        }
	    }

	    return idade;
	}
	
	public Integer countFalt(){
		List<Agenda> listAniversarioMes = agendaFacade.consultarFaltaMes(pegarPrimerioDataMesCorrente(),pegarUltimaDataMesCorrente());
		return listAniversarioMes.size();
	}
	
	public List<Agenda> faltaMes(){
		return agendaFacade.consultarFaltaMes(pegarPrimerioDataMesCorrente(),pegarUltimaDataMesCorrente());
	}

	private Date pegarUltimaDataMesCorrente(){
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime( new Date() );
		         
		int dia = cal.getActualMaximum( Calendar.DAY_OF_MONTH );
		int mes = (cal.get(Calendar.MONDAY)+1);
		int ano = cal.get(Calendar.YEAR);
		         
		 System.out.println( dia+"/"+mes+"/"+ano ); 
		         
		try {
		    Date data = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).parse( dia+"/"+mes+"/"+ano + " " + "23:59:59");
		    return data;
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		return null;
	}
	
	private Date pegarPrimerioDataMesCorrente(){
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime( new Date() );
		         
		int dia = 1;
		int mes = (cal.get(Calendar.MONDAY)+1);
		int ano = cal.get(Calendar.YEAR);
		         
		System.out.println( dia+"/"+mes+"/"+ano ); 
		         
		try {
		    Date data = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).parse( dia+"/"+mes+"/"+ano + " " + "00:00:00");
		    return data;
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		return null;
	}
	
	private void createAnimatedModels() throws ParseException {
		fluxoCaixa = initBarModel();
		fluxoCaixa.setTitle("Fluxo de Caixa");
		fluxoCaixa.setAnimate(true);
		fluxoCaixa.setLegendPosition("ne");
		Axis yAxis = fluxoCaixa.getAxis(AxisType.Y);
		fluxoCaixa.getAxes().put(AxisType.X, new CategoryAxis("Mês"));
		yAxis.setMin(0);
		yAxis.setMax(60000);
	}

	private BarChartModel initBarModel() throws ParseException {
		BarChartModel model = new BarChartModel();

		ChartSeries receita = new ChartSeries();
		receita.setLabel("Receita");
		consultaFluxoReceita();
		consultaFluxoDespesas();
		for (Integer item : dadosReceitaCaixa.keySet()) {
			BigDecimal valor = dadosReceitaCaixa.get(item);
			String mes = receberMes(item);
				
			receita.set(mes, valor);
		}

		ChartSeries despesas = new ChartSeries();
		despesas.setLabel("Despesas");
		for (Integer item : dadosDespesaCaixa.keySet()) {
			BigDecimal valor = dadosDespesaCaixa.get(item);
			String mes = receberMes(item);
			despesas.set(mes, valor);
		}

		model.addSeries(receita);
		model.addSeries(despesas);

		return model;
	}

	private String receberMes(Integer item) {
		String mes = "";
		
		switch (item) {
		case 1:
			mes = "Janeiro";
			break;
		case 2:
			mes = "Fevereiro";
			break;
		case 3:
			mes = "Março";
			break;
		case 4:
			mes = "Abril";
			break;
		case 5:
			mes = "Maio";
			break;
		case 6:
			mes = "Junho";
			break;
		case 7:
			mes = "Julho";
			break;
		case 8:
			mes = "Agosto";
			break;
		case 9:
			mes = "Setembro";
			break;
		case 10:
			mes = "Outubro";
			break;
		case 11:
			mes = "Novembro";
			break;
		case 12:
			mes = "Dezembro";
			break;
		}
		return mes;
	}
	
	public void  consultaFluxoReceita() throws ParseException{
		for (int i = 1; i <= 12; i++) {
			String mes = "";
			if (i < 10) {
				mes = StringUtils.leftPad(Integer.toString(i), 2, "0");
			} else {
				mes = Integer.toString(i);
			}

			BigDecimal somaReceita = this.fluxoCaixaFacade.consultarDataLancamento(geraDataComPrimeiroDia(mes), geraDataComUltimoDia(mes), getUsuarioLogado().getEmpresa().getId(), TipoLancamento.Receita);
			dadosReceitaCaixa.put(i, somaReceita);
		}
	}
	
	public void  consultaFluxoDespesas() throws ParseException{
		for (int i = 1; i <= 12; i++) {
			String mes = "";
			if (i < 10) {
				mes = StringUtils.leftPad(Integer.toString(i), 2, "0");
			} else {
				mes = Integer.toString(i);
			}

			BigDecimal somaDespesas = this.fluxoCaixaFacade.consultarDataLancamento(geraDataComPrimeiroDia(mes), geraDataComUltimoDia(mes), getUsuarioLogado().getEmpresa().getId(), TipoLancamento.Despesa);
			dadosDespesaCaixa.put(i, somaDespesas);
		}
	}
	
	public Date geraDataComPrimeiroDia(String mes) throws ParseException{
		DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String ano = getAno() == null ? "2017" : getAno();
		String dataCompleta =  ano +"/"+ mes +"/"+ "01";
		Date date = (Date) formatter.parse(dataCompleta);
		return date;
	}
	
	public Date geraDataComUltimoDia(String mes) throws ParseException{
		Calendar c = Calendar.getInstance();  
		c.set(Calendar.MONTH, Integer.parseInt(mes) - 1);  
		c.set(Calendar.YEAR, 2017);    
		int ultimodia = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String ano = getAno() == null ? "2017" : getAno();
		String dataCompleta = ano +"/"+ mes +"/"+ Integer.toString(ultimodia);
		Date date = (Date) formatter.parse(dataCompleta);
		return date;
	}
	
	public SelectItem[] getListaAno() {
		int i = 0;
		SelectItem[] items = new SelectItem[Ano.values().length];
		for(Ano ano: Ano.values()) {
			items[i++] = new SelectItem(ano, Integer.toString(ano.getValue()));
		}
		
		return items;
	} 
	
	public BarChartModel getFluxoCaixa() {
		return fluxoCaixa;
	}

	public void setFluxoCaixa(BarChartModel fluxoCaixa) {
		this.fluxoCaixa = fluxoCaixa;
	}
	
	public Usuario getUsuarioLogado() {
		FisioSystem userFisio = getUserFisioSystem();
		return userFisio.getUsuario();
	}
	
	private FisioSystem getUserFisioSystem() {
		UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
		FisioSystem userFisio = (FisioSystem) authentication.getPrincipal();
		return userFisio;
	}
	
	public void recebeAno(){
		init();
	}

	public String getAno() {
		return ano;
	}
	
	public void setAno(String ano) {
		this.ano = ano;
	}
}
