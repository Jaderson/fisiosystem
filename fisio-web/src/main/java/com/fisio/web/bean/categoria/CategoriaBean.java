package com.fisio.web.bean.categoria;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.categoria.CategoriaFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.relatorio.GeradorRelatorioDinamico;
import com.fisio.relatorio.categoria.CategoriaRelatorioDinamico;
import com.fisio.web.consulta.categoria.CategoriaConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class CategoriaBean extends AbstractBean<Categoria>{

	private CategoriaFacade categoriaFacade;
	private CategoriaConsulta consulta;
	
	private CategoriaRelatorioDinamico categoriaRelatorioDinamico;
	
	@Autowired
	public CategoriaBean(CategoriaFacade categoriaFacade, CategoriaConsulta consulta,
			CategoriaRelatorioDinamico categoriaRelatorioDinamico) {
		this.categoriaFacade = categoriaFacade;
		this.consulta = consulta;
		this.categoriaRelatorioDinamico = categoriaRelatorioDinamico;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Categoria());
	}
	
	@Override
	public void filtro() {
		Categoria categoriaConsulta = new Categoria();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.categoriaFacade.filtro(listaRestricoes, categoriaConsulta, null));
	}
	
	@Override
	public Categoria getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Categoria();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Categoria> getFacade() {
		return this.categoriaFacade;
	}
	
	public CategoriaConsulta getConsulta() {
		return consulta;
	}
	
	@Override
	protected GeradorRelatorioDinamico<Categoria> getRelatorio() {
		return this.categoriaRelatorioDinamico;
	}
}
