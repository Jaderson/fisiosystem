package com.fisio.web.bean.authentication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.usuario.UsuarioFacade;
import com.fisio.facade.usuarioPerfil.UsuarioPerfilFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.security.authentication.FisioSystem;
import com.fisio.framework.security.exception.FisioSystemAuthenticationException;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.web.bean.locale.UtilLocale;

@Component
@ManagedBean
@Scope("request")
@Transactional(propagation = Propagation.REQUIRED)
public class LoginBean {
	
	@Autowired UsuarioFacade usuarioFacade;
	@Autowired UsuarioPerfilFacade usuarioPerfilFacade;
	Usuario entidade;
	String novaSenha;
	
	public String doLogin() throws ServletException, IOException {
		continuarRequestParaSpringSecurity();
		/*setUltimoAcesso(UsernamePasswordAuthenticationToken());
		try {
			AniversarianteDiaJob.inicia();
		} catch (Exception e) {
			e.printStackTrace();
		} */
		return null;
	}
		
	private void continuarRequestParaSpringSecurity() throws ServletException, IOException {	
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			dispatchToUrlWithContext("/j_spring_security_check", context);
		} catch (FisioSystemAuthenticationException e) {
			dispatchToUrlWithContext("/", context);
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,null, e.getMessage()));
		}
	}
	
	public String logout() {
		SecurityContextHolder.clearContext();
		return "login";
	}
	
	public String getUserName() throws IOException {
		try {
			FisioSystem userFisioSystem = UsernamePasswordAuthenticationToken();
			return userFisioSystem.getUsuario().getPessoa().getNome();
		} catch (ClassCastException e) {
			return UtilLocale.getMensagemI18n("nao_autenticado");
		}
	}

	private FisioSystem UsernamePasswordAuthenticationToken() {
		UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
		FisioSystem userFisioSystem = (FisioSystem) authentication.getPrincipal();
		return userFisioSystem;
	}

/*	private void setUltimoAcesso(FisioSystem userFisioSystem) {
		List<UsuarioPerfil> listaUsuarioPerfil = recuperarPerfilUsuario(userFisioSystem.getUsuario());
		getEntidade().setUltimoAcesso(new Date());
		getEntidade().setPerfils(new HashSet<UsuarioPerfil>());
		for (UsuarioPerfil perfil : listaUsuarioPerfil) {
			UsuarioPerfil usuarioPerfil = new UsuarioPerfil(userFisioSystem.getUsuario(), perfil.getPerfil());
			getEntidade().getPerfils().add(usuarioPerfil);
		}
		this.usuarioFacade.alterar(userFisioSystem.getUsuario());
	}  */
	
	private List<UsuarioPerfil> recuperarPerfilUsuario(Usuario entidade) {
		List<Restricoes> listaRestricoes =  new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("usuario.id", entidade.getId()));
		return this.usuarioPerfilFacade.consultarTodos(new UsuarioPerfil(), listaRestricoes, null);
	}
	
	public void alterarSenha() throws IOException {
		if(this.novaSenha == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, UtilLocale.getMensagemI18n("erro_o_campo_senha_nao_pode_ser_nulo_ou_vazio")));
		}else{
			Usuario usuario = this.usuarioFacade.consultarPorId(UsernamePasswordAuthenticationToken().getUsuario().getId());
			String senhaNova = new Md5PasswordEncoder().encodePassword(this.novaSenha, null);
			usuario.setSenha(senhaNova);
			usuario.setTokenAcesso(senhaNova);
			usuario.setAlterarSenha(true);
			usuario.setPessoa(UsernamePasswordAuthenticationToken().getUsuario().getPessoa());
			List<UsuarioPerfil> listaUsuarioPerfil = recuperarPerfilUsuario(UsernamePasswordAuthenticationToken().getUsuario());
			usuario.setPerfils(new HashSet<UsuarioPerfil>());
			for (UsuarioPerfil perfil : listaUsuarioPerfil) {
				UsuarioPerfil usuarioPerfil = new UsuarioPerfil(usuario, perfil.getPerfil());
				getEntidade().getPerfils().add(usuarioPerfil);
			}
			this.usuarioFacade.alterar(usuario);
		}
	}

	private void dispatchToUrlWithContext(String url, ExternalContext context)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = ((ServletRequest) context.getRequest())
				.getRequestDispatcher(url);
		dispatcher.forward((ServletRequest) context.getRequest(),
				(ServletResponse) context.getResponse());
		FacesContext.getCurrentInstance().responseComplete();
	}
	
	public Usuario getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Usuario();
		}
		return entidade;
	}
	
	public void setEntidade(Usuario entidade) {
		this.entidade = entidade;
	}
	
	public String getNovaSenha() {
		return novaSenha;
	}
	
	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
}
