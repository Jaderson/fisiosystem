package com.fisio.web.bean.usuario;

import static com.fisio.web.constantes.ConstantesUtilWeb.REGISTRO_CADASTRADO_COM_SUCESSO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Component;

import com.fisio.email.EnviadorEmail;
import com.fisio.facade.perfil.PerfilFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.facade.usuario.UsuarioFacade;
import com.fisio.facade.usuarioPerfil.UsuarioPerfilFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.relatorio.GeradorRelatorioDinamico;
import com.fisio.relatorio.usuario.UsuarioRelatorioDinamico;
import com.fisio.web.consulta.usuario.UsuarioConsulta;
import com.fisio.web.converter.perfil.PerfilConversor;
import com.fisio.web.converter.pessoa.PessoaConversor;
import com.fisio.web.converter.simnaoparaboolean.SimNaoParaBooleanConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class UsuarioBean extends AbstractBean<Usuario>{

	private UsuarioFacade usuarioFacade;
	private PessoaFacade pessoaFacade;
	private PerfilFacade perfilFacade;
	private UsuarioPerfilFacade usuarioPerfilFacade;
	private UsuarioConsulta consulta;
	
	private PessoaConversor pessoaConversor;
	private SimNaoParaBooleanConversor conversorSimNao;
	
	private EnviadorEmail enviadorEmail;
	
	private DualListModel<Perfil> listaPerfisDoUsuario;
	private PerfilConversor perfilConversor;
	
	private UsuarioRelatorioDinamico usuarioRelatorioDinamico;
	
	private boolean ativo;
	
	@Autowired
	public UsuarioBean(UsuarioFacade usuarioFacade, PessoaFacade pessoaFacade, UsuarioConsulta consulta, 
			PerfilFacade perfilFacade, UsuarioPerfilFacade usuarioPerfilFacade,	EnviadorEmail enviadorEmail,
			UsuarioRelatorioDinamico usuarioRelatorioDinamico) {
		this.usuarioFacade = usuarioFacade;
		this.pessoaFacade = pessoaFacade;
		this.perfilFacade = perfilFacade;
		this.usuarioPerfilFacade = usuarioPerfilFacade;
		this.consulta = consulta;
		this.usuarioRelatorioDinamico = usuarioRelatorioDinamico;
		this.enviadorEmail = enviadorEmail;
		this.pessoaConversor = new PessoaConversor();
		this.conversorSimNao = new SimNaoParaBooleanConversor();
		this.perfilConversor = new PerfilConversor();
		this.consulta.setEmpresaUsuarioLogado(getUsuarioLogado().getPessoa().getEmpresa());
		inicializarDualListModel();
	}
	
	@Override
	public void novo() {
		super.novo();
	}
	
	@Override
	public void cadastrar(Usuario entidade) {
		if(entidade.getId() != null){
			modificarEntidadeAntesDeCadastrarEhOuAlterar(entidade);
		}
		
		String senha = gerarSenhaAutomatica();
		encriptarSenha(senha);
		getFacade().cadastrar(entidade);
		enviadorEmail.enviarEmail(getEntidade(),senha);
		cadastrarUsuarioPerfil(entidade);
		filtro();
		limparEntidade();
		setCadastrarVisivel(false);
		setDetalharVisivel(false);
		lancarSucesso(REGISTRO_CADASTRADO_COM_SUCESSO);
	}
	
	private void cadastrarUsuarioPerfil(Usuario entidade){
		for (Perfil perfil : getListaPerfisDoUsuario().getTarget()) {
			UsuarioPerfil usuarioPerfil = new UsuarioPerfil(getEntidade(), perfil);
			this.usuarioPerfilFacade.cadastrar(usuarioPerfil);
		}
	}
	
	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(Usuario entidade) {
		getEntidade().setPerfils(new HashSet<UsuarioPerfil>());
		for (Perfil perfil : getListaPerfisDoUsuario().getTarget()) {
			UsuarioPerfil usuarioPerfil = new UsuarioPerfil(getEntidade(), perfil);
			getEntidade().getPerfils().add(usuarioPerfil);
		}
	}
	
	@Override
	public void excluir(Usuario entidade) {
		entidade.setAtivo(SimNao.Não);
		getFacade().alterar(entidade);
	}
	
	public void encriptarSenha(String senha) {
		String senhaEncriptada = new Md5PasswordEncoder().encodePassword(senha, null);
		entidade.setSenha(senhaEncriptada);
		entidade.setTokenAcesso(senhaEncriptada);
	}

	@Override
	public void setSelecionarAlterar(Usuario entidade) {
		super.setSelecionarAlterar(entidade);
		inicializarDualListModel();
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
	
	public void esqueceuSenha(Usuario entidade) {
		Usuario usuarioLogado = new Usuario();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("email", entidade.getPessoa().getEmail()));
		Usuario usuarioConsultado = this.usuarioFacade.consultarEntidade(usuarioLogado, listaRestricoes);
		if(VerificadorUtil.naoEstaNulo(usuarioConsultado)){
			modificarEntidadeAntesDaOperacaoParaAlterar(usuarioConsultado);
			getFacade().alterar(usuarioConsultado);
		}else{
			lancarErro("erro_email_nao_confere");
			return;
		}
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Usuario());
	}
	
	@Override
	public void filtro() {
		Usuario usuarioConsulta = new Usuario();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("pessoa.empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("login", this.filtro));
		}
		setListFiltrado(this.usuarioFacade.filtro(listaRestricoes, usuarioConsulta, null));
	}
	
	@Override
	public Usuario getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Usuario();
		}
		return entidade;
	}
	
	private String gerarSenhaAutomatica() {
		String[] carct = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
				"m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
				"y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
				"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
				"W", "X", "Y", "Z" };
		String senha = "";

		for (int x = 0; x < 10; x++) {
			int j = (int) (Math.random() * carct.length);
			senha += carct[j];
		}
		return senha;
	}
	
	@Override
	protected Facade<Usuario> getFacade() {
		return this.usuarioFacade;
	}
	
	public UsuarioConsulta getConsulta() {
		return consulta;
	}
	
	public List<ItemCombo> getListaPessoas() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Pessoa pessoaConsulta = new Pessoa();
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Sim));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		if (VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa().getId()));
		}

		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		
		for (Pessoa pessoa : listaPessoas) {
				listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
		}
		return listaItemCombo;
	} 
	
	public PessoaConversor getPessoaConversor() {
		return pessoaConversor;
	}
	
	public SimNaoParaBooleanConversor getConversorSimNao() {
		return conversorSimNao;
	}

	public void setAtivo(boolean ativo) {
		entidade.setAtivo(ativo ? SimNao.Sim : SimNao.Não);
		this.ativo = ativo;
	}
	
	public boolean isAtivo() {
		return SimNao.Sim.equals(entidade.getAtivo()) ? true : false;
	}
	
	public DualListModel<Perfil> getListaPerfisDoUsuario() {
		return listaPerfisDoUsuario;
	}
	
	public void setListaPerfisDoUsuario(
			DualListModel<Perfil> listaPerfisDoUsuario) {
		this.listaPerfisDoUsuario = listaPerfisDoUsuario;
	}
	
	public PerfilConversor getPerfilConversor() {
		return perfilConversor;
	}
	
	private void inicializarDualListModel() {
		List<Perfil> listaPerfil = this.perfilFacade.consultarTodos(new Perfil());
		
		List<Perfil> perfisDoUsuario = recuperarPerfisDoUsuario(getEntidade().getId());
		listaPerfil.removeAll(perfisDoUsuario);

		listaPerfisDoUsuario = new DualListModel<Perfil>(listaPerfil, perfisDoUsuario);
	}

	private List<Perfil> recuperarPerfisDoUsuario(Integer id) {
		List<Perfil> listaPerfil = new ArrayList<Perfil>();
		if(VerificadorUtil.naoEstaNulo(getEntidade().getId())) {
			List<Restricoes> listaRestricoesUsuarioPerfil = new ArrayList<Restricoes>();
			listaRestricoesUsuarioPerfil.add(Restricoes.igual("usuario.id", getEntidade().getId()));		
			List<UsuarioPerfil> listaUsuarioPerfil = this.usuarioPerfilFacade.
					consultarTodos(new UsuarioPerfil(), listaRestricoesUsuarioPerfil, null);
			for(UsuarioPerfil usuarioPerfil : listaUsuarioPerfil) {
				listaPerfil.add(usuarioPerfil.getPerfil());
			}			
		}
		
		return listaPerfil;
	}
	
	@Override
	protected GeradorRelatorioDinamico<Usuario> getRelatorio() {
		return this.usuarioRelatorioDinamico;
	}
}
