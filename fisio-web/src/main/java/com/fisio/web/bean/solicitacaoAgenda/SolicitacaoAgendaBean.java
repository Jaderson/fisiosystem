package com.fisio.web.bean.solicitacaoAgenda;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.agenda.AgendaFacade;
import com.fisio.facade.solicitacaoAgenda.SolicitacaoAgendaFacade;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.solicitacaoAgenda.SolicitacaoAgendaConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class SolicitacaoAgendaBean extends AbstractBean<SolicitacaoAgenda>{

	private SolicitacaoAgendaFacade solicitacaoAgendaFacade;
	private SolicitacaoAgendaConsulta consulta;
	private AgendaFacade agendaFacade;
	private List<SolicitacaoAgenda> listaAceito;
	private List<SolicitacaoAgenda> listaRecusado;
	
	/*private SolicitacaoAgendaRelatorioDinamico SolicitacaoAgendaRelatorioDinamico;*/
	
	@Autowired
	public SolicitacaoAgendaBean(SolicitacaoAgendaFacade solicitacaoAgendaFacade, SolicitacaoAgendaConsulta consulta,
			AgendaFacade agendaFacade/*, SolicitacaoAgendaRelatorioDinamico solicitacaoAgendaRelatorioDinamico*/) {
		this.solicitacaoAgendaFacade = solicitacaoAgendaFacade;
		this.consulta = consulta;
		this.agendaFacade = agendaFacade;
		/*this.solicitacaoAgendaRelatorioDinamico = solicitacaoAgendaRelatorioDinamico*/
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new SolicitacaoAgenda());
	}
	
	@Override
	public void setSelecionarAlterar(SolicitacaoAgenda entidade) {
		entidade.setConfirmado("N");
		this.solicitacaoAgendaFacade.alterar(entidade);
		atualizarDataTable();
		filtro();
	}
	
	public void confirmarSolicitacao(SolicitacaoAgenda entidade){
		Agenda agenda = new Agenda();
		agenda.setPessoa(entidade.getPessoa());
		agenda.setFuncionario(entidade.getFuncionario());
		agenda.setEmpresa(entidade.getEmpresa());
		agenda.setEspecialidade(entidade.getEspecialidade());
		agenda.setDataInicio(entidade.getDataInicio());
		agenda.setDataFim(entidade.getDataFim());
		agenda.setHorario(entidade.getHorario());
		agenda.setFalta(false);
		agenda.setDiaTodo(false);
		agenda.setCancelou(false);
		agenda.setSolicitacaoAgenda(entidade);
		
		if(VerificadorUtil.naoEstaNulo(agenda)){
			this.agendaFacade.cadastrar(agenda);
			entidade.setConfirmado("S");
			this.solicitacaoAgendaFacade.alterar(entidade);
		}
		atualizarDataTable();
		filtro();
	}
	
	public void reabrirSolicitacao(SolicitacaoAgenda entidade){
		entidade.setConfirmado(null);
		this.solicitacaoAgendaFacade.alterar(entidade);
		atualizarDataTable();
		filtro();
	}
	
	public void atualizarDataTable(){
		listAceito();
		listRecusado();
	}
	
	public void listAceito(){
		SolicitacaoAgenda solicitacaoAgendaConsulta = new SolicitacaoAgenda();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("confirmado", "S"));
		setListaAceito(this.solicitacaoAgendaFacade.consultarTodosDesc(solicitacaoAgendaConsulta, listaRestricoes, solicitacaoAgendaConsulta.getCampoOrderBy()));
	}
	
	public void listRecusado(){
		SolicitacaoAgenda solicitacaoAgendaConsulta = new SolicitacaoAgenda();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("confirmado", "N"));
		setListaRecusado(this.solicitacaoAgendaFacade.consultarTodosDesc(solicitacaoAgendaConsulta, listaRestricoes, solicitacaoAgendaConsulta.getCampoOrderBy()));
	}
	
	@Override
	public void filtro() {
		SolicitacaoAgenda solicitacaoAgendaConsulta = new SolicitacaoAgenda();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.isNull("confirmado"));
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("pessoa.nome", this.filtro));
		}
		setListFiltrado(this.solicitacaoAgendaFacade.filtro(listaRestricoes, solicitacaoAgendaConsulta, solicitacaoAgendaConsulta.getCampoOrderBy()));
		atualizarDataTable();
	}
	
	@Override
	public SolicitacaoAgenda getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new SolicitacaoAgenda();
		}
		return entidade;
	}
	
	@Override
	protected Facade<SolicitacaoAgenda> getFacade() {
		return this.solicitacaoAgendaFacade;
	}
	
	public SolicitacaoAgendaConsulta getConsulta() {
		return consulta;
	}

	public List<SolicitacaoAgenda> getListaAceito() {
		return listaAceito;
	}

	public void setListaAceito(List<SolicitacaoAgenda> listaAceito) {
		this.listaAceito = listaAceito;
	}

	public List<SolicitacaoAgenda> getListaRecusado() {
		return listaRecusado;
	}

	public void setListaRecusado(List<SolicitacaoAgenda> listaRecusado) {
		this.listaRecusado = listaRecusado;
	}
	
	/*@Override
	protected GeradorRelatorioDinamico<SolicitacaoAgenda> getRelatorio() {
		return this.solicitacaoAgendaRelatorioDinamico;
	}*/ 
}
