package com.fisio.web.bean.especialidade;

import static com.fisio.web.constantes.ConstantesUtilWeb.REGISTRO_EXCLUIDO_COM_SUCESSO;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.especialidade.EspecialidadeFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.SimNao;
import com.fisio.relatorio.GeradorRelatorioDinamico;
import com.fisio.relatorio.especialidade.EspecialidadeRelatorioDinamico;
import com.fisio.web.consulta.especialidade.EspecialidadeConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class EspecialidadeBean extends AbstractBean<Especialidade>{

	private EspecialidadeFacade especialidadeFacade;
	private EspecialidadeConsulta consulta;
	
	private EspecialidadeRelatorioDinamico especialidadeRelatorioDinamico;
	
	@Autowired
	public EspecialidadeBean(EspecialidadeFacade especialidadeFacade, EspecialidadeConsulta consulta,
			EspecialidadeRelatorioDinamico especialidadeRelatorioDinamico) {
		this.especialidadeFacade = especialidadeFacade;
		this.consulta = consulta;
		this.especialidadeRelatorioDinamico = especialidadeRelatorioDinamico;
	}
	
	@Override
	protected void modificarEntidadeAntesDaOperacaoParaCadastrar(Especialidade entidade) {
		getEntidade().setAtivo(SimNao.Sim);
		super.modificarEntidadeAntesDaOperacaoParaCadastrar(entidade);
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
	
	@Override
	public void excluir(Especialidade entidade) {
		entidade.setAtivo(SimNao.Não);
		getFacade().alterar(entidade);
		filtro();
		limparEntidade();
		lancarSucesso(REGISTRO_EXCLUIDO_COM_SUCESSO);
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Especialidade());
	}
	
	@Override
	public void filtro() {
		Especialidade especialidadeConsulta = new Especialidade();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.especialidadeFacade.filtro(listaRestricoes, especialidadeConsulta, null));
	}
	
	@Override
	public Especialidade getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Especialidade();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Especialidade> getFacade() {
		return this.especialidadeFacade;
	}
	
	public EspecialidadeConsulta getConsulta() {
		return consulta;
	}
	
	@Override
	protected GeradorRelatorioDinamico<Especialidade> getRelatorio() {
		return this.especialidadeRelatorioDinamico;
	}
}
