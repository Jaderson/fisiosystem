package com.fisio.web.bean.formaPagamento;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.formaPagamento.FormaPagamentoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.formaPagamento.FormaPagamentoConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class FormaPagamentoBean extends AbstractBean<FormaPagamento>{

	private FormaPagamentoFacade formaPagamentoFacade;
	private FormaPagamentoConsulta consulta;
	
	/*private FormaPagamentoRelatorioDinamico FormaPagamentoRelatorioDinamico;*/
	
	@Autowired
	public FormaPagamentoBean(FormaPagamentoFacade formaPagamentoFacade, FormaPagamentoConsulta consulta) {
		this.formaPagamentoFacade = formaPagamentoFacade;
		this.consulta = consulta;
		/*this.formaPagamentoRelatorioDinamico = formaPagamentoRelatorioDinamico*/
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new FormaPagamento());
	}
	
	@Override
	public void filtro() {
		FormaPagamento formaPagamentoConsulta = new FormaPagamento();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.formaPagamentoFacade.filtro(listaRestricoes, formaPagamentoConsulta, null));
	}
	
	@Override
	public FormaPagamento getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new FormaPagamento();
		}
		return entidade;
	}
	
	@Override
	protected Facade<FormaPagamento> getFacade() {
		return this.formaPagamentoFacade;
	}
	
	public FormaPagamentoConsulta getConsulta() {
		return consulta;
	}
	
	/*@Override
	protected GeradorRelatorioDinamico<FormaPagamento> getRelatorio() {
		return this.formaPagamentoRelatorioDinamico;
	}*/
}
