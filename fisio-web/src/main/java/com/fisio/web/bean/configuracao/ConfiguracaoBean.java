package com.fisio.web.bean.configuracao;

import static com.fisio.web.constantes.ConstantesUtilWeb.REGISTRO_CADASTRADO_COM_SUCESSO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.configuracao.ConfiguracaoFacade;
import com.fisio.facade.dataFalta.DataFaltaFacade;
import com.fisio.facade.horario.HorarioFacade;
import com.fisio.facade.horarioDisponivel.HorarioDisponivelFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.agenda.DataFalta;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.configuracao.Configuracao;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.DateFormatterUtil;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.empresa.SetadorEmpresa;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.repository.dataFalta.DataFaltaRepository;
import com.fisio.web.consulta.configuracao.ConfiguracaoConsulta;
import com.fisio.web.converter.dataFalta.DataFaltaConversor;
import com.fisio.web.converter.horario.HorarioConversor;
import com.fisio.web.converter.pessoa.PessoaConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class ConfiguracaoBean extends AbstractBean<Configuracao>{

	private ConfiguracaoFacade configuracaoFacade;
	private ConfiguracaoConsulta consulta;
	private PessoaConversor pessoaConversor;
	private PessoaFacade pessoaFacade;
	private Date dataFalta;
	private DataFaltaFacade dataFaltaFacade;
	private DataFaltaRepository dataFaltaRepository;
	private DataFaltaConversor dataFaltaConversor;
	private DualListModel<DataFalta> listaDataFaltaSalvas;
	private DualListModel<Horario> listaHorario;
	private HorarioFacade horarioFacade;
	private HorarioConversor horarioConversor;
	private HorarioDisponivelFacade horarioDisponivelFacade;
	private TreeNode listaDataFaltaConfiguracao;
	private boolean dataAtivo;
	private TreeNode[] selectTreeNode;
	private boolean isProcesso = true;
	
	
	/*private ConfiguracaoRelatorioDinamico ConfiguracaoRelatorioDinamico;*/
	
	@Autowired
	public ConfiguracaoBean(ConfiguracaoFacade configuracaoFacade, ConfiguracaoConsulta consulta,
			PessoaFacade pessoaFacade, DataFaltaFacade dataFaltaFacade, HorarioFacade horarioFacade,
			HorarioDisponivelFacade horarioDisponivelFacade, DataFaltaRepository dataFaltaRepository) {
		this.configuracaoFacade = configuracaoFacade;
		this.consulta = consulta;
		this.pessoaFacade = pessoaFacade;
		this.pessoaConversor = new PessoaConversor();
		this.dataFaltaConversor = new DataFaltaConversor();
		this.horarioConversor = new HorarioConversor();
		this.dataFaltaFacade = dataFaltaFacade;
		this.horarioFacade = horarioFacade;
		this.horarioDisponivelFacade = horarioDisponivelFacade;
		this.dataFaltaRepository = dataFaltaRepository;
		inicializarDualListModel();
		inicializarDualListModelHorario();
		createTree();
		/*this.configuracaoRelatorioDinamico = configuracaoRelatorioDinamico*/
	}

	private void inicializarDualListModelHorario() {
		if(VerificadorUtil.naoEstaNulo(getEntidade().getId())){
			List<Horario> listHorario = this.horarioFacade.consultarTodos(new Horario());
			setListaHorario(new DualListModel<Horario>(listHorario, new ArrayList<Horario>()));
		}else{
			setListaHorario(new DualListModel<Horario>());
		}
	}

	private void inicializarDualListModel() {
		if(VerificadorUtil.naoEstaNulo(getEntidade()) && 
				VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())){
			List<DataFalta> listaDataFalta = this.dataFaltaRepository.consultarDatasPorConfiguracao(getEntidade());
			setListaDataFaltaSalvas(new DualListModel<DataFalta>(listaDataFalta, new ArrayList<DataFalta>()));		
		}else{
			setListaDataFaltaSalvas(new DualListModel<DataFalta>());
		}
	}

	public void incluirData(Configuracao entidade){
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", entidade.getId()));
		if(VerificadorUtil.estaNulo(this.configuracaoFacade.consultarEntidade(entidade, listaRestricoes))){
			lancarErro("erro_favor_salvar_uma_configuração_para_esse_usuario_primeiro");
			return;
		}
		
		if(VerificadorUtil.naoEstaNulo(getDataFalta())){
			DataFalta dataFalta = new DataFalta(getDataFalta(),getEntidade(),getUsuarioLogado().getEmpresa());
			this.dataFaltaFacade.cadastrar(dataFalta);
			setDataFalta(null);
			inicializarDualListModel();
		}
	}
	
	@Override
	public void cadastrar(Configuracao entidade) {
		modificarEntidadeAntesDeCadastrarEhOuAlterar(entidade);
		modificarEntidadeAntesDaOperacaoParaCadastrar(entidade);
		adicionarEmpresaSeNecessario(entidade);
		if(this.isProcesso){
			getFacade().cadastrar(entidade);
			limparEntidade();
			setCadastrarVisivel(false);
			setDetalharVisivel(false);
			lancarSucesso(REGISTRO_CADASTRADO_COM_SUCESSO);
		}
		createTree();
		this.isProcesso = true;
	}
	
	private void adicionarEmpresaSeNecessario(Configuracao entidade) {
		if(entidade instanceof SetadorEmpresa) {
			((SetadorEmpresa) entidade).setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
		}
	}
	
	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(Configuracao entidade) {
		
		if(!getListaDataFaltaSalvas().getTarget().isEmpty()){
			DataFalta consultaDataFalta = null;
			for (DataFalta data : getListaDataFaltaSalvas().getTarget()) {
				List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
				listaRestricoes.add(Restricoes.igual("dataFalta", data.getDataFalta()));
				listaRestricoes.add(Restricoes.igual("configuracao.id", getEntidade().getId()));
				List<DataFalta> listaDataFalta = this.dataFaltaFacade.consultarTodos(new DataFalta(), listaRestricoes, null);
				if(listaDataFalta.size() > 1){
					for (DataFalta dataFalta : listaDataFalta) {
						List<Restricoes> listaRestricoesHorario = new ArrayList<Restricoes>();
						listaRestricoesHorario.add(Restricoes.igual("dataFalta.id", dataFalta.getId()));
						List<HorarioDisponivel> listHorarioDisponivel = this.horarioDisponivelFacade
								.consultarTodos(new HorarioDisponivel(), listaRestricoesHorario, null);
						if(!listHorarioDisponivel.isEmpty()){
							consultaDataFalta = dataFalta;
						}else{
							this.dataFaltaFacade.excluir(dataFalta);
						}
					}
				}else{
					consultaDataFalta = listaDataFalta.get(0);
				}
				
				consultaDataFalta.setHorarioDisponivel(new HashSet<HorarioDisponivel>());
				for (Horario horario : getListaHorario().getTarget()) {
					List<Restricoes> listaRestricoesHorario = new ArrayList<Restricoes>();
					listaRestricoesHorario.add(Restricoes.igual("horario.id", horario.getId()));
					listaRestricoesHorario.add(Restricoes.igual("dataFalta.id", consultaDataFalta.getId()));
					HorarioDisponivel horarioEntidade = this.horarioDisponivelFacade.consultarEntidade(new HorarioDisponivel(), listaRestricoesHorario);
					if(VerificadorUtil.estaNulo(horarioEntidade)){
						HorarioDisponivel horarioDisponivel = new HorarioDisponivel(horario,
								consultaDataFalta);
						consultaDataFalta.getHorarioDisponivel().add(horarioDisponivel);
						this.horarioDisponivelFacade.cadastrar(horarioDisponivel);
						this.dataFaltaFacade.alterar(consultaDataFalta);
					}else{
						lancarErro("erro_horario_ja_salvo_para_esse_dia_e_horario");
						this.isProcesso = false;
						return;
					}
				}
			}
		}else if(getListaDataFaltaSalvas().getTarget().isEmpty() && !getListaHorario().getTarget().isEmpty()){
			lancarErro("erro_favor_selecionar_uma_data_para_relacionar_horario");
			this.isProcesso = false;
			return;
		}
		
		inicializarDualListModel();
		inicializarDualListModelHorario();
		alteraConfiguracaoSeExistir(entidade);
	}
	
	public TreeNode createTree(){
		if(VerificadorUtil.naoEstaNulo(getEntidade().getId())){
			List<DataFalta> listDataFalta = consultarDataFataPorConfiguracao(getEntidade());
			TreeNode root = criarArvoreComOsItens(listDataFalta);
			return this.listaDataFaltaConfiguracao = root;
		}
		return listaDataFaltaConfiguracao = new DefaultTreeNode();
	}

	private TreeNode criarArvoreComOsItens(List<DataFalta> listDataFalta) {
		TreeNode root = new CheckboxTreeNode(new Date(), null);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		for (DataFalta data : listDataFalta) {
			String dataFalta = format.format(data.getDataFalta());
			TreeNode noPai =  new CheckboxTreeNode(dataFalta, root);
			for (HorarioDisponivel horarioDisponivel : consultarHorarioPorConfiguracao(data)) {
				HorarioDisponivel horario = new HorarioDisponivel(horarioDisponivel.getHorario(), horarioDisponivel.getDataFalta());
				new CheckboxTreeNode("Indentificador: " + horario.getHorario().getId() + " / " + " Horário: " + horario.getHorario().getHorarioInicio() + " - " + horario.getHorario().getHorarioFim(), noPai);
			}
		}
		return root;
	}
	
	public void excluirTreeNode(TreeNode[] nodes) {
		boolean isProcesso = true;
		
		if (nodes != null && nodes.length > 0) {

			for (TreeNode nodeDate : nodes) {
				Date dataFalta = null;
				try {
					dataFalta = DateFormatterUtil.formatarStringParaDataConfiguracao(nodeDate.getData().toString(),
							"dd/MM/yyyy");
				} catch (ParseException e) {
				} finally {
					if (dataFalta instanceof Date) {
						List<Restricoes> listaRestricoesData = new ArrayList<Restricoes>();
						listaRestricoesData.add(Restricoes.igual("dataFalta",
								DateFormatterUtil.formatarStringParaData(nodeDate.getData().toString(), "dd/MM/yyyy")));
						listaRestricoesData.add(Restricoes.igual("configuracao.id", getEntidade().getId()));
						DataFalta dataFaltaEntidade = this.dataFaltaFacade.consultarEntidade(new DataFalta(),
								listaRestricoesData);

						if (VerificadorUtil.naoEstaNulo(dataFaltaEntidade)) {
							List<Restricoes> listaRestricoesHorario = new ArrayList<Restricoes>();
							listaRestricoesHorario.add(Restricoes.igual("dataFalta.id", dataFaltaEntidade.getId()));
							List<HorarioDisponivel> listHorarioDisponivel = this.horarioDisponivelFacade
									.consultarTodos(new HorarioDisponivel(), listaRestricoesHorario, null);
							for (HorarioDisponivel horarioDisponivel : listHorarioDisponivel) {
								this.horarioDisponivelFacade.excluir(horarioDisponivel);
							}
							this.dataFaltaFacade.excluir(dataFaltaEntidade);
							isProcesso = false;
						}
					}
				}
			}

			if(isProcesso){
				for (TreeNode node : nodes) {
					List<Restricoes> listaRestricoesData = new ArrayList<Restricoes>();
					listaRestricoesData.add(Restricoes.igual("dataFalta",
							DateFormatterUtil.formatarStringParaData(node.getParent().toString(), "dd/MM/yyyy")));
					listaRestricoesData.add(Restricoes.igual("configuracao.id", getEntidade().getId()));
					DataFalta dataFaltaEntidade = this.dataFaltaFacade.consultarEntidade(new DataFalta(),
							listaRestricoesData);
					
					List<Restricoes> listaRestricoesHorario = new ArrayList<Restricoes>();
					listaRestricoesHorario.add(
							Restricoes.igual("horario.id", Long.parseLong(node.getData().toString().substring(16,17))));
					listaRestricoesHorario.add(Restricoes.igual("dataFalta.id", dataFaltaEntidade.getId()));
					HorarioDisponivel entidade = this.horarioDisponivelFacade.consultarEntidade(new HorarioDisponivel(),
							listaRestricoesHorario);
					this.horarioDisponivelFacade.excluir(entidade);
				}
			}
			createTree();
		}else{
			lancarErro("erro_favor_selecionar_uma_data_ou_um_horaraio_excluir");
			return;
		}
		
		lancarSucesso("registro_excluido_com_sucesso");
	}

	private List<DataFalta> consultarDataFataPorConfiguracao(Configuracao entidade) {
		return this.dataFaltaRepository.consultarDatasPorConfiguracaoComHorario(entidade);
	}

	private List<HorarioDisponivel> consultarHorarioPorConfiguracao(DataFalta entidade) {
		HorarioDisponivel horario =  new HorarioDisponivel();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("dataFalta.dataFalta", entidade.getDataFalta()));
		List<HorarioDisponivel> listHorario = this.horarioDisponivelFacade.consultarTodos(horario, listaRestricoes, horario.getCampoOrderBy());
		return listHorario;
	}

	private void alteraConfiguracaoSeExistir(Configuracao entidade) {
		Configuracao configuracaoConsulta = new Configuracao();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("pessoa.id", entidade.getPessoa().getId()));
		Configuracao configuracao = this.configuracaoFacade.consultarEntidade(configuracaoConsulta, listaRestricoes);
		if(VerificadorUtil.naoEstaNulo(configuracao)){
			this.configuracaoFacade.alterar(configuracao);
		}
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Configuracao());
	}
	
	public void consultarUsuario(){
		Configuracao configuracaoConsulta = new Configuracao();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())
				&& VerificadorUtil.naoEstaNulo(getEntidade().getPessoa().getId())){
			listaRestricoes.add(Restricoes.igual("pessoa.id", getEntidade().getPessoa().getId()));
			Configuracao entidade = this.configuracaoFacade.consultarEntidade(configuracaoConsulta, listaRestricoes);
			if(VerificadorUtil.estaNulo(entidade)){
				setEntidade(getEntidade());
				this.dataAtivo = true;
			}else{
				setEntidade(entidade);
			};
		}else{
			setEntidade(this.configuracaoFacade.consultarEntidade(configuracaoConsulta, null));
			this.dataAtivo = false;
		}
		
		inicializarDualListModel();
		inicializarDualListModelHorario();
		createTree();
	}

	public List<ItemCombo> getListaPessoas() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Pessoa pessoaConsulta = new Pessoa();
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Sim));
		
		if (VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa().getId()));
		}

		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		
		for (Pessoa pessoa : listaPessoas) {
				listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
		}
		return listaItemCombo;
	} 
	
	@Override
	public Configuracao getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Configuracao();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Configuracao> getFacade() {
		return this.configuracaoFacade;
	}
	
	public ConfiguracaoConsulta getConsulta() {
		return consulta;
	}

	public PessoaConversor getPessoaConversor() {
		return pessoaConversor;
	}

	public void setPessoaConversor(PessoaConversor pessoaConversor) {
		this.pessoaConversor = pessoaConversor;
	}

	public Date getDataFalta() {
		return dataFalta;
	}

	public void setDataFalta(Date dataFalta) {
		this.dataFalta = dataFalta;
	}
	
	public DualListModel<DataFalta> getListaDataFaltaSalvas() {
		return listaDataFaltaSalvas;
	}
	
	public void setListaDataFaltaSalvas(DualListModel<DataFalta> listaDataFaltaSalvas) {
		this.listaDataFaltaSalvas = listaDataFaltaSalvas;
	}
	
	public DataFaltaConversor getDataFaltaConversor() {
		return dataFaltaConversor;
	}
	
	public void setDataFaltaConversor(DataFaltaConversor dataFaltaConversor) {
		this.dataFaltaConversor = dataFaltaConversor;
	}
	
	public DualListModel<Horario> getListaHorario() {
		return listaHorario;
	}
	
	public void setListaHorario(DualListModel<Horario> listaHorario) {
		this.listaHorario = listaHorario;
	}
	
	public HorarioConversor getHorarioConversor() {
		return horarioConversor;
	}
	
	public void setHorarioConversor(HorarioConversor horarioConversor) {
		this.horarioConversor = horarioConversor;
	}
	
	public TreeNode getListaDataFaltaConfiguracao() {
		return listaDataFaltaConfiguracao;
	}
	
	@Override
	public void setListFiltrado(List<Configuracao> listFiltrado) {
		super.setListFiltrado(listFiltrado);
	}
	
	public boolean isDataAtivo() {
		return dataAtivo;
	}
	
	public void setDataAtivo(boolean dataAtivo) {
		this.dataAtivo = dataAtivo;
	}
	
	public TreeNode[] getSelectTreeNode() {
		return selectTreeNode;
	}
	
	public void setSelectTreeNode(TreeNode[] selectTreeNode) {
		this.selectTreeNode = selectTreeNode;
	}
	/*@Override
	protected GeradorRelatorioDinamico<Configuracao> getRelatorio() {
		return this.configuracaoRelatorioDinamico;
	}*/ 
}
