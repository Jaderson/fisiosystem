package com.fisio.web.bean.objeto;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.objeto.ObjetoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Objeto;
import com.fisio.web.consulta.objeto.ObjetoConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class ObjetoBean extends AbstractBean<Objeto>{

	private ObjetoFacade objetoFacade;
	private ObjetoConsulta consulta;
	
	@Autowired
	public ObjetoBean(ObjetoFacade objetoFacade, ObjetoConsulta consulta) {
		this.objetoFacade = objetoFacade;
		this.consulta = consulta;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Objeto());
	}
	
	@Override
	public void filtro() {
		Objeto objetoConsulta = new Objeto();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.objetoFacade.filtro(listaRestricoes, objetoConsulta, null));
	}
	
	@Override
	public Objeto getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Objeto();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Objeto> getFacade() {
		return this.objetoFacade;
	}
	
	public ObjetoConsulta getConsulta() {
		return consulta;
	}
}
