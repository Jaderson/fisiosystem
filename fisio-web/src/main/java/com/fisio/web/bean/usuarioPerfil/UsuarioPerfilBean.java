package com.fisio.web.bean.usuarioPerfil;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.usuarioPerfil.UsuarioPerfilFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.web.consulta.usuarioPerfil.UsuarioPerfilConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class UsuarioPerfilBean extends AbstractBean<UsuarioPerfil>{

	private UsuarioPerfilFacade usuarioPerfilFacade;
	private UsuarioPerfilConsulta consulta;
	
	@Autowired
	public UsuarioPerfilBean(UsuarioPerfilFacade usuarioPerfilFacade, UsuarioPerfilConsulta consulta) {
		this.usuarioPerfilFacade = usuarioPerfilFacade;
		this.consulta = consulta;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new UsuarioPerfil());
	}
	
	@Override
	public void filtro() {
		UsuarioPerfil usuarioPerfilConsulta = new UsuarioPerfil();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.usuarioPerfilFacade.filtro(listaRestricoes, usuarioPerfilConsulta, null));
	}
	
	@Override
	public UsuarioPerfil getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new UsuarioPerfil();
		}
		return entidade;
	}
	
	@Override
	protected Facade<UsuarioPerfil> getFacade() {
		return this.usuarioPerfilFacade;
	}
	
	public UsuarioPerfilConsulta getConsulta() {
		return consulta;
	}
}
