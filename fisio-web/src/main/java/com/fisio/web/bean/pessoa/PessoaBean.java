package com.fisio.web.bean.pessoa;

import static com.fisio.web.constantes.ConstantesUtilWeb.REGISTRO_EXCLUIDO_COM_SUCESSO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.medico.MedicoFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.facade.profissao.ProfissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.medico.Medico;
import com.fisio.framework.profissao.Profissao;
import com.fisio.framework.util.ConstantesModelUtil;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.Sexo;
import com.fisio.model.pessoa.SimNao;
import com.fisio.model.pessoa.TipoPessoa;
import com.fisio.relatorio.GeradorRelatorioDinamico;
import com.fisio.relatorio.pessoa.PessoaRelatorioDinamico;
import com.fisio.web.constantes.enumerator.Estado;
import com.fisio.web.consulta.pessoa.PessoaConsulta;
import com.fisio.web.converter.medico.MedicoConverter;
import com.fisio.web.converter.profissao.ProfissaoConversor;
import com.fisio.web.converter.simnaoparaboolean.SimNaoParaBooleanConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
@SessionScoped
public class PessoaBean extends AbstractBean<Pessoa>{

	public static final String DIRETORIO_FOTO_PESSOA = "pessoas/";
	
	private PessoaFacade pessoaFacade;
	private PessoaConsulta consulta;
	private ProfissaoFacade profissaoFacade;
	private ProfissaoConversor profissoesConversor;
	private MedicoFacade medicoFacade;
	private MedicoConverter medicoConverter;
	
	private boolean funcionario;
	private UploadedFile uploadedFile;
	private InputStream inputStream;
	private SimNaoParaBooleanConversor conversorSimNao;
	
	@SuppressWarnings("unused")
	private boolean funcionarioSimNao;
	private boolean crefito;
	
	private PessoaRelatorioDinamico pessoaRelatorioDinamico;
	
	@Autowired
	public PessoaBean(PessoaFacade pessoaFacade, PessoaConsulta consulta, ProfissaoFacade profissoesFacade,
			PessoaRelatorioDinamico pessoaRelatorioDinamico,
			MedicoFacade medicoFacade) {
		this.pessoaFacade = pessoaFacade;
		this.consulta = consulta;
		this.profissaoFacade = profissoesFacade;
		this.pessoaRelatorioDinamico = pessoaRelatorioDinamico;
		
		this.profissoesConversor = new ProfissaoConversor();
		this.conversorSimNao = new SimNaoParaBooleanConversor();
		this.consulta.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
		this.medicoFacade = medicoFacade;
		this.medicoConverter = new MedicoConverter();
		mostrarCampoCrefito();
	}
	
	@Override
	protected void modificarEntidadeAntesDaOperacaoParaCadastrar(Pessoa entidade) {
		getEntidade().setCriadoEm(new Date());
		if(VerificadorUtil.estaNulo(getEntidade().getEmail())){
			getEntidade().setEmail("contato@cinematicafisioesaude.com.br");
		}
		if(VerificadorUtil.estaNulo(getEntidade().getProfissao())){
			getEntidade().setProfissao(getUsuarioLogado().getPessoa().getProfissao());
		}
	}
	
	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(Pessoa entidade) {
		getEntidade().setTipoPessoa(TipoPessoa.F);
		getEntidade().setAtivo(SimNao.Sim);
	}
	
	@Override
	public void excluir(Pessoa entidade) {
		entidade.setAtivo(SimNao.Não);
		getFacade().alterar(entidade);
		filtro();
		limparEntidade();
		lancarSucesso(REGISTRO_EXCLUIDO_COM_SUCESSO);
	}
	
	@Override
	public void setSelecionar(Pessoa entidade) {
		super.setSelecionar(entidade);
		if(VerificadorUtil.naoEstaNulo(getEntidade())){
			getEntidade().setFotoUrlEndereco("/imagens/" + getEntidade().getFotoUrl());
		}
	}
	
	public void buscarEnderecoPorCEP() {
	}
	
	@Override
	public void novo() {
		getEntidade().setTipoPessoa(TipoPessoa.F);
		super.novo();
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
 		
	@Override
	protected void limparEntidade() {
		setEntidade(new Pessoa());
	}
	
	@Override
	public void filtro() {
		Pessoa pessoaConsulta = new Pessoa();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.pessoaFacade.filtro(listaRestricoes, pessoaConsulta, pessoaConsulta.getCampoOrderBy()));
	}
	
	@Override
	public Pessoa getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Pessoa();
		}
		return entidade;
	}
	
	public void setFuncionario(boolean funcionario) {
		entidade.setFuncionario(this.funcionario ? SimNao.Sim : SimNao.Não);
		this.funcionario = funcionario;
	}
	
	public boolean isFuncionario() {
		return SimNao.Sim.equals(entidade.getFuncionario()) ? false :true;
	}
	
	public boolean isCrefito() {
		return crefito;
	}
	
	public void setCrefito(boolean crefito) {
		this.crefito = crefito;
	}
	
	@Override
	protected Facade<Pessoa> getFacade() {
		return this.pessoaFacade;
	}
	
	public PessoaConsulta getConsulta() {
		return consulta;
	}
	
	public void setConsulta(PessoaConsulta consulta) {
		this.consulta = consulta;
	}

	public void handleFileUpload(FileUploadEvent event) {		
		try {
			uploadedFile = event.getFile();
			getEntidade().setFotoUrl(DIRETORIO_FOTO_PESSOA + uploadedFile.getFileName());
			
			inputStream = event.getFile().getInputstream();
			File file = new File(ConstantesModelUtil.BASE_PATH_URL + DIRETORIO_FOTO_PESSOA,uploadedFile.getFileName());
			file.createNewFile();
			FileOutputStream fileOutput = new FileOutputStream(file);
			
			byte[] buffer = new byte[1024];
			int length;
			
			while((length = inputStream.read(buffer))>0){
				fileOutput.write(buffer, 0, length);
			}
			
			fileOutput.close();
			inputStream.close();
			
			if(VerificadorUtil.naoEstaNulo(getEntidade())){
				getEntidade().setFotoUrlEndereco("/images" + getEntidade().getFotoUrl());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public StreamedContent getFotoUploaded() {
		try {
			File file = new File(getEntidade().getFotoUrlEndereco());
			System.out.println(file.exists());
			FileInputStream stream = new FileInputStream(file);
			StreamedContent image = new DefaultStreamedContent(stream, "image/jpg");
			return image;
			
		} catch (Exception e) {
			throw new RuntimeException("Falha ao carregar imagem");
		}
	}
	
	public void mostrarCampoCrefito(){
		if(getEntidade().getFuncionario() == SimNao.Sim ? false : true){
			setCrefito(true);
			getEntidade().setCrefito(null);
		}else{
			setCrefito(false);
		}
	}
	
	public SelectItem[] getSexoValues() {
		int i = 0;
		SelectItem[] items = new SelectItem[Sexo.values().length];
		for(Sexo sexo : Sexo.values()) {
			items[i++] = new SelectItem(sexo, sexo.getDescricao());
		}
		
		return items;
	}
		
	public SelectItem[] getTipoPessoaValues() {
		int i = 0;
		SelectItem[] items = new SelectItem[TipoPessoa.values().length];
		for(TipoPessoa tipoPessoa : TipoPessoa.values()) {
			items[i++] = new SelectItem(tipoPessoa, tipoPessoa.getValue());
		}
		
		return items;
	}
	
	public SelectItem[] getListaEstados() {
		int i = 0;
		SelectItem[] items = new SelectItem[Estado.values().length];
		for(Estado estado: Estado.values()) {
			items[i++] = new SelectItem(estado, estado.getEstado());
		}
		
		return items;
	}
	
	public List<ItemCombo> getListaProfissoes() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Profissao profissoesConsulta = new Profissao();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();

		List<Profissao> listaProfissoes = this.profissaoFacade.consultarTodos(profissoesConsulta, listaRestricoes, profissoesConsulta.getCampoOrderBy());
		for (Profissao profissoes : listaProfissoes) {
				listaItemCombo.add(new ItemCombo(profissoes.getDescricao(), profissoes));
		}
		return listaItemCombo;
	}
	
	public List<ItemCombo> getListaMedicos() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Medico medicoConsulta = new Medico();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();

		List<Medico> listaMedicos = this.medicoFacade.consultarTodos(medicoConsulta, listaRestricoes, medicoConsulta.getCampoOrderBy());
		for (Medico medico : listaMedicos) {
				listaItemCombo.add(new ItemCombo(medico.getNome(), medico));
		}
		return listaItemCombo;
	}
	
	public ProfissaoConversor getProfissoesConversor() {
		return profissoesConversor;
	}
	
	public void setProfissoesConversor(ProfissaoConversor profissoesConversor) {
		this.profissoesConversor = profissoesConversor;
	}
	
	public SimNaoParaBooleanConversor getConversorSimNao() {
		return conversorSimNao;
	}
	
	public void setConversorSimNao(SimNaoParaBooleanConversor conversorSimNao) {
		this.conversorSimNao = conversorSimNao;
	}
	
	public boolean isFuncionarioSimNao() {
		return SimNao.Sim.equals(entidade.getFuncionario()) ? true : false;
	}

	public void setFuncionarioSimNao(boolean funcionarioSimNao) {
		entidade.setFuncionario(funcionarioSimNao ? SimNao.Sim : SimNao.Não);
		this.funcionarioSimNao = funcionarioSimNao;
	}
	
	public MedicoConverter getMedicoConverter() {
		return medicoConverter;
	}

	public void setMedicoConverter(MedicoConverter medicoConverter) {
		this.medicoConverter = medicoConverter;
	}

	@Override
	protected GeradorRelatorioDinamico<Pessoa> getRelatorio() {
		return this.pessoaRelatorioDinamico;
	}
}
