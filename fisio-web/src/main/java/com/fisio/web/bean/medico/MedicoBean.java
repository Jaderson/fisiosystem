package com.fisio.web.bean.medico;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.especialidade.EspecialidadeFacade;
import com.fisio.facade.medico.MedicoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.medico.Medico;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.SimNao;
import com.fisio.web.consulta.medico.MedicoConsulta;
import com.fisio.web.converter.especialidade.EspecialidadeConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class MedicoBean extends AbstractBean<Medico> {

	private MedicoFacade medicoFacade;
	private MedicoConsulta consulta;
	private EspecialidadeFacade especialidadeFacade;
	private EspecialidadeConversor especialidadeConversor;

	/* private MedicoRelatorioDinamico MedicoRelatorioDinamico; */

	@Autowired
	public MedicoBean(MedicoFacade medicoFacade, MedicoConsulta consulta, EspecialidadeFacade especialidadeFacade/*
																													 * MedicoRelatorioDinamico
																													 * medicoRelatorioDinamico
																													 */) {
		this.medicoFacade = medicoFacade;
		this.consulta = consulta;
		/* this.medicoRelatorioDinamico = medicoRelatorioDinamico */
		this.especialidadeFacade = especialidadeFacade;
		this.especialidadeConversor = new EspecialidadeConversor();
	}

	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}

	@Override
	protected void limparEntidade() {
		setEntidade(new Medico());
	}

	@Override
	public void filtro() {
		Medico medicoConsulta = new Medico();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if (this.filtro != null) {
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.medicoFacade.filtro(listaRestricoes, medicoConsulta, medicoConsulta.getCampoOrderBy()));
	}

	@Override
	public Medico getEntidade() {
		if (VerificadorUtil.estaNulo(entidade)) {
			entidade = new Medico();
		}
		return entidade;
	}

	public List<ItemCombo> getListaEspecialidade() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Especialidade especialidadeConsulta = new Especialidade();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		List<Especialidade> listaEspecialidade = this.especialidadeFacade.consultarTodos(especialidadeConsulta,
				listaRestricoes, especialidadeConsulta.getCampoOrderBy());
		for (Especialidade especialidade : listaEspecialidade) {
			listaItemCombo.add(new ItemCombo(especialidade.getDescricao(), especialidade));
		}
		return listaItemCombo;
	}

	@Override
	protected Facade<Medico> getFacade() {
		return this.medicoFacade;
	}

	public MedicoConsulta getConsulta() {
		return consulta;
	}

	public EspecialidadeConversor getEspecialidadeConversor() {
		return especialidadeConversor;
	}

	public void setEspecialidadeConversor(EspecialidadeConversor especialidadeConversor) {
		this.especialidadeConversor = especialidadeConversor;
	}

	/*
	 * @Override protected GeradorRelatorioDinamico<Medico> getRelatorio() {
	 * return this.medicoRelatorioDinamico; }
	 */
}
