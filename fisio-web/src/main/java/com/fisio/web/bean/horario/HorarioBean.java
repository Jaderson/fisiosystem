package com.fisio.web.bean.horario;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.horario.HorarioFacade;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.horario.HorarioConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class HorarioBean extends AbstractBean<Horario>{

	private HorarioFacade horarioFacade;
	private HorarioConsulta consulta;
	
	/*private HorarioRelatorioDinamico HorarioRelatorioDinamico;*/
	
	@Autowired
	public HorarioBean(HorarioFacade horarioFacade, HorarioConsulta consulta/*, HorarioRelatorioDinamico horarioRelatorioDinamico*/) {
		this.horarioFacade = horarioFacade;
		this.consulta = consulta;
		/*this.horarioRelatorioDinamico = horarioRelatorioDinamico*/
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Horario());
	}
	
	@Override
	public void filtro() {
		Horario horarioConsulta = new Horario();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("horarioInicio", this.filtro));
		}
		setListFiltrado(this.horarioFacade.filtro(listaRestricoes, horarioConsulta,horarioConsulta.getCampoOrderBy()));
	}
	
	@Override
	public Horario getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Horario();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Horario> getFacade() {
		return this.horarioFacade;
	}
	
	public HorarioConsulta getConsulta() {
		return consulta;
	}
	
	/*@Override
	protected GeradorRelatorioDinamico<Horario> getRelatorio() {
		return this.horarioRelatorioDinamico;
	}*/ 
}
