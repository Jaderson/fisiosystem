package com.fisio.web.bean.dataFalta;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.dataFalta.DataFaltaFacade;
import com.fisio.framework.agenda.DataFalta;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.dataFalta.DataFaltaConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class DataFaltaBean extends AbstractBean<DataFalta>{

	private DataFaltaFacade dataFaltaFacade;
	private DataFaltaConsulta consulta;
	
	/*private DataFaltaRelatorioDinamico DataFaltaRelatorioDinamico;*/
	
	@Autowired
	public DataFaltaBean(DataFaltaFacade dataFaltaFacade, DataFaltaConsulta consulta/*, DataFaltaRelatorioDinamico dataFaltaRelatorioDinamico*/) {
		this.dataFaltaFacade = dataFaltaFacade;
		this.consulta = consulta;
		/*this.dataFaltaRelatorioDinamico = dataFaltaRelatorioDinamico*/
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new DataFalta());
	}
	
	/*@Override
	public void filtro() {
		DataFalta dataFaltaConsulta = new DataFalta();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.dataFaltaFacade.filtro(listaRestricoes, dataFaltaConsulta));
	} */
	
	@Override
	public DataFalta getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new DataFalta();
		}
		return entidade;
	}
	
	@Override
	protected Facade<DataFalta> getFacade() {
		return this.dataFaltaFacade;
	}
	
	public DataFaltaConsulta getConsulta() {
		return consulta;
	}
	
	/*@Override
	protected GeradorRelatorioDinamico<DataFalta> getRelatorio() {
		return this.dataFaltaRelatorioDinamico;
	}*/ 
}
