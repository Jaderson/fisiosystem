package com.fisio.web.bean.acompanhamentoFilho;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.acompanhamentoFilho.AcompanhamentoFilhoFacade;
import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.acompanhamentoFilho.AcompanhamentoFilhoConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class AcompanhamentoFilhoBean extends AbstractBean<AcompanhamentoFilho>{

	private AcompanhamentoFilhoFacade acompanhamentoFilhoFacade;
	private AcompanhamentoFilhoConsulta consulta;
	
	@Autowired
	public AcompanhamentoFilhoBean(AcompanhamentoFilhoFacade acompanhamentoFilhoFacade, AcompanhamentoFilhoConsulta consulta) {
		this.acompanhamentoFilhoFacade = acompanhamentoFilhoFacade;
		this.consulta = consulta;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new AcompanhamentoFilho());
	}
	
	@Override
	public void filtro() {
		AcompanhamentoFilho acompanhamentoFilhoConsulta = new AcompanhamentoFilho();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.acompanhamentoFilhoFacade.filtro(listaRestricoes, acompanhamentoFilhoConsulta,null));
	}
	
	@Override
	public AcompanhamentoFilho getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new AcompanhamentoFilho();
		}
		return entidade;
	}
	
	@Override
	protected Facade<AcompanhamentoFilho> getFacade() {
		return this.acompanhamentoFilhoFacade;
	}
	
	public AcompanhamentoFilhoConsulta getConsulta() {
		return consulta;
	}
}
