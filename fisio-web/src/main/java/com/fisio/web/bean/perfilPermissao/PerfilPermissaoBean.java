package com.fisio.web.bean.perfilPermissao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.perfilPermissao.PerfilPermissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.web.consulta.perfilPermissao.PerfilPermissaoConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class PerfilPermissaoBean extends AbstractBean<PerfilPermissao>{

	private PerfilPermissaoFacade perfilPermissaoFacade;
	private PerfilPermissaoConsulta consulta;
	
	@Autowired
	public PerfilPermissaoBean(PerfilPermissaoFacade perfilPermissaoFacade, PerfilPermissaoConsulta consulta) {
		this.perfilPermissaoFacade = perfilPermissaoFacade;
		this.consulta = consulta;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new PerfilPermissao());
	}
	
	@Override
	public void filtro() {
		PerfilPermissao perfilPermissaoConsulta = new PerfilPermissao();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.perfilPermissaoFacade.filtro(listaRestricoes, perfilPermissaoConsulta, null));
	}
	
	@Override
	public PerfilPermissao getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new PerfilPermissao();
		}
		return entidade;
	}
	
	@Override
	protected Facade<PerfilPermissao> getFacade() {
		return this.perfilPermissaoFacade;
	}
	
	public PerfilPermissaoConsulta getConsulta() {
		return consulta;
	}
}
