package com.fisio.web.bean.agenda;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.agenda.AgendaFacade;
import com.fisio.facade.configuracao.ConfiguracaoFacade;
import com.fisio.facade.especialidade.EspecialidadeFacade;
import com.fisio.facade.horario.HorarioFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.facade.usuarioPerfil.UsuarioPerfilFacade;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.configuracao.Configuracao;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.DateFormatterUtil;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.repository.horario.HorarioRepository;
import com.fisio.service.agenda.AgendaService;
import com.fisio.web.consulta.agenda.AgendaConsulta;
import com.fisio.web.converter.especialidade.EspecialidadeConversor;
import com.fisio.web.converter.horario.HorarioConversor;
import com.fisio.web.converter.pessoa.PessoaConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;
import com.ibm.icu.util.GregorianCalendar;

@Component
@ManagedBean
@Scope("view")
public class AgendaBean extends AbstractBean<Agenda> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AgendaFacade agendaFacade;
	private AgendaService agendaService;
	private AgendaConsulta consulta;
	private ScheduleModel eventModel;
	private PessoaFacade pessoaFacade;
	private PessoaConversor pessoaConversor;
	private EspecialidadeConversor especialidadeConversor;
	private EspecialidadeFacade especialidadeFacade;
	private ConfiguracaoFacade configuracaoFacade;
	private UsuarioPerfilFacade usuarioPerfilFacade;
	public boolean btnRedirect = false;
	private ScheduleEvent event = new DefaultScheduleEvent();
	private Date dataInicioCopia;
	private Date dataFimCopia;
	private HorarioConversor horarioConversor;
	private HorarioFacade horarioFacade;
	private Integer idHorario;
	private List<Horario> listHorarioDisponivel = new ArrayList<Horario>();
	private HorarioRepository horarioRepository;
	private boolean checkAnoCorrente;
	private Integer idEmpresa;
	
	
	@Autowired
	public AgendaBean(AgendaFacade agendaFacade, AgendaConsulta consulta, PessoaFacade pessoaFacade,
			EspecialidadeFacade especialidadeFacade, AgendaService agendaService, ConfiguracaoFacade configuracaoFacade,
			HorarioFacade horarioFacade, HorarioRepository horarioRepository, UsuarioPerfilFacade usuarioPerfilFacade) {
		this.agendaFacade = agendaFacade;
		this.consulta = consulta;
		this.pessoaFacade = pessoaFacade;
		this.especialidadeFacade = especialidadeFacade;
		this.agendaService = agendaService;
		this.configuracaoFacade = configuracaoFacade;
		this.usuarioPerfilFacade = usuarioPerfilFacade;
		this.pessoaConversor = new PessoaConversor();
		this.especialidadeConversor = new EspecialidadeConversor();
		this.horarioConversor = new HorarioConversor();
		this.horarioRepository = horarioRepository;
		this.horarioFacade = horarioFacade;
		this.consulta.setEmpresaUsuarioLogado(getUsuarioLogado().getPessoa().getEmpresa());
		this.idEmpresa = getUsuarioLogado().getEmpresa().getId();
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
	
	@Override
	public void setSelecionarAlterar(Agenda entidade) {
		getEntidade().setPessoa(entidade.getPessoa());
		super.setSelecionarAlterar(entidade);
	}

	@Override
	protected void init() {
		eventModel = new DefaultScheduleModel();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		/*listaRestricoes.add(Restricoes.igual("falta", false));*/
		listaRestricoes.add(Restricoes.igual("cancelou", false));
		listaRestricoes.add(Restricoes.between("dataInicio", getAnoCorrente(1,1, "00:00:00"), getAnoCorrente(31,12, "23:59:59")));
		if(possuiPermissao("ROLE_AGENDA_TODOS")) {
			listaRestricoes.add(Restricoes.igual("funcionario.id", getUsuarioLogado().getPessoa().getId()));
		}
		List<Agenda> listaAgendaFiltrada = this.agendaFacade.consultarTodos(new Agenda(), listaRestricoes, null);
		setListFiltrado(listaAgendaFiltrada);
		for (Agenda agenda : listFiltrado) {
			DefaultScheduleEvent event = new DefaultScheduleEvent();
			event.setTitle(agenda.getPessoa().getNome() + " / " + agenda.getEspecialidade().getDescricao() + " / " + agenda.getFuncionario().getNome());
			event.setStartDate(agenda.getDataInicio());
			event.setEndDate(agenda.getDataFim());
			event.setAllDay(agenda.isDiaTodo());
			event.setData(agenda.getId());
			event.setEditable(true);
			
			printCoresAgenda(agenda, event);
			
			eventModel.addEvent(event);
		}
	}
	
	public Date getAnoCorrente(int dia, int mes, String horario){
		com.ibm.icu.util.Calendar cal = GregorianCalendar.getInstance();
		cal.setTime( new Date() );
		         
		int ano = cal.get(Calendar.YEAR);
		         
		try {
		    Date data = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).parse( dia+"/"+mes+"/"+ano + " " + horario);
		    return data;
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		return null;
	}

	private void printCoresAgenda(Agenda agenda, DefaultScheduleEvent event) {
		if(agenda.getFuncionario().getNome().equals("Guilherme Lyra") &&  !agenda.isFalta()) {
			event.setStyleClass("emp1");
		} else if (agenda.getFuncionario().getNome().equals("Jeymison Morais") && !agenda.isFalta()) {
			event.setStyleClass("emp2");
		} else if (agenda.getFuncionario().getNome().equals("Sarah Luna") && !agenda.isFalta()) {
			event.setStyleClass("emp3");
		} else if (agenda.getFuncionario().getNome().equals("Bergson") && !agenda.isFalta()) {
			event.setStyleClass("emp4");
		} else if (agenda.getFuncionario().getNome().equals("Estagiário") && !agenda.isFalta()) {
			event.setStyleClass("emp5");
		} else if (agenda.getFuncionario().getNome().equals("Bruno Lucena") && !agenda.isFalta()) {
			event.setStyleClass("emp6");
		} else if (agenda.getFuncionario().getNome().equals("Jackeline Barros") && !agenda.isFalta()) {
			event.setStyleClass("emp7");
		} else if (agenda.getFuncionario().getNome().equals("Fellipe Albuquerque") && !agenda.isFalta()) {
			event.setStyleClass("emp8");
		} else {
			event.setStyleClass("defautl");
		}
		
		if(agenda.isFalta()) {
			event.setStyleClass("falta");
		}
	}
	
	public void evolucao(Agenda entidade){
		if(VerificadorUtil.naoEstaNulo(entidade.getPessoa())){
			FacesContext.getCurrentInstance().getExternalContext().getFlash().clear();
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("AgendaParam", entidade);
			redirecionarParaTela("/paginas/acompanhamento.xhtml");
		}
	}
	
	public void fluxoCaixa(Agenda entidade){
		if(VerificadorUtil.naoEstaNulo(entidade.getPessoa())){
			FacesContext.getCurrentInstance().getExternalContext().getFlash().clear();
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("AgendaParam", entidade);
			redirecionarParaTela("/paginas/fluxoCaixa.xhtml");
		}
	}
	
	public void pesquisarPorFuncionario(){
		eventModel = new DefaultScheduleModel();
		if(VerificadorUtil.naoEstaNulo(getEntidade().getFuncionario())){
			List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
			listaRestricoes.add(Restricoes.igual("funcionario.id", getEntidade().getFuncionario().getId()));
			listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
			List<Agenda> listaAgendaFiltrada = this.agendaFacade.consultarTodos(new Agenda(), listaRestricoes, null);
			getEntidade().setFuncionario(null);
			for (Agenda agenda : listaAgendaFiltrada) {
				DefaultScheduleEvent event = new DefaultScheduleEvent();
				event.setTitle(agenda.getPessoa().getNome() + "-" + agenda.getEspecialidade().getDescricao());
				event.setStartDate(agenda.getDataInicio());
				event.setEndDate(agenda.getDataFim());
				event.setAllDay(agenda.isDiaTodo());
				event.setData(agenda.getId());
				event.setEditable(true);
				
				printCoresAgenda(agenda, event);
				
				eventModel.addEvent(event);
			}
		}else{
			init();
		}
	}
	
	public int verificarDiaSemanaPorData(Date data){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(data);			
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	public void addEvent(ActionEvent actionEvent) {
		try {
			this.agendaService.verificarCamposObrigatorios(getEntidade());
			if (event.getId() == null) {
				converterHorarioToDate(false);
				if (getEntidade().getDataInicio().getTime() <= getEntidade()
						.getDataFim().getTime()) {
					getEntidade().setEmpresa(getUsuarioLogado().getEmpresa());
					this.agendaFacade.cadastrar(getEntidade());
					init();
				} else {
					lancarErro("erro_data_fim_menor_que_data_inicio");
					return;
				}
			} else {
				converterHorarioToDate(false);
				this.agendaFacade.alterar(getEntidade());
				init();
			}
			event = new DefaultScheduleEvent();
			limparEntidade();

		} catch (Exception e) {
			lancarErro(e.getMessage());
		}
	}

	public void copiar(Agenda entidade) throws ParseException{
		getEntidade().setId(null);
		converterHorarioToDate(true);
		
		if (getEntidade().getDataInicio().getTime() <= getEntidade()
				.getDataFim().getTime()) {
			getEntidade().setEmpresa(getUsuarioLogado().getEmpresa());
			getEntidade().setDataInicio(getEntidade().getDataInicio());
			getEntidade().setDataFim(getEntidade().getDataFim());
			getEntidade().setHorario(consultarHorario(getIdHorario()));
			this.agendaFacade.cadastrar(getEntidade());
			setDataInicioCopia(null);
			setDataFimCopia(null);
			getListHorarioDisponivel().clear();
			init();
		} else {
			lancarErro("erro_data_fim_menor_que_data_inicio");
			return;
		}
	}
	
	private void converterHorarioToDate(boolean isCopy) throws ParseException {
		String dataInicio = DateFormatterUtil.formatarDataParaString(getDataInicioCopia() == null ? getEntidade().getDataInicio() : getDataInicioCopia(), "yyyy/MM/dd");
		String dataFim = DateFormatterUtil.formatarDataParaString(getDataInicioCopia() == null ? getEntidade().getDataInicio() : getDataInicioCopia(), "yyyy/MM/dd");
		Horario horario = consultarHorario(isCopy && getIdHorario() != 0 ? getIdHorario() : getEntidade().getHorario().getId());
		
		dataInicio = dataInicio + " " + horario.getHorarioInicio();
		dataFim = dataFim + " " + horario.getHorarioFim();

		getEntidade().setDataInicio(DateFormatterUtil.formatarStringParaData(dataInicio,"yyyy/MM/dd HH:mm"));
		getEntidade().setDataFim(DateFormatterUtil.formatarStringParaData(dataFim,"yyyy/MM/dd HH:mm"));
	}
	
	public Horario consultarHorario(Integer idHorario){
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", idHorario));
		return this.horarioFacade.consultarEntidade(new Horario(), listaRestricoes);
	}
	@Override
	public void excluir(Agenda entidade) {
		super.excluir(entidade);
		init();
	}
	
	public void onEventSelect(SelectEvent selectEvent) {
		event = (ScheduleEvent) selectEvent.getObject();
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", event.getData()));
		setEntidade(this.agendaFacade.consultarEntidade(new Agenda(),
				listaRestricoes));
	}
     
	public void onDateSelect(SelectEvent selectEvent) {
		limparEntidade();
		event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(),
				(Date) selectEvent.getObject());
		getEntidade().setDataInicio(event.getStartDate());
		getEntidade().setDataFim(event.getEndDate());
	}
     
    public void onEventMove(ScheduleEntryMoveEvent event) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", event.getScheduleEvent().getData()));
		Agenda entidade = this.agendaFacade.consultarEntidade(new Agenda(),
				listaRestricoes);
		entidade.setDataInicio(event.getScheduleEvent().getStartDate());
		entidade.setDataFim(event.getScheduleEvent().getEndDate());
		this.agendaFacade.alterar(entidade);
		limparEntidade();
		init();
    }
     
    public void onEventResize(ScheduleEntryResizeEvent event) {
    	List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", event.getScheduleEvent().getData()));
		Agenda entidade = this.agendaFacade.consultarEntidade(new Agenda(),
				listaRestricoes);
		entidade.setDataInicio(event.getScheduleEvent().getStartDate());
		entidade.setDataFim(event.getScheduleEvent().getEndDate());
		this.agendaFacade.alterar(entidade);
		limparEntidade();
		init();
    }
    

    public List<ItemCombo> getListaPessoas() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Pessoa pessoaConsulta = new Pessoa();
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Não));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		if (VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa().getId()));
		}
		
		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		
		for (Pessoa pessoa : listaPessoas) {
				listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
		}
		return listaItemCombo;
	} 
    
    public void onHorarioChange(){
    	
    	List<Horario> listHorario = horarioRepository.consultarHorarioPorData(getEntidade().getFuncionario().getId(), getEntidade().getDataInicio());
    	getListHorarioDisponivel().clear();
    	for (Horario horario : listHorario) {
			getListHorarioDisponivel().add(horario);
		}
    }
    
    public List<ItemCombo> getListaHorario() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Horario horarioConsulta = new Horario();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		List<Horario> listaHorario = this.horarioFacade.consultarTodos(horarioConsulta, listaRestricoes, horarioConsulta.getCampoOrderBy());
		
		for (Horario horario : getListHorarioDisponivel().isEmpty() ? listaHorario : getListHorarioDisponivel()) {
				listaItemCombo.add(new ItemCombo(horario.getHorarioInicio() + " - " + horario.getHorarioFim(), horario));
		}
		return listaItemCombo;
    }
	
    
    public List<ItemCombo> getListaFuncionario() {
    	Pessoa pessoaConsulta = new Pessoa();
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Sim));
		
		
		if (VerificadorUtil.naoEstaNulo(getEntidade()) && VerificadorUtil.naoEstaNulo(getEntidade().getFuncionario())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getFuncionario().getId()));
			List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
			for (Pessoa pessoa : listaPessoas) {
				for (UsuarioPerfil perfil : usuarioPerfilFacade.consultarPerfilPorUsuario(pessoa)) {
					if(perfil.getPerfil().getDescricao().equals("Fisioterapeuta")) {
						listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
					}
				}
			}
		}
		
		if(VerificadorUtil.naoEstaNulo(getEntidade().getDataInicio())){
			List<Configuracao> listPessoaPorConfiguracao = this.configuracaoFacade.consultarPessoaPorConfiguracao(true,verificarDiaSemanaPorData(getEntidade().getDataInicio()),
					getUsuarioLogado().getPessoa().getEmpresa().getId());
			for (Configuracao pessoa : listPessoaPorConfiguracao) {
				Pessoa entidade = new Pessoa();
				entidade.setNome(pessoa.getPessoa().getNome());
				entidade.setId(pessoa.getPessoa().getId());
				for (UsuarioPerfil perfil : usuarioPerfilFacade.consultarPerfilPorUsuario(entidade)) {
					if(perfil.getPerfil().getDescricao().equals("Fisioterapeuta")) {
						listaItemCombo.add(new ItemCombo(pessoa.getPessoa().getNome(), entidade));
					}
				}
			}
		}
		return listaItemCombo;
	} 
    
    public List<ItemCombo> getListaFuncionarioBuscar(){
    	Pessoa pessoaConsulta = new Pessoa();
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Sim));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		if (VerificadorUtil.naoEstaNulo(getEntidade().getFuncionario())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getFuncionario().getId()));
		}
		
		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		for (Pessoa pessoa : listaPessoas) {
			for (UsuarioPerfil perfil : usuarioPerfilFacade.consultarPerfilPorUsuario(pessoa)) {
				if(perfil.getPerfil().getDescricao().equals("Fisioterapeuta")) {
					listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
				}
			}
		}
		return listaItemCombo;
    }
    
    public List<ItemCombo> getListaEspecialidade() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Especialidade especialidadeConsulta = new Especialidade();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		List<Especialidade> listaEspecialidade = this.especialidadeFacade.consultarTodos(especialidadeConsulta, listaRestricoes, especialidadeConsulta.getCampoOrderBy());
		for (Especialidade especialidade : listaEspecialidade) {
				listaItemCombo.add(new ItemCombo(especialidade.getDescricao(), especialidade));
		}
		return listaItemCombo;
	}
    
	@Override
	protected void limparEntidade() {
		setEntidade(new Agenda());
	}
	
	@Override
	public Agenda getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Agenda();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Agenda> getFacade() {
		return this.agendaFacade;
	}
	
	public AgendaConsulta getConsulta() {
		return consulta;
	}

	public PessoaConversor getPessoaConversor() {
		return pessoaConversor;
	}

	public void setPessoaConversor(PessoaConversor pessoaConversor) {
		this.pessoaConversor = pessoaConversor;
	}

	public EspecialidadeConversor getEspecialidadeConversor() {
		return especialidadeConversor;
	}

	public void setEspecialidadeConversor(
			EspecialidadeConversor especialidadeConversor) {
		this.especialidadeConversor = especialidadeConversor;
	}
	
	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}
	
	public boolean isBtnRedirect() {
		return btnRedirect;
	}
	
	public void setBtnRedirect(boolean btnRedirect) {
		this.btnRedirect = btnRedirect;
	}
	
	public Date getDataInicioCopia() {
		return dataInicioCopia;
	}
	
	public void setDataInicioCopia(Date dataInicioCopia) {
		this.dataInicioCopia = dataInicioCopia;
	}
	
	public Date getDataFimCopia() {
		return dataFimCopia;
	}
	
	public void setDataFimCopia(Date dataFimCopia) {
		this.dataFimCopia = dataFimCopia;
	}
	
	public HorarioConversor getHorarioConversor() {
		return horarioConversor;
	}
	
	public void setHorarioConversor(HorarioConversor horarioConversor) {
		this.horarioConversor = horarioConversor;
	}
	
	public Integer getIdHorario() {
		return idHorario;
	}
	
	public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}
	
	public List<Horario> getListHorarioDisponivel() {
		return listHorarioDisponivel;
	}
	
	public void setListHorarioDisponivel(List<Horario> listHorarioDisponivel) {
		this.listHorarioDisponivel = listHorarioDisponivel;
	}
	
	public boolean isCheckAnoCorrente() {
		return checkAnoCorrente;
	}
	
	public void setCheckAnoCorrente(boolean checkAnoCorrente) {
		this.checkAnoCorrente = checkAnoCorrente;
	}
	
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}


