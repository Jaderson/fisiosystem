package com.fisio.web.bean.banco;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.banco.BancoFacade;
import com.fisio.framework.banco.Banco;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.relatorio.GeradorRelatorioDinamico;
import com.fisio.relatorio.banco.BancoRelatorioDinamico;
import com.fisio.web.consulta.banco.BancoConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class BancoBean extends AbstractBean<Banco>{

	private BancoFacade bancoFacade;
	private BancoConsulta consulta;
	
	private BancoRelatorioDinamico bancoRelatorioDinamico;
	
	@Autowired
	public BancoBean(BancoFacade bancoFacade, BancoConsulta consulta,
			BancoRelatorioDinamico bancoRelatorioDinamico) {
		this.bancoFacade = bancoFacade;
		this.consulta = consulta;
		this.bancoRelatorioDinamico = bancoRelatorioDinamico;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Banco());
	}
	
	@Override
	public void filtro() {
		Banco bancoConsulta = new Banco();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.bancoFacade.filtro(listaRestricoes, bancoConsulta, null));
	}
	
	@Override
	public Banco getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Banco();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Banco> getFacade() {
		return this.bancoFacade;
	}
	
	public BancoConsulta getConsulta() {
		return consulta;
	}
	
	@Override
	protected GeradorRelatorioDinamico<Banco> getRelatorio() {
		return this.bancoRelatorioDinamico;
	}
}
