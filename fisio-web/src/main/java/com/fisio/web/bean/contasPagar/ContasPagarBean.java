package com.fisio.web.bean.contasPagar;

import static com.fisio.web.constantes.ConstantesUtilWeb.REGISTRO_CADASTRADO_COM_SUCESSO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.categoria.CategoriaFacade;
import com.fisio.facade.contasPagar.ContasPagarFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.contasPagar.ContasPagar;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.service.contasPagar.ContasPagarService;
import com.fisio.web.consulta.contasPagar.ContasPagarConsulta;
import com.fisio.web.converter.categoria.CategoriaConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class ContasPagarBean extends AbstractBean<ContasPagar>{

	private ContasPagarFacade contasPagarFacade;
	private ContasPagarConsulta consulta;
	private ContasPagarService contasPagarService;
	
	private CategoriaFacade cantegoriaFacade;
	private CategoriaConversor categoriaConversor;
	
	private Date primeiraData;
	private Date segundaData;
	private List<ContasPagar> listFiltradoPago = new ArrayList<ContasPagar>();
	private boolean bloquear;
	
	@Autowired
	public ContasPagarBean(ContasPagarFacade contasPagarFacade, ContasPagarConsulta consulta, 
			ContasPagarService contasPagarService, CategoriaFacade categoriaFacade) {
		this.contasPagarFacade = contasPagarFacade;
		this.consulta = consulta;
		this.cantegoriaFacade = categoriaFacade;
		this.contasPagarService = contasPagarService;
		this.categoriaConversor = new CategoriaConversor();
		this.consulta.setEmpresa(getUsuarioLogado().getEmpresa());
		filtroContasPaga();
	}
	
	public void onCellEdit(CellEditEvent event) {
		ContasPagar entidade =(ContasPagar)((DataTable)event.getComponent()).getRowData();
		contasPagarFacade.alterar(entidade);
	}
	
	@Override
	public void cadastrar(ContasPagar entidade) {
		try {
			this.contasPagarService.verificarCamposObrigatorios(entidade);
			Integer numeroParcelas = Integer.valueOf(entidade.getParcelas() == null ? "1" : entidade.getParcelas());
			
			if (numeroParcelas != null && numeroParcelas > 1) {
				
				GregorianCalendar dataModificada = new GregorianCalendar();
				dataModificada.setTime(entidade.getVencimento());
				
				for (int i = 1; i <= numeroParcelas; i++) {
					ContasPagar contasPagar = new ContasPagar();
					dataModificada.set(Calendar.MONTH, i);
					contasPagar.setVencimento(dataModificada.getTime());
					contasPagar.setEmpresa(getUsuarioLogado().getEmpresa());
					contasPagar.setParcelas(String.valueOf(i) + "/"
							+ numeroParcelas);
					contasPagar.setContaFixa(false);
					contasPagar.setDescricao(entidade.getDescricao());
					contasPagar.setPago(false);
					contasPagar.setValor(entidade.getValor());
					this.contasPagarFacade.cadastrar(contasPagar);
				}
			} else {
				entidade.setParcelas(getMensagemI18n("Mensalmente"));
				entidade.setEmpresa(getUsuarioLogado().getEmpresa());
				this.contasPagarFacade.cadastrar(entidade);
			}
			atualizarDataTable();
			limparEntidade();
			setCadastrarVisivel(false);
			lancarSucesso(REGISTRO_CADASTRADO_COM_SUCESSO);
			
		} catch (Exception e) {
			lancarErro(e.getMessage());
		}
	}
	
	public void setSelecionarPaga(ContasPagar entidade) {
		if(entidade.isContaFixa()){
			ContasPagar contasPagar = new ContasPagar();
			contasPagar.setPago(true);
			contasPagar.setContaFixa(true);
			contasPagar.setValor(entidade.getValor());
			contasPagar.setVencimento(new Date());
			contasPagar.setParcelas(getMensagemI18n("Mensalmente"));
			contasPagar.setDescricao(entidade.getDescricao());
			contasPagar.setEmpresa(entidade.getEmpresa());
			contasPagar.setUsuario(getUsuarioLogado());
			contasPagar.setCategoria(getEntidade().getCategoria());
			this.contasPagarFacade.cadastrar(contasPagar);
		}else{
			entidade.setPago(true);
			entidade.setUsuario(getUsuarioLogado());
			this.contasPagarFacade.alterar(entidade);
		}
		atualizarDataTable();
	}
	
	public List<ItemCombo> getListaCategoria() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Categoria categoriaConsulta = new Categoria();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();

		List<Categoria> listaCategoria = this.cantegoriaFacade.consultarTodos(categoriaConsulta, listaRestricoes, categoriaConsulta.getCampoOrderBy());
		for (Categoria categoria : listaCategoria) {
				listaItemCombo.add(new ItemCombo(categoria.getDescricao(), categoria));
		}
		return listaItemCombo;
	}

	private void atualizarDataTable() {
		filtroContasPaga();
		filtro();
	}
	
	public void excluirContasPaga(ContasPagar entidade) {
		if (entidade.isContaFixa()) {
			this.contasPagarFacade.excluir(entidade);
		} else {
			entidade.setPago(false);
			this.contasPagarFacade.excluirContasParceladas(entidade);
		}
		atualizarDataTable();
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new ContasPagar());
	}
	
	@Override
	public void novo() {
		desbloquear();
		super.novo();
	}
	
	@Override
	public void filtro() {
		ContasPagar contasPagarConsulta = new ContasPagar();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("pago", false));
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.contasPagarFacade.filtro(listaRestricoes, contasPagarConsulta, null));
		
		/*if(this.filtro != null && !this.filtro.equals("__/__/____")){ */
		/*}*/
	}
	
	public void filtroContasPaga(){
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("pago", true));
		if(this.primeiraData != null && this.segundaData != null){
			listaRestricoes.add(Restricoes.between("vencimento", this.primeiraData, this.segundaData));
		}
		setListFiltradoPago(this.contasPagarFacade.filtro(listaRestricoes, new ContasPagar(), null));
	}
	
	public BigDecimal getValorTotal() {
		BigDecimal total = BigDecimal.ZERO;

		for (ContasPagar valor : getListFiltradoPago()) {
			total = total.add(valor.getValor());
		}
		return total;
	}
	
	public void bloquear() {
		setBloquear(entidade.isContaFixa() ? true : false);
	}
	
	public void desbloquear(){
		setBloquear(false);
	}
	
	@Override
	public ContasPagar getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new ContasPagar();
		}
		return entidade;
	}
	
	@Override
	protected Facade<ContasPagar> getFacade() {
		return this.contasPagarFacade;
	}
	
	public ContasPagarConsulta getConsulta() {
		return consulta;
	}
	
	public Date getPrimeiraData() {
		return primeiraData;
	}

	public void setPrimeiraData(Date primeiraData) {
		this.primeiraData = primeiraData;
	}

	public Date getSegundaData() {
		return segundaData;
	}

	public void setSegundaData(Date segundaData) {
		this.segundaData = segundaData;
	}

	public List<ContasPagar> getListFiltradoPago() {
		return listFiltradoPago;
	}
	
	public void setListFiltradoPago(List<ContasPagar> listFiltradoPago) {
		this.listFiltradoPago = listFiltradoPago;
	}
	
	public boolean isBloquear() {
		return bloquear;
	}
	
	public void setBloquear(boolean bloquearParcelas) {
		this.bloquear = bloquearParcelas;
	}
	
	public CategoriaConversor getCategoriaConversor() {
		return categoriaConversor;
	}
}
