package com.fisio.web.bean.authentication;

import static com.fisio.web.constantes.ConstantesUtilWeb.MENSAGEM_SUCESSO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Component;

import com.fisio.converter.empresa.EmpresaConversor;
import com.fisio.email.EnviadorEmail;
import com.fisio.facade.empresa.EmpresaFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.facade.usuario.UsuarioFacade;
import com.fisio.facade.usuarioPerfil.UsuarioPerfilFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.empresa.Empresa;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.web.bean.locale.UtilLocale;
import com.fisio.web.framework.combomodel.ItemCombo;

@Component
@ManagedBean
@Scope("request")
public class RecuperarSenha {
	
	private UsuarioFacade usuarioFacade;
	private PessoaFacade pessoaFacade;
	private EnviadorEmail enviadorEmail;
	private UsuarioPerfilFacade usuarioPerfilFacade;
	private EmpresaFacade empresaFacade; 
	private EmpresaConversor empresaConversor = new EmpresaConversor();
	private String email;
	private Empresa empresa;
	

	@Autowired
	public RecuperarSenha(UsuarioFacade usuarioFacade,PessoaFacade pessoaFacade,
			EnviadorEmail enviadorEmail, UsuarioPerfilFacade usuarioPerfilFacade,
			EmpresaFacade empresaFacade){
		this.usuarioFacade = usuarioFacade;
		this.pessoaFacade = pessoaFacade;
		this.enviadorEmail = enviadorEmail;
		this.usuarioPerfilFacade = usuarioPerfilFacade;
		this.empresaFacade = empresaFacade;
		this.empresaConversor = new EmpresaConversor();
		
	}
	
	public void esqueceuSenha() throws IOException {
		
		if(VerificadorUtil.estaNulo(this.email) || this.email.equals("")){
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, UtilLocale.getMensagemI18n("Digite_seu_email")));
			return;
		}
		
		List<Restricoes> listaRestricoesPessoa = new ArrayList<Restricoes>();
		listaRestricoesPessoa.add(Restricoes.igual("email", this.email));
		listaRestricoesPessoa.add(Restricoes.igual("empresa.id", getEmpresa().getId()));
		Pessoa pessoaConsultada = this.pessoaFacade.consultarEntidade(new Pessoa(), listaRestricoesPessoa);
		if(VerificadorUtil.estaNulo(pessoaConsultada)){
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, UtilLocale.getMensagemI18n("erro_email_nao_confere")));
			return;
		}
		
		List<Restricoes> listaRestricoesUsuario = new ArrayList<Restricoes>();
		listaRestricoesUsuario.add(Restricoes.igual("pessoa.id", pessoaConsultada.getId()));
		Usuario usuarioConsultado = this.usuarioFacade.consultarEntidade(new Usuario(), listaRestricoesUsuario);
		String senhaGeradaAutomatica = gerarSenhaAutomatica();
		enviadorEmail.enviarNovaSenha(usuarioConsultado, senhaGeradaAutomatica);
		String novaSenha = new Md5PasswordEncoder().encodePassword(senhaGeradaAutomatica, null);
		usuarioConsultado.setSenha(novaSenha);
		usuarioConsultado.setTokenAcesso(novaSenha);
		usuarioConsultado.setPerfils(new HashSet<UsuarioPerfil>());
		
		this.usuarioFacade.alterar(usuarioConsultado);
			
		getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, MENSAGEM_SUCESSO, UtilLocale.getMensagemI18n("email_enviado_com_sucesso")));
		return;
	}
	
	public List<ItemCombo> getListaEmpresas() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		List<Empresa> listaEmpresa = this.empresaFacade.consultarTodos(new Empresa());
		for (Empresa empresa : listaEmpresa) {
				listaItemCombo.add(new ItemCombo(empresa.getNomeFantasia(), empresa));
		}
		return listaItemCombo;
	} 
	
	private String gerarSenhaAutomatica() {
		String[] carct = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
				"m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
				"y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
				"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
				"W", "X", "Y", "Z" };
		String senha = "";

		for (int x = 0; x < 10; x++) {
			int j = (int) (Math.random() * carct.length);
			senha += carct[j];
		}
		return senha;
	}
	
	
	public FacesContext getContext() {
		return FacesContext.getCurrentInstance();
	}
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public EmpresaConversor getEmpresaConversor() {
		return empresaConversor;
	}
	
	public void setEmpresaConversor(EmpresaConversor empresaConversor) {
		this.empresaConversor = empresaConversor;
	}
	
}
