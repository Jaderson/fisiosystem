package com.fisio.web.bean.fluxoCaixa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.especialidade.EspecialidadeFacade;
import com.fisio.facade.fluxoCaixa.FluxoCaixaFacade;
import com.fisio.facade.formaPagamento.FormaPagamentoFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.caixa.FluxoCaixa;
import com.fisio.framework.caixa.TipoLancamento;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.web.consulta.fluxoCaixa.FluxoCaixaConsulta;
import com.fisio.web.converter.especialidade.EspecialidadeConversor;
import com.fisio.web.converter.formaPagamento.FormaPagamentoConversor;
import com.fisio.web.converter.pessoa.PessoaConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class FluxoCaixaBean extends AbstractBean<FluxoCaixa>{

	private FluxoCaixaFacade fluxoCaixaFacade;
	private FluxoCaixaConsulta consulta;
	private EspecialidadeConversor especialidadeConversor;
	private EspecialidadeFacade especialidadeFacade;
	private FormaPagamentoFacade formaPagamentoFacade;
	private FormaPagamentoConversor formaPagamentoConversor;
	private PessoaConversor pessoaConversor;
	private PessoaFacade pessoaFacade;
	private Date primeiraData;
	private Date segundaData;
	private boolean pesquisarVisivel;
	private boolean ocultaPaciente = true;
	private boolean ocultaParcela = true;
	private boolean ocultaNumeroRecibo = true;
	public String  labelPagamento = "Quantidade Parcela";
	
	@Autowired
	public FluxoCaixaBean(FluxoCaixaFacade fluxoCaixaFacade, FluxoCaixaConsulta consulta, EspecialidadeFacade especialidadeFacade,
			PessoaFacade pessoaFacade, FormaPagamentoFacade formaPagamentoFacade) {
		this.fluxoCaixaFacade = fluxoCaixaFacade;
		this.consulta = consulta;
		this.especialidadeFacade = especialidadeFacade;
		this.pessoaFacade = pessoaFacade;
		this.formaPagamentoFacade = formaPagamentoFacade;
		this.formaPagamentoConversor =  new FormaPagamentoConversor();
		this.especialidadeConversor = new EspecialidadeConversor();
		this.pessoaConversor = new PessoaConversor();
		this.consulta.setUsuario(getUsuarioLogado());
		this.consulta.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
	}
	
	@Override
	protected void init() {
		Agenda entidadeParametro = (Agenda) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("AgendaParam");
		if(VerificadorUtil.naoEstaNulo(entidadeParametro) && VerificadorUtil.naoEstaNulo(entidadeParametro.getPessoa())){
				novo();
				getEntidade().setTipoLancamento(TipoLancamento.Receita);
				getEntidade().setPessoa(entidadeParametro.getPessoa());
				getEntidade().setEspecialidade(entidadeParametro.getEspecialidade());
		}
		super.init();
	}
	
	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(FluxoCaixa entidade) {
		entidade.setUsuario(getUsuarioLogado());
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
	
	@Override
	public void setSelecionarAlterar(FluxoCaixa entidade) {
		super.setSelecionarAlterar(entidade);
		ocultarCampos();
	}
	
	public void ocultarCampos(){
		ocultarPaciente();
		ocultarNumeroRecibo();
	}

	@Override
	protected void limparEntidade() {
		setEntidade(new FluxoCaixa());
	}
	
	public void abrirPesquisa(){
		setPesquisarVisivel(true);
	}
	
	public void fecharPesquisa() {
		setPrimeiraData(null);
		setSegundaData(null);
		setPesquisarVisivel(false);
	}
	
	@Override
	public void filtro() {
		FluxoCaixa fluxoCaixaConsulta = new FluxoCaixa();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		if(this.primeiraData != null && this.segundaData != null){
			listaRestricoes.add(Restricoes.between("dataLancamento", this.primeiraData, this.segundaData));
		}
		
		fecharPesquisa();
		setListFiltrado(this.fluxoCaixaFacade.filtro(listaRestricoes, fluxoCaixaConsulta, fluxoCaixaConsulta.getCampoOrderBy()));
	}	

	public List<ItemCombo> getListaEspecialidade() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Especialidade especialidadeConsulta = new Especialidade();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		List<Especialidade> listaEspecialidade = this.especialidadeFacade.consultarTodos(especialidadeConsulta, listaRestricoes, especialidadeConsulta.getCampoOrderBy());
		for (Especialidade especialidade : listaEspecialidade) {
				listaItemCombo.add(new ItemCombo(especialidade.getDescricao(), especialidade));
		}
		return listaItemCombo;
	}
	
	public List<ItemCombo> getListaPessoas() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Pessoa pessoaConsulta = new Pessoa();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		if (VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa().getId()));
		}
		
		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		
		for (Pessoa pessoa : listaPessoas) {
				listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
		}
		return listaItemCombo;
	} 

	private void ocultarPaciente() {
		if (getEntidade().getTipoLancamento().equals(TipoLancamento.Despesa)) {
			setOcultaPaciente(false);
		} else {
			setOcultaPaciente(true);
		}
	}
	
	private void ocultarNumeroRecibo() {
		if (getEntidade().getTipoLancamento().equals(TipoLancamento.Despesa)) {
			setOcultaNumeroRecibo(false);
		} else {
			setOcultaNumeroRecibo(true);
		}
	}
	
	public BigDecimal getValorTotal() {
		BigDecimal totalReceitas = BigDecimal.ZERO;
		BigDecimal totalDespesas = BigDecimal.ZERO;

		for (FluxoCaixa valor : getListFiltrado()) {
			if (TipoLancamento.Receita.equals(valor.getTipoLancamento())) {
				totalReceitas = totalReceitas.add(valor.getValor());
			} else if (TipoLancamento.Despesa.equals(valor.getTipoLancamento())) {
				totalDespesas = totalDespesas.add(valor.getValor());
			}
		}
		return totalReceitas.subtract(totalDespesas);
	}
	
	@Override
	public FluxoCaixa getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new FluxoCaixa();
		}
		return entidade;
	}
	
	public SelectItem[] getTipoLancamentoValues() {
		int i = 0;
		SelectItem[] items = new SelectItem[TipoLancamento.values().length];
		for(TipoLancamento tipoLancamento : TipoLancamento.values()) {
			items[i++] = new SelectItem(tipoLancamento, tipoLancamento.getValue());
		}
		
		return items;
	}
	
	public List<ItemCombo> getListaFormaPagamento() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		FormaPagamento formaPagamentoConsulta = new FormaPagamento();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();

		List<FormaPagamento> listaFormaPagamento = this.formaPagamentoFacade.consultarTodos(formaPagamentoConsulta, listaRestricoes, formaPagamentoConsulta.getCampoOrderBy());
		for (FormaPagamento formaPagamento : listaFormaPagamento) {
				listaItemCombo.add(new ItemCombo(formaPagamento.getDescricao(), formaPagamento));
		}
		return listaItemCombo;
	}
	
	public void habilitarParcela(){
		if(getEntidade().getFormaPagamento().getId() == 1 || getEntidade().getFormaPagamento().getId() == 5){
			this.ocultaParcela = true;
		}else if(getEntidade().getFormaPagamento().getId() == 2 || getEntidade().getFormaPagamento().getId() == 4){
			this.ocultaParcela = false;
		}else if(getEntidade().getFormaPagamento().getId() == 3){
			this.labelPagamento = "Numero do Cheque";
			this.ocultaParcela = false;
		}else{
			this.ocultaParcela = true;
		}
	}
	
	@Override
	protected Facade<FluxoCaixa> getFacade() {
		return this.fluxoCaixaFacade;
	}
	
	public FluxoCaixaConsulta getConsulta() {
		return consulta;
	}

	public EspecialidadeConversor getEspecialidadeConversor() {
		return especialidadeConversor;
	}

	public void setEspecialidadeConversor(
			EspecialidadeConversor especialidadeConversor) {
		this.especialidadeConversor = especialidadeConversor;
	}
	
	public PessoaConversor getPessoaConversor() {
		return pessoaConversor;
	}
	
	public void setPessoaConversor(PessoaConversor pessoaConversor) {
		this.pessoaConversor = pessoaConversor;
	}
	
	public boolean isOcultaPaciente() {
		return ocultaPaciente;
	}
	
	public void setOcultaPaciente(boolean ocultaPaciente) {
		this.ocultaPaciente = ocultaPaciente;
	}
	
	public boolean isOcultaParcela() {
		return ocultaParcela;
	}
	
	public void setOcultaParcela(boolean ocultaParcela) {
		this.ocultaParcela = ocultaParcela;
	}
	
	public Date getPrimeiraData() {
		return primeiraData;
	}
	
	public void setPrimeiraData(Date primeiraData) {
		this.primeiraData = primeiraData;
	}
	
	public Date getSegundaData() {
		return segundaData;
	}
	
	public void setSegundaData(Date segundaData) {
		this.segundaData = segundaData;
	}
	
	public boolean isPesquisarVisivel() {
		return pesquisarVisivel;
	}
	
	public void setPesquisarVisivel(boolean pesquisarVisivel) {
		this.pesquisarVisivel = pesquisarVisivel;
	}
	
	public FormaPagamentoConversor getFormaPagamentoConversor() {
		return formaPagamentoConversor;
	}
	
	public void setFormaPagamentoConversor(
			FormaPagamentoConversor formaPagamentoConversor) {
		this.formaPagamentoConversor = formaPagamentoConversor;
	}

	public boolean isOcultaNumeroRecibo() {
		return ocultaNumeroRecibo;
	}

	public void setOcultaNumeroRecibo(boolean ocultaNumeroRecibo) {
		this.ocultaNumeroRecibo = ocultaNumeroRecibo;
	}
}
