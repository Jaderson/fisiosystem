package com.fisio.web.bean.horarioDisponivel;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.horarioDisponivel.HorarioDisponivelFacade;
import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.horarioDisponivel.HorarioDisponivelConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class HorarioDisponivelBean extends AbstractBean<HorarioDisponivel>{

	private HorarioDisponivelFacade horarioDisponivelFacade;
	private HorarioDisponivelConsulta consulta;
	
	/*private HorarioDisponivelRelatorioDinamico HorarioDisponivelRelatorioDinamico;*/
	
	@Autowired
	public HorarioDisponivelBean(HorarioDisponivelFacade horarioDisponivelFacade, HorarioDisponivelConsulta consulta/*, HorarioDisponivelRelatorioDinamico horarioDisponivelRelatorioDinamico*/) {
		this.horarioDisponivelFacade = horarioDisponivelFacade;
		this.consulta = consulta;
		/*this.horarioDisponivelRelatorioDinamico = horarioDisponivelRelatorioDinamico*/
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new HorarioDisponivel());
	}
	
	@Override
	public void filtro() {
		HorarioDisponivel horarioDisponivelConsulta = new HorarioDisponivel();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.horarioDisponivelFacade.filtro(listaRestricoes, horarioDisponivelConsulta,null));
	}
	
	@Override
	public HorarioDisponivel getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new HorarioDisponivel();
		}
		return entidade;
	}
	
	@Override
	protected Facade<HorarioDisponivel> getFacade() {
		return this.horarioDisponivelFacade;
	}
	
	public HorarioDisponivelConsulta getConsulta() {
		return consulta;
	}
	
	/*@Override
	protected GeradorRelatorioDinamico<HorarioDisponivel> getRelatorio() {
		return this.horarioDisponivelRelatorioDinamico;
	}*/ 
}
