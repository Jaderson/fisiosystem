package com.fisio.web.bean.biomecanicaPalmilha;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.biomecanicaPalmilha.BiomecanicaPalmilhaFacade;
import com.fisio.framework.avaliacoes.BiomecanicaPalmilha;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.biomecanicaPalmilha.BiomecanicaPalmilhaConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class BiomecanicaPalmilhaBean extends AbstractBean<BiomecanicaPalmilha>{

	private BiomecanicaPalmilhaFacade biomecanicaPalmilhaFacade;
	private BiomecanicaPalmilhaConsulta consulta;
	
	@Autowired
	public BiomecanicaPalmilhaBean(BiomecanicaPalmilhaFacade biomecanicaPalmilhaFacade, BiomecanicaPalmilhaConsulta consulta) {
		this.biomecanicaPalmilhaFacade = biomecanicaPalmilhaFacade;
		this.consulta = consulta;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new BiomecanicaPalmilha());
	}
	
	@Override
	public void filtro() {
		BiomecanicaPalmilha biomecanicaPalmilhaConsulta = new BiomecanicaPalmilha();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.biomecanicaPalmilhaFacade.filtro(listaRestricoes, biomecanicaPalmilhaConsulta, null));
	}
	
	@Override
	public BiomecanicaPalmilha getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new BiomecanicaPalmilha();
		}
		return entidade;
	}
	
	@Override
	protected Facade<BiomecanicaPalmilha> getFacade() {
		return this.biomecanicaPalmilhaFacade;
	}
	
	public BiomecanicaPalmilhaConsulta getConsulta() {
		return consulta;
	}
}
