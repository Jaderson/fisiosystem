package com.fisio.web.bean.fluxoBancario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.banco.BancoFacade;
import com.fisio.facade.fluxoBancario.FluxoBancarioFacade;
import com.fisio.framework.banco.Banco;
import com.fisio.framework.caixa.TipoLancamento;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.fluxoBancario.FluxoBancario;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.fluxoBancario.FluxoBancarioConsulta;
import com.fisio.web.converter.bancoConversor.BancoConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class FluxoBancarioBean extends AbstractBean<FluxoBancario>{

	private FluxoBancarioFacade fluxoBancarioFacade;
	private FluxoBancarioConsulta consulta;
	private BancoFacade bancoFacade;
	private BancoConversor bancoConversor;
	private Date primeiraData;
	private Date segundaData;
	private boolean pesquisarVisivel;
	private boolean totalVisivel;
	
	@Autowired
	public FluxoBancarioBean(FluxoBancarioFacade fluxoBancarioFacade, FluxoBancarioConsulta consulta,
			BancoFacade bancoFacade) {
		this.fluxoBancarioFacade = fluxoBancarioFacade;
		this.consulta = consulta;
		this.bancoFacade = bancoFacade;
		this.bancoConversor = new BancoConversor();
		this.consulta.setUsuario(getUsuarioLogado());
		this.consulta.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
	}
	
	public void abrirPesquisa(){
		setPesquisarVisivel(true);
	}
	
	public void fecharPesquisa(){
		setPrimeiraData(null);
		setSegundaData(null);
		setPesquisarVisivel(false);
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new FluxoBancario());
	}
	
	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(FluxoBancario entidade) {
		
		entidade.setUsuario(getUsuarioLogado());
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		if(VerificadorUtil.naoEstaNulo(entidade.getBanco())){
			listaRestricoes.add(Restricoes.igual("id", entidade.getBanco().getId()));
		}
		List<Banco> listaBancos = this.bancoFacade.consultarTodos(new Banco(), listaRestricoes, null);
		if(VerificadorUtil.naoEstaNulo(entidade.getTipoLancamento())){
			if(entidade.getTipoLancamento().equals(TipoLancamento.Receita)){
				for (Banco banco : listaBancos) {
					banco.setSaldo(banco.getSaldo().add(entidade.getValor()));
					this.bancoFacade.alterar(banco);
				}
			}else{
				for (Banco banco : listaBancos) {
					banco.setSaldo(banco.getSaldo().subtract(entidade.getValor()));
					this.bancoFacade.alterar(banco);
				}
			}
		}
	}
	
	@Override
	public void excluir(FluxoBancario entidade) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("id", entidade.getBanco().getId()));
		List<Banco> listaBancos = this.bancoFacade.consultarTodos(new Banco(), listaRestricoes, null);
		
		if(entidade.getTipoLancamento().equals(TipoLancamento.Receita)){
			for (Banco banco : listaBancos) {
				 banco.setSaldo(banco.getSaldo().subtract(entidade.getValor()));
				 this.bancoFacade.alterar(banco);
			}
		}else{
			for (Banco banco : listaBancos) {
				 banco.setSaldo(banco.getSaldo().add(entidade.getValor()));
				 this.bancoFacade.alterar(banco);
			}
		}
		super.excluir(entidade);
	}
	
	@Override
	public void filtro() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		if(this.primeiraData != null && this.segundaData != null){
			listaRestricoes.add(Restricoes.between("dataLancamento", this.primeiraData, this.segundaData));
		}if(VerificadorUtil.naoEstaNulo(getEntidade().getBanco())){
			listaRestricoes.add(Restricoes.igual("banco.id", getEntidade().getBanco().getId()));
			setTotalVisivel(true);
		}
		
		fecharPesquisa();
		setListFiltrado(this.fluxoBancarioFacade.filtro(listaRestricoes, new FluxoBancario(), null));
	}
	
	public BigDecimal getValorTotal() {
		BigDecimal totalReceitas = BigDecimal.ZERO;
		BigDecimal totalDespesas = BigDecimal.ZERO;

		for (FluxoBancario valor : getListFiltrado()) {
			if (TipoLancamento.Receita.equals(valor.getTipoLancamento())) {
				totalReceitas = totalReceitas.add(valor.getValor());
			} else if (TipoLancamento.Despesa.equals(valor.getTipoLancamento())) {
				totalDespesas = totalDespesas.add(valor.getValor());
			}
		}
		
		getEntidade().setBanco(null);
		this.primeiraData = null;
		this.segundaData = null;
		return totalReceitas.subtract(totalDespesas);
	}
	
	public List<ItemCombo> getListaBanco() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Banco bancoConsulta = new Banco();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getEmpresa().getId()));
		if (VerificadorUtil.naoEstaNulo(getEntidade().getBanco())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getBanco().getId()));
		}
		List<Banco> listaBancos = this.bancoFacade.consultarTodos(bancoConsulta, listaRestricoes, bancoConsulta.getCampoOrderBy());
		for (Banco banco : listaBancos) {
				listaItemCombo.add(new ItemCombo(banco.getDescricao(), banco));
		}
		return listaItemCombo;
	}
	
	public SelectItem[] getTipoLancamentoValues() {
		int i = 0;
		SelectItem[] items = new SelectItem[TipoLancamento.values().length];
		for(TipoLancamento tipoLancamento : TipoLancamento.values()) {
			items[i++] = new SelectItem(tipoLancamento, tipoLancamento.getValue());
		}
		
		return items;
	}
	
	@Override
	public FluxoBancario getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new FluxoBancario();
		}
		return entidade;
	}
	
	@Override
	protected Facade<FluxoBancario> getFacade() {
		return this.fluxoBancarioFacade;
	}
	
	public FluxoBancarioConsulta getConsulta() {
		return consulta;
	}
	
	public BancoConversor getBancoConversor() {
		return bancoConversor;
	}
	
	public void setBancoConversor(BancoConversor bancoConversor) {
		this.bancoConversor = bancoConversor;
	}

	public Date getPrimeiraData() {
		return primeiraData;
	}

	public void setPrimeiraData(Date primeiraData) {
		this.primeiraData = primeiraData;
	}

	public Date getSegundaData() {
		return segundaData;
	}

	public void setSegundaData(Date segundaData) {
		this.segundaData = segundaData;
	}

	public boolean isPesquisarVisivel() {
		return pesquisarVisivel;
	}

	public void setPesquisarVisivel(boolean pesquisarVisivel) {
		this.pesquisarVisivel = pesquisarVisivel;
	}
	
	public boolean isTotalVisivel() {
		return totalVisivel;
	}
	
	public void setTotalVisivel(boolean totalVisivel) {
		this.totalVisivel = totalVisivel;
	}
}
