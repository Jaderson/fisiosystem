package com.fisio.web.bean.profissao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.profissao.ProfissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.profissao.Profissao;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.relatorio.GeradorRelatorioDinamico;
import com.fisio.relatorio.profissao.ProfissaoRelatorioDinamico;
import com.fisio.web.consulta.profissao.ProfissaoConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class ProfissaoBean extends AbstractBean<Profissao>{

	private ProfissaoFacade profissoesFacade;
	private ProfissaoConsulta consulta;
	
	private ProfissaoRelatorioDinamico profissaoRelatorioDinamico;
	
	@Autowired
	public ProfissaoBean(ProfissaoFacade profissoesFacade, ProfissaoConsulta consulta,
			ProfissaoRelatorioDinamico profissaoRelatorioDinamico) {
		this.profissoesFacade = profissoesFacade;
		this.consulta = consulta;
		this.profissaoRelatorioDinamico = profissaoRelatorioDinamico;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Profissao());
	}
	
	@Override
	public void filtro() {
		Profissao profissoesConsulta = new Profissao();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.profissoesFacade.filtro(listaRestricoes, profissoesConsulta, null));
	}
	
	@Override
	public Profissao getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Profissao();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Profissao> getFacade() {
		return this.profissoesFacade;
	}
	
	public ProfissaoConsulta getConsulta() {
		return consulta;
	}
	
	@Override
	protected GeradorRelatorioDinamico<Profissao> getRelatorio() {
		return this.profissaoRelatorioDinamico;
	}
}
