package com.fisio.web.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@ManagedBean
@Scope("view")
public class pageBean {

	private MapModel simpleModel;

	 @PostConstruct
	    public void init() {
	        simpleModel = new DefaultMapModel();
	          
	        LatLng coord1 = new LatLng(-9.6511692, -35.7118343);
	          
	        //Basic marker
	        simpleModel.addOverlay(new Marker(coord1, "FisioCorpus Pilates"));
	    }

	public MapModel getSimpleModel() {
		return simpleModel;
	}

}
