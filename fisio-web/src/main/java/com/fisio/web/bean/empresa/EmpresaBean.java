package com.fisio.web.bean.empresa;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.empresa.EmpresaFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.empresa.Empresa;
import com.fisio.web.constantes.enumerator.Estado;
import com.fisio.web.consulta.empresa.EmpresaConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class EmpresaBean extends AbstractBean<Empresa>{

	private EmpresaFacade empresaFacade;
	private EmpresaConsulta consulta;
	
	/*private EmpresaRelatorioDinamico EmpresaRelatorioDinamico;*/
	
	@Autowired
	public EmpresaBean(EmpresaFacade empresaFacade, EmpresaConsulta consulta) {
		this.empresaFacade = empresaFacade;
		this.consulta = consulta;
		/*this.empresaRelatorioDinamico = empresaRelatorioDinamico*/
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Empresa());
	}
	
	public void buscarEnderecoPorCEP(){
	}
	
	public SelectItem[] getListaEstados() {
		int i = 0;
		SelectItem[] items = new SelectItem[Estado.values().length];
		for(Estado estado: Estado.values()) {
			items[i++] = new SelectItem(estado, estado.getEstado());
		}
		
		return items;
	}
	
	@Override
	public void filtro() {
		Empresa empresaConsulta = new Empresa();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nomeFantasia", this.filtro));
		}
		setListFiltrado(this.empresaFacade.filtro(listaRestricoes, empresaConsulta, null));
	}
	
	@Override
	public Empresa getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Empresa();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Empresa> getFacade() {
		return this.empresaFacade;
	}
	
	public EmpresaConsulta getConsulta() {
		return consulta;
	}
	
	/*@Override
	protected GeradorRelatorioDinamico<Empresa> getRelatorio() {
		return this.empresaRelatorioDinamico;
	}*/ 
}
