package com.fisio.web.bean.palmilha;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.palmilha.PalmilhaFacade;
import com.fisio.framework.avaliacoes.Palmilha;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.web.consulta.palmilha.PalmilhaConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class PalmilhaBean extends AbstractBean<Palmilha>{

	private PalmilhaFacade palmilhaFacade;
	private PalmilhaConsulta consulta;
	
	@Autowired
	public PalmilhaBean(PalmilhaFacade palmilhaFacade, PalmilhaConsulta consulta) {
		this.palmilhaFacade = palmilhaFacade;
		this.consulta = consulta;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Palmilha());
	}
	
	@Override
	public void filtro() {
		Palmilha palmilhaConsulta = new Palmilha();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.palmilhaFacade.filtro(listaRestricoes, palmilhaConsulta, null));
	}
	
	@Override
	public Palmilha getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Palmilha();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Palmilha> getFacade() {
		return this.palmilhaFacade;
	}
	
	public PalmilhaConsulta getConsulta() {
		return consulta;
	}
}
