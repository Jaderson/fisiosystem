package com.fisio.web.bean.permissao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.acao.AcaoFacade;
import com.fisio.facade.objeto.ObjetoFacade;
import com.fisio.facade.permissao.PermissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Acao;
import com.fisio.model.autenticacao.Objeto;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.web.consulta.permissao.PermissaoConsulta;
import com.fisio.web.converter.acao.AcaoConversor;
import com.fisio.web.converter.objeto.ObjetoConversor;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class PermissaoBean extends AbstractBean<Permissao>{

	private PermissaoFacade permissaoFacade;
	private ObjetoFacade objetoFacade;
	private AcaoFacade acaoFacade;
	private PermissaoConsulta consulta;
	
	private AcaoConversor acaoConversor;
	private ObjetoConversor objetoConversor;
	
	@Autowired
	public PermissaoBean(PermissaoFacade permissaoFacade, PermissaoConsulta consulta, 
			ObjetoFacade objetoFacade, AcaoFacade acaoFacade) {
		this.permissaoFacade = permissaoFacade;
		this.objetoFacade = objetoFacade;
		this.acaoFacade = acaoFacade;
		this.consulta = consulta;
		this.acaoConversor = new AcaoConversor();
		this.objetoConversor = new ObjetoConversor();
	}
	
	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(Permissao entidade) {
		if (acaoIhObjetoForamSelecioandos(entidade)) {
			entidade.setDescricaoSistema(entidade.getObjeto().getDescricao() + "_" + entidade.getAcao().getDescricao());
		}
	}
	
	public List<Objeto> getListaObjetos() {
		Objeto objetoConsulta = new Objeto();
		return this.objetoFacade.consultarTodos(objetoConsulta, new ArrayList<Restricoes>(), objetoConsulta.getCampoOrderBy());
	}
	
	public List<Acao> getListaAcoes() {
		Acao acaoConsulta = new Acao();
		return this.acaoFacade.consultarTodos(acaoConsulta, new ArrayList<Restricoes>(), acaoConsulta.getCampoOrderBy());
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Permissao());
	}
	
	@Override
	public void filtro() {
		Permissao permissaoConsulta = new Permissao();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.permissaoFacade.filtro(listaRestricoes, permissaoConsulta, permissaoConsulta.getCampoOrderBy()));
	}
	
	@Override
	public Permissao getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Permissao();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Permissao> getFacade() {
		return this.permissaoFacade;
	}
	
	public PermissaoConsulta getConsulta() {
		return consulta;
	}
	
	public AcaoConversor getAcaoConversor() {
		return acaoConversor;
	}
	
	public ObjetoConversor getObjetoConversor() {
		return objetoConversor;
	}
	
	private boolean acaoIhObjetoForamSelecioandos(Permissao entidade) {
		return VerificadorUtil.naoEstaNulo(entidade.getObjeto()) && VerificadorUtil.naoEstaNulo(entidade.getAcao());
	}
}
