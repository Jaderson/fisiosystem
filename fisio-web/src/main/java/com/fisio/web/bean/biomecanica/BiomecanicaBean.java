package com.fisio.web.bean.biomecanica;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.biomecanica.BiomecanicaFacade;
import com.fisio.facade.biomecanicaPalmilha.BiomecanicaPalmilhaFacade;
import com.fisio.facade.palmilha.PalmilhaFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.avaliacoes.Biomecanica;
import com.fisio.framework.avaliacoes.BiomecanicaPalmilha;
import com.fisio.framework.avaliacoes.Lado;
import com.fisio.framework.avaliacoes.Palmilha;
import com.fisio.framework.avaliacoes.Resultado;
import com.fisio.framework.avaliacoes.TipoMedida;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.web.consulta.biomecanica.BiomecanicaConsulta;
import com.fisio.web.converter.pessoa.PessoaConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class BiomecanicaBean extends AbstractBean<Biomecanica> {

	private BiomecanicaFacade biomecanicaFacade;
	private BiomecanicaConsulta consulta;
	private BiomecanicaPalmilhaFacade biomecanicaPalmilhaFacade;
	private PalmilhaFacade palmilhaFacade;
	private PessoaFacade pessoaFacade;

	private Palmilha palmilha;
	private PessoaConversor pessoaConversor;
	private List<Palmilha> listPalmilha = new ArrayList<Palmilha>();

	@Autowired
	public BiomecanicaBean(BiomecanicaFacade biomecanicaFacade,
			BiomecanicaConsulta consulta, PessoaFacade pessoaFacade,
			PalmilhaFacade palmilhaFacade,
			BiomecanicaPalmilhaFacade biomecanicaPalmilhaFacade) {
		this.biomecanicaPalmilhaFacade = biomecanicaPalmilhaFacade;
		this.biomecanicaFacade = biomecanicaFacade;
		this.consulta = consulta;
		this.pessoaFacade = pessoaFacade;
		this.palmilhaFacade = palmilhaFacade;
		this.pessoaConversor = new PessoaConversor();
		this.palmilha = new Palmilha();
		this.consulta.setUsuario(getUsuarioLogado());
		this.consulta.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
	}

	@Override
	public void novo() {
		getListPalmilha().clear();
		this.palmilha = new Palmilha();
		super.novo();
	}

	@Override
	public void excluir(Biomecanica entidade) {
		excluirBiomecanicaPalmilha(entidade);
		super.excluir(entidade);
	}

	@Override
	public void setSelecionarAlterar(Biomecanica entidade) {
		buscarPalmilha(entidade);
		super.setSelecionarAlterar(entidade);
	}

	@Override
	protected void modificarEntidadeAntesDaOperacaoParaCadastrar(Biomecanica entidade) {
		entidade.setUsuario(getUsuarioLogado());
		entidade.setBiomecanicaPalmilhas(new HashSet<BiomecanicaPalmilha>());
		for (Palmilha p : getListPalmilha()) {
			Palmilha palmilha = popularPalmilha(p);
			BiomecanicaPalmilha entidadePalmilha = new BiomecanicaPalmilha(entidade, palmilha);
			entidadePalmilha.setPalmilha(palmilha);
			this.palmilhaFacade.cadastrar(palmilha);
			entidade.getBiomecanicaPalmilhas().add(entidadePalmilha);
		}
	}

	private void excluirBiomecanicaPalmilha(Biomecanica entidade) {
		BiomecanicaPalmilha biomecanicaPalmilha = new BiomecanicaPalmilha();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("biomecanica.id", entidade.getId()));
		List<BiomecanicaPalmilha> listaBiomecanicaPalmilha = this.biomecanicaPalmilhaFacade
				.consultarTodos(new BiomecanicaPalmilha(), listaRestricoes,
						biomecanicaPalmilha.getCampoOrderBy());
		for (BiomecanicaPalmilha biomecanicaPalmilhas : listaBiomecanicaPalmilha) {
			this.biomecanicaPalmilhaFacade.excluir(biomecanicaPalmilhas);
			this.palmilhaFacade.excluir(biomecanicaPalmilha.getPalmilha());
		}
	}

	private void buscarPalmilha(Biomecanica entidade) {
		getListPalmilha().clear();
		BiomecanicaPalmilha biomecanicaPalmilha = new BiomecanicaPalmilha();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("biomecanica.id", entidade.getId()));
		List<BiomecanicaPalmilha> listaBiomecanicaPalmilha = this.biomecanicaPalmilhaFacade
				.consultarTodos(new BiomecanicaPalmilha(), listaRestricoes,biomecanicaPalmilha.getCampoOrderBy());
		for (BiomecanicaPalmilha biomecanicaPalmilhas : listaBiomecanicaPalmilha) {
			List<Restricoes> listaRestricoesPalmilha = new ArrayList<Restricoes>();
			listaRestricoesPalmilha.add(Restricoes.igual("id", biomecanicaPalmilhas.getPalmilha().getId()));
			List<Palmilha> listaPalmilha = this.palmilhaFacade.consultarTodos(new Palmilha(), listaRestricoesPalmilha,null);
			for (Palmilha palmilha : listaPalmilha) {
				getListPalmilha().add(palmilha);
			}
		}

		if(!getListPalmilha().isEmpty()){
			popuplarPalmilhaAoEditar(getListPalmilha().get(0));
		}
		setDadosPalmilha();
	}
	
	private void popuplarPalmilhaAoEditar(Palmilha entidade) {
		this.palmilha.setAbsorcao(entidade.getAbsorcao());
		this.palmilha.setAltura(entidade.getAltura());
		this.palmilha.setAntepe(entidade.getAntepe());
		this.palmilha.setArco(entidade.getArco());
		this.palmilha.setBorda(entidade.getBorda());
		this.palmilha.setCobertura(entidade.getBorda());
		this.palmilha.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
		this.palmilha.setLado(entidade.getLado());
		this.palmilha.setNumeracao(entidade.getNumeracao());
		this.palmilha.setPad(entidade.getPad());
		this.palmilha.setRetrope(entidade.getRetrope());
		this.palmilha.setSuporte(entidade.getSuporte());
	}

	public void setDadosPalmilha(){
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("lado", getPalmilha().getLado()));
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		Palmilha palmilha = this.palmilhaFacade.consultarEntidade(new Palmilha(),listaRestricoes);
		setPalmilha(palmilha);
	}

	public void salvarPalmilha() {
		this.listPalmilha.add(popularPalmilha(this.palmilha));
	}
	
	public void alterarPalmilha(Palmilha entidade){
		this.palmilhaFacade.alterar(entidade);
	}

	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}

	@Override
	protected void limparEntidade() {
		setEntidade(new Biomecanica());
	}

	private Palmilha popularPalmilha(Palmilha p) {
		Palmilha palmilha = new Palmilha();
		palmilha.setAbsorcao(p.getAbsorcao());
		palmilha.setAltura(p.getAltura());
		palmilha.setAntepe(p.getAntepe());
		palmilha.setArco(p.getArco());
		palmilha.setBorda(p.getBorda());
		palmilha.setCobertura(p.getBorda());
		palmilha.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
		palmilha.setLado(p.getLado());
		palmilha.setNumeracao(p.getNumeracao());
		palmilha.setPad(p.getPad());
		palmilha.setRetrope(p.getRetrope());
		palmilha.setSuporte(p.getSuporte());
		return palmilha;
	}

	public void setDadosPessoas() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa().getId()));
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Não));
		Pessoa pessoa = this.pessoaFacade.consultarEntidade(new Pessoa(),
				listaRestricoes);
		getEntidade().setPessoa(pessoa);
	}

	@Override
	public void filtro() {
		Biomecanica biomecanicaConsulta = new Biomecanica();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		if (this.filtro != null) {
			listaRestricoes.add(Restricoes.like("pessoa.nome", this.filtro));
		}
		setListFiltrado(this.biomecanicaFacade.filtro(listaRestricoes,
				biomecanicaConsulta, null));
	}

	public List<ItemCombo> getListaPessoas() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Pessoa pessoaConsulta = new Pessoa();

		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Não));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
		
		if (VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa().getId()));
		}

		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes,pessoaConsulta.getCampoOrderBy());

		for (Pessoa pessoa : listaPessoas) {
			listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
		}
		return listaItemCombo;
	}

	public List<String> getListaTipoMedida() {
		final List<String> lista = new ArrayList<String>();
		for (TipoMedida tipoMedida : Arrays.asList(TipoMedida.values())) {
			if (tipoMedida.equals(TipoMedida.Normal)
					|| tipoMedida.equals(TipoMedida.Menor)
					|| tipoMedida.equals(TipoMedida.Maior)) {
				lista.add(tipoMedida.getValor());
			}
		}
		return lista;
	}

	@Override
	public Biomecanica getEntidade() {
		if (VerificadorUtil.estaNulo(entidade)) {
			entidade = new Biomecanica();
		}
		return entidade;
	}

	public List<String> getListaTipoMedidaNormalAlterada() {
		final List<String> lista = new ArrayList<String>();
		for (TipoMedida tipoMedida : Arrays.asList(TipoMedida.values())) {
			if (tipoMedida.equals(TipoMedida.Normal)
					|| tipoMedida.equals(TipoMedida.Alterada)) {
				lista.add(tipoMedida.getValor());
			}
		}
		return lista;
	}

	public List<String> getListaLado() {
		final List<String> lista = new ArrayList<String>();
		for (Lado lado : Arrays.asList(Lado.values())) {
			if (!lado.equals(Lado.Bilateral)) {
				lista.add(lado.getValor());
			}
		}
		return lista;
	}

	public List<String> getListaResultado() {
		final List<String> lista = new ArrayList<String>();
		for (Resultado resultado : Arrays.asList(Resultado.values())) {
			lista.add(resultado.getResultado());
		}
		return lista;
	}

	@Override
	protected Facade<Biomecanica> getFacade() {
		return this.biomecanicaFacade;
	}

	public BiomecanicaConsulta getConsulta() {
		return consulta;
	}

	public PessoaConversor getPessoaConversor() {
		return pessoaConversor;
	}

	public void setPessoaConversor(PessoaConversor pessoaConversor) {
		this.pessoaConversor = pessoaConversor;
	}

	public List<Palmilha> getListPalmilha() {
		return listPalmilha;
	}

	public void setListPalmilha(List<Palmilha> listPalmilha) {
		this.listPalmilha = listPalmilha;
	}

	public Palmilha getPalmilha() {
		return palmilha;
	}

	public void setPalmilha(Palmilha palmilha) {
		this.palmilha = palmilha;
	}

}
