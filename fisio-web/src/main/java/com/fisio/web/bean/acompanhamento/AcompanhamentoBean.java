package com.fisio.web.bean.acompanhamento;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.acompanhamento.AcompanhamentoFacade;
import com.fisio.facade.acompanhamentoFilho.AcompanhamentoFilhoFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.framework.acompanhamento.AcompanhamentoPessoa;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.web.consulta.acompanhamento.AcompanhamentoConsulta;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class AcompanhamentoBean extends AbstractBean<AcompanhamentoPessoa>{

	private AcompanhamentoFacade acompanhamentoFacade;
	private AcompanhamentoConsulta consulta;
	private AcompanhamentoFilho acompanhamentoFilho;
	private AcompanhamentoFilho acompanhamentoFilhoParaAlterar;
	private List<AcompanhamentoFilho> listAcompanhamentoFilho = new ArrayList<AcompanhamentoFilho>();
	private List<AcompanhamentoFilho> listAcompanhamentoFilhoDetalhes = new ArrayList<AcompanhamentoFilho>();
	private AcompanhamentoFilhoFacade acompanhamentoFilhoFacade;
	private PessoaFacade pessoaFacade;
	
	@Autowired
	public AcompanhamentoBean(AcompanhamentoFacade acompanhamentoFacade, AcompanhamentoConsulta consulta,
			AcompanhamentoFilhoFacade acompanhamentoFilhoFacade, PessoaFacade pessoaFacade) {
		this.acompanhamentoFacade = acompanhamentoFacade;
		this.consulta = consulta;
		this.acompanhamentoFilhoFacade = acompanhamentoFilhoFacade;
		this.pessoaFacade = pessoaFacade;
		this.consulta.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
	}
	
	@Override
	protected void init() {
		Agenda entidadeParametro = (Agenda) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("AgendaParam");
		if(VerificadorUtil.naoEstaNulo(entidadeParametro) && VerificadorUtil.naoEstaNulo(entidadeParametro.getPessoa())){
			AcompanhamentoPessoa entidade = buscarAcompanhamentoPorPessoa(entidadeParametro.getPessoa());
			if(VerificadorUtil.naoEstaNulo(entidade)){
				setSelecionarAlterar(entidade);
			}else{
				novo();
				getEntidade().setPessoa(entidadeParametro.getPessoa());
			}
		}
		super.init();
	}
	
	@Override
	public void novo() {
		getListAcompanhamentoFilho().clear();
		super.novo();
	}
	
	public void selecionarItemAlterar(AcompanhamentoFilho item) {
		setAcompanhamentoFilhoParaAlterar(item);
	}
	

	public void alterarItem() {
		this.acompanhamentoFilhoFacade.alterar(getAcompanhamentoFilhoParaAlterar());
		setAcompanhamentoFilhoParaAlterar(null);
		buscarListAcompanhamentoFilho(getEntidade());
	}
	
	
	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(AcompanhamentoPessoa entidade) {
		entidade.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
		entidade.setAcompanhamento(new HashSet<AcompanhamentoFilho>());
		entidade.setPessoa(getEntidade().getPessoa());
		for (AcompanhamentoFilho acompanhamentoFilho : getListAcompanhamentoFilho()) {
			this.acompanhamentoFilhoFacade.cadastrar(acompanhamentoFilho);
		}
	}
	
	public void excluirAcompanhamentoFilho(AcompanhamentoFilho entidade) {
		AcompanhamentoFilho acompanhamentoFilho = new AcompanhamentoFilho();
		
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("pessoa.id", entidade.getPessoa().getId()));
		listaRestricoes.add(Restricoes.igual("id", entidade.getId()));
		List<AcompanhamentoFilho> acompanhamentoFilhos = this.acompanhamentoFilhoFacade.consultarTodos(acompanhamentoFilho, listaRestricoes, acompanhamentoFilho.getCampoOrderBy());
		for (AcompanhamentoFilho item : acompanhamentoFilhos) {
			this.acompanhamentoFilhoFacade.excluir(item);
		}
		getListAcompanhamentoFilho().remove(entidade);
	}

	@Override
	public void setSelecionarAlterar(AcompanhamentoPessoa entidade) {
		setListAcompanhamentoFilho(buscarListAcompanhamentoFilho(entidade));
		super.setSelecionarAlterar(entidade);
	}
	
	@Override
	public void setSelecionar(AcompanhamentoPessoa entidade) {
		consultaItensFilho(entidade.getPessoa().getId());
		super.setSelecionar(entidade);
	}
	
	private List<AcompanhamentoFilho> buscarListAcompanhamentoFilho(AcompanhamentoPessoa entidade) {
		AcompanhamentoFilho acompanhamentoFilho = new AcompanhamentoFilho();
			
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("pessoa.id", entidade.getPessoa().getId()));

		return this.acompanhamentoFilhoFacade.consultarTodos(acompanhamentoFilho, listaRestricoes, acompanhamentoFilho.getCampoOrderBy());
	}

	public void adicionarAcompanhamentoFilho(AcompanhamentoFilho entidade){
		entidade.setPessoa(getEntidade().getPessoa());
		entidade.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
		getListAcompanhamentoFilho().add(entidade);
		setAcompanhamentoFilho(new AcompanhamentoFilho());
	}
	
	private AcompanhamentoPessoa buscarAcompanhamentoPorPessoa(Pessoa entidade){
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("pessoa.id", entidade.getId()));
		return this.acompanhamentoFacade.consultarEntidade(new AcompanhamentoPessoa(), listaRestricoes);
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new AcompanhamentoPessoa());
	}
	
	public List<ItemCombo> getListaPessoas() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Pessoa pessoaConsulta = new Pessoa();
			
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Não));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));
			
		if (VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa().getId()));
		}

		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes, pessoaConsulta.getCampoOrderBy());
		
		for (Pessoa pessoa : listaPessoas) {
				listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
		}
		return listaItemCombo;
	} 
	
	@Override
	public void filtro() {
		AcompanhamentoPessoa acompanhamentoConsulta = new AcompanhamentoPessoa();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("pessoa.nome", this.filtro));
		}
		setListFiltrado(this.acompanhamentoFacade.filtro(listaRestricoes, acompanhamentoConsulta,null));
	}
	
	private void consultaItensFilho(Integer idPessoa) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("pessoa.id", idPessoa));
		setListAcompanhamentoFilhoDetalhes(listAcompanhamentoFilho = this.acompanhamentoFilhoFacade.consultarTodos(new AcompanhamentoFilho(), listaRestricoes, null));
	}

	@Override
	public AcompanhamentoPessoa getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new AcompanhamentoPessoa();
		}
		return entidade;
	}
	
	@Override
	protected Facade<AcompanhamentoPessoa> getFacade() {
		return this.acompanhamentoFacade;
	}
	
	public AcompanhamentoConsulta getConsulta() {
		return consulta;
	}
	
	public AcompanhamentoFilho getAcompanhamentoFilho() {
		if(VerificadorUtil.estaNulo(this.acompanhamentoFilho)) {
			this.acompanhamentoFilho = new AcompanhamentoFilho();
		}
		return acompanhamentoFilho;
	}
	
	public void setAcompanhamentoFilho(AcompanhamentoFilho acompanhamentoFilho) {
		this.acompanhamentoFilho = acompanhamentoFilho;
	}
	
	public List<AcompanhamentoFilho> getListAcompanhamentoFilho() {
		return listAcompanhamentoFilho;
	}
	
	public void setListAcompanhamentoFilho(List<AcompanhamentoFilho> listAcompanhamentoFilho) {
		this.listAcompanhamentoFilho = listAcompanhamentoFilho;
	}
	
	public AcompanhamentoFilho getAcompanhamentoFilhoParaAlterar() {
		return acompanhamentoFilhoParaAlterar;
	}
	
	public void setAcompanhamentoFilhoParaAlterar(
			AcompanhamentoFilho acompanhamentoFilhoParaAlterar) {
		this.acompanhamentoFilhoParaAlterar = acompanhamentoFilhoParaAlterar;
	}

	public List<AcompanhamentoFilho> getListAcompanhamentoFilhoDetalhes() {
		return listAcompanhamentoFilhoDetalhes;
	}
	
	public void setListAcompanhamentoFilhoDetalhes(
			List<AcompanhamentoFilho> listAcompanhamentoFilhoDetalhes) {
		this.listAcompanhamentoFilhoDetalhes = listAcompanhamentoFilhoDetalhes;
	}
	
}
