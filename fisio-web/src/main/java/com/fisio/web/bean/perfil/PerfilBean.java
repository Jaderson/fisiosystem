package com.fisio.web.bean.perfil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.perfil.PerfilFacade;
import com.fisio.facade.perfilPermissao.PerfilPermissaoFacade;
import com.fisio.facade.permissao.PermissaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.web.consulta.perfil.PerfilConsulta;
import com.fisio.web.converter.permissao.PermissaoConversor;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class PerfilBean extends AbstractBean<Perfil> {

	private PerfilFacade perfilFacade;
	private PermissaoFacade permissaoFacade;
	private PerfilPermissaoFacade perfilPermissaoFacade;
	private PerfilConsulta consulta;

	private PermissaoConversor permissaoConversor;

	private DualListModel<Permissao> listaPermissoesDisponiveis;

	@Autowired
	public PerfilBean(PerfilFacade perfilFacade,
			PermissaoFacade permissaoFacade,
			PerfilPermissaoFacade perfilPermissaoFacade, PerfilConsulta consulta) {
		this.perfilFacade = perfilFacade;
		this.permissaoFacade = permissaoFacade;
		this.perfilPermissaoFacade = perfilPermissaoFacade;
		this.permissaoConversor = new PermissaoConversor();
		this.consulta = consulta;
		this.consulta.setUsuario(getUsuarioLogado());
		inicializarDualListModel();
	}

	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(Perfil entidade) {
		entidade.setPermissoes(new HashSet<PerfilPermissao>());
		for (Permissao permissao : getListaPermissoesDisponiveis().getTarget()) {
			PerfilPermissao perfilPermissao = new PerfilPermissao(entidade,
					permissao);
			entidade.getPermissoes().add(perfilPermissao);
		}
	}

	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}

	@Override
	protected void limparEntidade() {
		setEntidade(new Perfil());
	}

	@Override
	public void setSelecionarAlterar(Perfil entidade) {
		super.setSelecionarAlterar(entidade);
		inicializarDualListModel();
	}

	public DualListModel<Permissao> getListaPermissoesDisponiveis() {
		return listaPermissoesDisponiveis;
	}

	public void setListaPermissoesDisponiveis(
			DualListModel<Permissao> listaPerfilPermissoesDisponiveis) {
		this.listaPermissoesDisponiveis = listaPerfilPermissoesDisponiveis;
	}

	@Override
	public void filtro() {
		Perfil perfilConsulta = new Perfil();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if (this.filtro != null) {
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.perfilFacade.filtro(listaRestricoes,
				perfilConsulta, null));
	}

	@Override
	public Perfil getEntidade() {
		if (VerificadorUtil.estaNulo(entidade)) {
			entidade = new Perfil();
		}
		return entidade;
	}

	@Override
	protected Facade<Perfil> getFacade() {
		return this.perfilFacade;
	}

	public PerfilConsulta getConsulta() {
		return consulta;
	}

	public PermissaoConversor getPermissaoConversor() {
		return permissaoConversor;
	}

	private void inicializarDualListModel() {
		List<Permissao> listaPermissao = this.permissaoFacade
				.consultarTodos(new Permissao());

		List<Permissao> permissoesDoPerfil = recuperarPermissoesDoPerfil(getEntidade()
				.getId());
		listaPermissao.removeAll(permissoesDoPerfil);

		listaPermissoesDisponiveis = new DualListModel<Permissao>(
				listaPermissao, permissoesDoPerfil);
	}

	private List<Permissao> recuperarPermissoesDoPerfil(Integer id) {
		List<Permissao> listaPermissoes = new ArrayList<Permissao>();
		if (VerificadorUtil.naoEstaNulo(getEntidade().getId())) {
			List<Restricoes> listaRestricoesPerfilPermissao = new ArrayList<Restricoes>();
			listaRestricoesPerfilPermissao.add(Restricoes.igual("perfil.id",
					getEntidade().getId()));
			List<PerfilPermissao> listaPerfilPermissao = this.perfilPermissaoFacade
					.consultarTodos(new PerfilPermissao(),
							listaRestricoesPerfilPermissao, null);
			for (PerfilPermissao perfilPermissao : listaPerfilPermissao) {
				listaPermissoes.add(perfilPermissao.getPermissao());
			}
		}

		return listaPermissoes;
	}
}
