package com.fisio.web.bean.acao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.acao.AcaoFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Acao;
import com.fisio.web.consulta.acao.AcaoConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class AcaoBean extends AbstractBean<Acao>{

	private AcaoFacade acaoFacade;
	private AcaoConsulta consulta;
	
	@Autowired
	public AcaoBean(AcaoFacade acaoFacade, AcaoConsulta consulta) {
		this.acaoFacade = acaoFacade;
		this.consulta = consulta;
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Acao());
	}
	
	@Override
	public void filtro() {
		Acao acaoConsulta = new Acao();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("descricao", this.filtro));
		}
		setListFiltrado(this.acaoFacade.filtro(listaRestricoes, acaoConsulta, null));
	}
	
	@Override
	public Acao getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Acao();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Acao> getFacade() {
		return this.acaoFacade;
	}
	
	public AcaoConsulta getConsulta() {
		return consulta;
	}
}
