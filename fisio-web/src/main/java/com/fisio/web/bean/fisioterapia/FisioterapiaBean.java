package com.fisio.web.bean.fisioterapia;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.fisioterapia.FisioterapiaFacade;
import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.avaliacoes.Fisioterapia;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.model.pessoa.SimNao;
import com.fisio.web.consulta.fisioterapia.FisioterapiaConsulta;
import com.fisio.web.converter.pessoa.PessoaConversor;
import com.fisio.web.framework.combomodel.ItemCombo;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class FisioterapiaBean extends AbstractBean<Fisioterapia>{

	private FisioterapiaFacade fisioterapiaFacade;
	private FisioterapiaConsulta consulta;
	
	private PessoaConversor pessoaConversor;
	private PessoaFacade pessoaFacade;
	
	@Autowired
	public FisioterapiaBean(FisioterapiaFacade fisioterapiaFacade, FisioterapiaConsulta consulta,
			PessoaFacade pessoaFacade) {
		this.fisioterapiaFacade = fisioterapiaFacade;
		this.consulta = consulta;
		this.pessoaFacade = pessoaFacade;
		this.pessoaConversor = new PessoaConversor();
		this.consulta.setUsuario(getUsuarioLogado());
		this.consulta.setEmpresa(getUsuarioLogado().getPessoa().getEmpresa());
	}
	
	@Override
	protected void modificarEntidadeAntesDeCadastrarEhOuAlterar(
			Fisioterapia entidade) {
		entidade.setUsuario(getUsuarioLogado());
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Fisioterapia());
	}
	
	@Override
	public void filtro() {
		Fisioterapia fisioterapiaConsulta = new Fisioterapia();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("pessoa.nome", this.filtro));
		}
		setListFiltrado(this.fisioterapiaFacade.filtro(listaRestricoes, fisioterapiaConsulta, null));
	}
	
	public List<ItemCombo> getListaPessoas() {
		List<ItemCombo> listaItemCombo = new ArrayList<ItemCombo>();
		Pessoa pessoaConsulta = new Pessoa();

		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado().getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Não));
		listaRestricoes.add(Restricoes.igual("ativo", SimNao.Sim));

		if (VerificadorUtil.naoEstaNulo(getEntidade().getPessoa())) {
			listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa().getId()));
		}
		
		List<Pessoa> listaPessoas = this.pessoaFacade.consultarTodos(pessoaConsulta, listaRestricoes,pessoaConsulta.getCampoOrderBy());

		for (Pessoa pessoa : listaPessoas) {
			listaItemCombo.add(new ItemCombo(pessoa.getNome(), pessoa));
		}
		return listaItemCombo;
	}
	
	public void setDadosPessoas() {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("id", getEntidade().getPessoa()
				.getId()));
		listaRestricoes.add(Restricoes.igual("empresa.id", getUsuarioLogado()
				.getPessoa().getEmpresa().getId()));
		listaRestricoes.add(Restricoes.igual("funcionario", SimNao.Não));

		Pessoa pessoa = this.pessoaFacade.consultarEntidade(new Pessoa(),
				listaRestricoes);
		getEntidade().setPessoa(pessoa);
	}
	
	@Override
	public Fisioterapia getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Fisioterapia();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Fisioterapia> getFacade() {
		return this.fisioterapiaFacade;
	}
	
	public FisioterapiaConsulta getConsulta() {
		return consulta;
	}
	
	public PessoaConversor getPessoaConversor() {
		return pessoaConversor;
	}

	public void setPessoaConversor(PessoaConversor pessoaConversor) {
		this.pessoaConversor = pessoaConversor;
	}
}
