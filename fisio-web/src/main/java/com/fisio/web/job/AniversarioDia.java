package com.fisio.web.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class AniversarioDia implements Job{
	
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		 SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy – hh:mm:ss");
		 System.out.println("Rodou: " + dateFormat.format( new Date() )); 
	} 
}
