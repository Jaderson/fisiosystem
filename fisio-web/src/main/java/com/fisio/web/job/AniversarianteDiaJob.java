package com.fisio.web.job;

import javax.faces.bean.ManagedBean;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class AniversarianteDiaJob extends AbstractBean<Pessoa>{
	
	@Autowired PessoaFacade pessoaFacade;
	
	 public static void inicia() throws Exception {
	        JobDetail job = JobBuilder.newJob(AniversarioDia.class).withIdentity(
	                "tarefaAloMundo", "group1").build();
	        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(
	                "gatilhoAloMundo", "group1").withSchedule(
	                CronScheduleBuilder.cronSchedule("0/5 * * * * ?")).build();
	 
	        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
	        scheduler.start();
	        scheduler.scheduleJob(job, trigger);
	    }

	@Override
	protected void limparEntidade() {
		setEntidade(new Pessoa());		
	}

	@Override
	protected Pessoa getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Pessoa();
		}
		return entidade;
	}

	@Override
	protected Facade<Pessoa> getFacade() {
		return this.pessoaFacade;
	}
}
