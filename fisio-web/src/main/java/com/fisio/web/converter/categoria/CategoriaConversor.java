package com.fisio.web.converter.categoria;

import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.web.framework.converter.ConversorGeneric;

public class CategoriaConversor extends ConversorGeneric  {
	protected static final String key = "jsf.EntityConverter.Categoria";

	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if (value == null) {
			return EMPTY;
		}
		return ((Categoria) value).getId().toString();
	}
}
