package com.fisio.web.converter.horario;

import com.fisio.framework.agenda.Horario;
import com.fisio.web.framework.converter.ConversorGeneric;

public class HorarioConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.Horario";

	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if (value == null) {
			return EMPTY;
		}
		return ((Horario) value).getId().toString();
	}
}
