package com.fisio.web.converter.pessoa;

import com.fisio.model.autenticacao.Usuario;
import com.fisio.web.framework.converter.ConversorGeneric;

public class UsuarioConversor extends ConversorGeneric{

	protected static final String key = "jsf.EntityConverter.Usuario";
	
	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if(value == null) {
			return EMPTY;
		}
		return ((Usuario) value).getId().toString();
	}

}
