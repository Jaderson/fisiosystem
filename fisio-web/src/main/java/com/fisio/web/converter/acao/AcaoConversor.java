package com.fisio.web.converter.acao;

import com.fisio.model.autenticacao.Acao;
import com.fisio.web.framework.converter.ConversorGeneric;

public class AcaoConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.Acao";

	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if (value == null) {
			return EMPTY;
		}
		return ((Acao) value).getId().toString();
	}
}
