package com.fisio.web.converter.pessoa;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.pessoa.PessoaRepository;

@ManagedBean
@Component
@Scope("view")
public class PessoaConverter implements Converter, Serializable{

	private static final long serialVersionUID = 1L;
	@Autowired PessoaRepository pessoaRepository;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            try {
                return this.pessoaRepository.consultarPorId(Integer.parseInt(value));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        }
        else {
            return null;
        }
    }
 
	@Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            return ((Pessoa) object).getId().toString();
        }
        else {
            return null;
        }
    }   

}
