package com.fisio.web.converter.dataFalta;

import com.fisio.framework.agenda.DataFalta;
import com.fisio.web.framework.converter.ConversorGeneric;

public class DataFaltaConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.DataFalta";

	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if (value == null) {
			return EMPTY;
		}
		return ((DataFalta) value).getId().toString();
	}
}
