package com.fisio.web.converter.bancoConversor;

import com.fisio.framework.banco.Banco;
import com.fisio.web.framework.converter.ConversorGeneric;

public class BancoConversor  extends ConversorGeneric{

	protected static final String key = "jsf.EntityConverter.Banco";
	
	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if(value == null) {
			return EMPTY;
		}
		return ((Banco) value).getId().toString();
	}
}
