package com.fisio.web.converter.perfil;

import com.fisio.model.autenticacao.Perfil;
import com.fisio.web.framework.converter.ConversorGeneric;

public class PerfilConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.Perfil";

	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if (value == null) {
			return EMPTY;
		}
		return ((Perfil) value).getId().toString();
	}
}
