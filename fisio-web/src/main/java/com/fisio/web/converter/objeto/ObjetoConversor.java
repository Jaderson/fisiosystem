package com.fisio.web.converter.objeto;

import com.fisio.model.autenticacao.Objeto;
import com.fisio.web.framework.converter.ConversorGeneric;

public class ObjetoConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.Objeto";

	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if (value == null) {
			return EMPTY;
		}
		return ((Objeto) value).getId().toString();
	}
}
