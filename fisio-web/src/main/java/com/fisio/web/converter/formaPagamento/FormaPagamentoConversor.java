package com.fisio.web.converter.formaPagamento;

import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.web.framework.converter.ConversorGeneric;

public class FormaPagamentoConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.FormaPagamento";
	
	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if(value == null) {
			return EMPTY;
		}
		return ((FormaPagamento) value).getId().toString();
	}
}
