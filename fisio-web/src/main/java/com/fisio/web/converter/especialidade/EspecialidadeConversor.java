package com.fisio.web.converter.especialidade;

import com.fisio.framework.especialidade.Especialidade;
import com.fisio.web.framework.converter.ConversorGeneric;

public class EspecialidadeConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.Especialidade";
	
	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if(value == null) {
			return EMPTY;
		}
		return ((Especialidade) value).getId().toString();
	}

}
