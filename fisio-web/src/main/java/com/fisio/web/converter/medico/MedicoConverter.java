package com.fisio.web.converter.medico;

import com.fisio.framework.medico.Medico;
import com.fisio.web.framework.converter.ConversorGeneric;

public class MedicoConverter extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.Medico";
	
	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if(value == null) {
			return EMPTY;
		}
		return ((Medico) value).getId().toString();
	}
}
