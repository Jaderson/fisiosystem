package com.fisio.web.converter.permissao;

import com.fisio.model.autenticacao.Permissao;
import com.fisio.web.framework.converter.ConversorGeneric;

public class PermissaoConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.Permissao";

	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if (value == null) {
			return EMPTY;
		}
		return ((Permissao) value).getId().toString();
	}

}
