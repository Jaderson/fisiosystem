package com.fisio.web.converter.profissao;

import com.fisio.framework.profissao.Profissao;
import com.fisio.web.framework.converter.ConversorGeneric;

public class ProfissaoConversor extends ConversorGeneric {

	protected static final String key = "jsf.EntityConverter.Profissoes";

	@Override
	protected String getKey() {
		return key;
	}

	@Override
	protected String getID(Object value) {
		if(value == null) {
			return EMPTY;
		}
		return ((Profissao) value).getId().toString();
	}
}
