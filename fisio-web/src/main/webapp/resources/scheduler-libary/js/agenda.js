$(function(){
			$(".myscheduler").dhx_scheduler({
				date:new Date(),
				mode:"day"
			});
			
			scheduler.config.limit_time_select = true;
			scheduler.config.details_on_dblclick = true;
			scheduler.config.details_on_create = false;
			scheduler.locale.labels.funcionario = "funcionario";
			scheduler.locale.labels.especialidade = "especialidade";
			scheduler.templates.event_class=function(start, end, event){
				/*console.log(event);*/
				var css = "";

				if(event.funcionario)
					var nomeFuncionario = event.funcionario;
					var nFuncionario = nomeFuncionario.replace(" ","_");
						nFuncionario = nFuncionario.replace(" ","_");
					var nome = nFuncionario.replace("é", "e");
					css += "event_"+nome;

				if(event.id == scheduler.getState().select_id){
					css += " selected";
				}
				return css; 
			};
			
			var funcionario = [
				{ key: 'Jeymison Morais', label: 'Jeymison Morais' },
				{ key: 'Monique Oliveira', label: 'Monique Oliveira' },
				{ key: 'Abaeté Macena Filho', label: 'Abaeté Macena Filho' },
				{ key: 'Bruno Lucena', label: 'Bruno Lucena' },
				{ key: 'Guilherme Lyra', label: 'Guilherme Lyra' }
			];

			var especialidade = [
				{key:'Avaliação Biomecânica',  label: 'Avaliação Biomecânica'},
				{key:'Avaliação Fisioterapia', label: 'Avaliação Fisioterapia'},
				{key:'Fisioterapia Esportiva', label: 'Fisioterapia Esportiva'}
			]
			
			scheduler.config.lightbox.sections=[
				{name:"description", height:43, map_to:"text", type:"textarea" , focus:true},
				{name:"Funcionario", height:20, type:"select", options: funcionario, map_to:"funcionario" },
				{name:"Especialidade", height:20, type:"select", options: especialidade, map_to:"especialidade" },
				{name:"time", height:72, type:"time", map_to:"auto" }
			];
								
			$.getJSON('http://localhost:8080/fisio-web/api/agendas/listar/1', function(data) {
				this.qtd = data.results.length;
						
				for (i = 0; i < this.qtd; i++){
									
					var currentDateInicio = new Date(data.results[i].dataInicio)
				    var twoDigitMonth = ((currentDateInicio.getMonth().length+1) === 1)? (currentDateInicio.getMonth()+1) : '0' + (currentDateInicio.getMonth()+1);
			 		var dataInicio = currentDateInicio.getDate() + "/" + twoDigitMonth + "/" + currentDateInicio.getFullYear() + " " + currentDateInicio.getHours() + ":" + currentDateInicio.getMinutes();
					
					var currentDate = new Date(data.results[i].dataFim)
					var twoDigitMonth = ((currentDate.getMonth().length+1) === 1)? (currentDate.getMonth()+1) : '0' + (currentDate.getMonth()+1);
			 		var dataFim = currentDate.getDate() + "/" + twoDigitMonth + "/" + currentDate.getFullYear() + " " + currentDate.getHours() + ":" + currentDate.getMinutes();
					
					scheduler.addEventNow({
						start_date: dataInicio,
						end_date:   dataFim,
						text:       data.results[i].pessoa.nome,
						funcionario:  data.results[i].funcionario.nome,
						especialidade: data.results[i].especialidade.descricao
					});
				}
			});
		});