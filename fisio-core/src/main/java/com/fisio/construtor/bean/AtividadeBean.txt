package com.fisio.construtor.web.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fisio.facade.atividade.AtividadeFacade;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.facade.Facade;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.Atividade;
import com.fisio.web.consulta.atividade.AtividadeConsulta;
import com.fisio.web.generico.AbstractBean;

@Component
@ManagedBean
@Scope("view")
public class AtividadeBean extends AbstractBean<Atividade>{

	private AtividadeFacade atividadeFacade;
	private AtividadeConsulta consulta;
	
	/*private AtividadeRelatorioDinamico AtividadeRelatorioDinamico;*/
	
	@Autowired
	public AtividadeBean(AtividadeFacade atividadeFacade, AtividadeConsulta consulta, AtividadeRelatorioDinamico atividadeRelatorioDinamico) {
		this.atividadeFacade = atividadeFacade;
		this.consulta = consulta;
		/*this.atividadeRelatorioDinamico = atividadeRelatorioDinamico*/
	}
	
	public void fecharDetalhe() {
		setDetalharVisivel(false);
		limparEntidade();
	}
		
	@Override
	protected void limparEntidade() {
		setEntidade(new Atividade());
	}
	
	@Override
	public void filtro() {
		Atividade atividadeConsulta = new Atividade();
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		if(this.filtro != null){
			listaRestricoes.add(Restricoes.like("nome", this.filtro));
		}
		setListFiltrado(this.atividadeFacade.filtro(listaRestricoes, atividadeConsulta));
	}
	
	@Override
	public Atividade getEntidade() {
		if(VerificadorUtil.estaNulo(entidade)) {
			entidade = new Atividade();
		}
		return entidade;
	}
	
	@Override
	protected Facade<Atividade> getFacade() {
		return this.atividadeFacade;
	}
	
	public AtividadeConsulta getConsulta() {
		return consulta;
	}
	
	/*@Override
	protected GeradorRelatorioDinamico<Atividade> getRelatorio() {
		return this.atividadeRelatorioDinamico;
	}*/ 
}
