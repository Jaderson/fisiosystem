package com.fisio.facade.atividade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.atividade.AtividadeFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Atividade;
import com.fisio.service.atividade.AtividadeService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class AtividadeFacadeImpl extends AbstractFacade<Atividade> implements AtividadeFacade{

	private AtividadeService atividadeService;

	@Autowired
	public AtividadeFacadeImpl(AtividadeService atividadeService) {
		this.atividadeService = atividadeService;
	}
	
	@Override
	protected Service<Atividade> getService() {
		return this.atividadeService;
	}

}
