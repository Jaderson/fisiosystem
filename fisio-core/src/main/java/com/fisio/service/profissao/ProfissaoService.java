package com.fisio.service.profissao;

import com.fisio.framework.profissao.Profissao;
import com.fisio.framework.service.Service;

public interface ProfissaoService extends Service<Profissao>{
}
