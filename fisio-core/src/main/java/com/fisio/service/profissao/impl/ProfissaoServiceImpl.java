package com.fisio.service.profissao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.profissao.Profissao;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.framework.validador.Validador;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.Repository;
import com.fisio.repository.pessoa.PessoaRepository;
import com.fisio.repository.profissao.ProfissaoRepository;
import com.fisio.service.profissao.ProfissaoService;

@Component
public class ProfissaoServiceImpl extends AbstractService<Profissao> implements ProfissaoService{

	private ProfissaoRepository profissoesRepository;
	private PessoaRepository pessoaRepository;

	@Autowired
	public ProfissaoServiceImpl(Validador validador, ProfissaoRepository profissoesRepository, PessoaRepository pessoaRepository) {
		super(validador);
		this.profissoesRepository = profissoesRepository;
		this.pessoaRepository = pessoaRepository;
	}

	@Override
	protected Repository<Profissao> getRepository() {
		return this.profissoesRepository;
	}
	
	@Override
	protected void regrasNegocioExcluir(Profissao entidade) {
		verificarSeExistePessoaComProfissao(entidade);
	}
	
	private void verificarSeExistePessoaComProfissao(Profissao entidade) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("profissao.id", entidade.getId()));
		List<Pessoa> listPessoa = this.pessoaRepository.consultarTodos(new Pessoa(), listaRestricoes, null);
		if(VerificadorUtil.naoEstaNulo(listPessoa)){
			throw new ValidationException("impossivel_excluir_existe_paciente_vinculado");
		}
	}

	@Override
	protected void regrasNegocioAlterar(Profissao entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Profissao entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Profissao entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
