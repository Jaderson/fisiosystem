package com.fisio.service.permissao;

import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Permissao;

public interface PermissaoService extends Service<Permissao>{
}
