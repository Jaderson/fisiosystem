package com.fisio.service.permissao.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.repository.Repository;
import com.fisio.repository.permissao.PermissaoRepository;
import com.fisio.service.permissao.PermissaoService;

@Component
public class PermissaoServiceImpl extends AbstractService<Permissao> implements PermissaoService{

	private PermissaoRepository permissaoRepository;

	@Autowired
	public PermissaoServiceImpl(Validador validador, PermissaoRepository permissaoRepository) {
		super(validador);
		this.permissaoRepository = permissaoRepository;
	}

	@Override
	protected Repository<Permissao> getRepository() {
		return this.permissaoRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Permissao entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Permissao entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Permissao entidade) {
		
		StringBuffer msg = new StringBuffer();
		msg.append(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getObjeto(), "erro_escolha_um_objeto;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getAcao(), "erro_escolha_uma_acao;"));
		
		if (msg.length() > 0) {
			throw new ValidationException(msg.toString());
		}
	}

}
