package com.fisio.service.acao.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.framework.validador.Validador;
import com.fisio.model.autenticacao.Acao;
import com.fisio.repository.Repository;
import com.fisio.repository.acao.AcaoRepository;
import com.fisio.service.acao.AcaoService;

@Component
public class AcaoServiceImpl extends AbstractService<Acao> implements AcaoService{

	private AcaoRepository acaoRepository;

	@Autowired
	public AcaoServiceImpl(Validador validador, AcaoRepository acaoRepository) {
		super(validador);
		this.acaoRepository = acaoRepository;
	}

	@Override
	protected Repository<Acao> getRepository() {
		return this.acaoRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Acao entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Acao entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Acao entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
