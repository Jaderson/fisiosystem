package com.fisio.service.acao;

import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Acao;

public interface AcaoService extends Service<Acao>{
}
