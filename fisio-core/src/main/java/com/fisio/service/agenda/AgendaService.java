package com.fisio.service.agenda;

import java.util.Date;
import java.util.List;

import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.service.Service;

public interface AgendaService extends Service<Agenda>{

	void verificarCamposObrigatorios(Agenda entidade);

	List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente);

	List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresa);
}
