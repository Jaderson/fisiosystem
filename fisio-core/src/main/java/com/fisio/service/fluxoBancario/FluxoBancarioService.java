package com.fisio.service.fluxoBancario;

import com.fisio.framework.fluxoBancario.FluxoBancario;
import com.fisio.framework.service.Service;

public interface FluxoBancarioService extends Service<FluxoBancario>{
}
