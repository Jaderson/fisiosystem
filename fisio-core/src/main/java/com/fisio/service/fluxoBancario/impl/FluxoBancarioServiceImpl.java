package com.fisio.service.fluxoBancario.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.fluxoBancario.FluxoBancario;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.fluxoBancario.FluxoBancarioRepository;
import com.fisio.service.fluxoBancario.FluxoBancarioService;

@Component
public class FluxoBancarioServiceImpl extends AbstractService<FluxoBancario> implements FluxoBancarioService{

	private FluxoBancarioRepository fluxoBancarioRepository;

	@Autowired
	public FluxoBancarioServiceImpl(Validador validador, FluxoBancarioRepository fluxoBancarioRepository) {
		super(validador);
		this.fluxoBancarioRepository = fluxoBancarioRepository;
	}

	@Override
	protected Repository<FluxoBancario> getRepository() {
		return this.fluxoBancarioRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(FluxoBancario entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(FluxoBancario entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(FluxoBancario entidade) {
		
		StringBuffer msg = new StringBuffer();
		msg.append(verificarSeCampoEstaNulo(entidade.getBanco(), "erro_banco_nao_pode_ser_nula;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getTipoLancamento(), "erro_voce_deve_escolher_entre_receita_ou_despesas_o_campo_nao_pode_ser_nulo;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getValor(), "erro_valor_nao_pode_ser_nula;"));
		
		if (msg.length() > 0) {
			throw new ValidationException(msg.toString());
		}
	}

}
