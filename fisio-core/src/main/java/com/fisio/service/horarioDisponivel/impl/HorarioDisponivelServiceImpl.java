package com.fisio.service.horarioDisponivel.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.horarioDisponivel.HorarioDisponivelRepository;
import com.fisio.service.horarioDisponivel.HorarioDisponivelService;

@Component
public class HorarioDisponivelServiceImpl extends AbstractService<HorarioDisponivel> implements HorarioDisponivelService{

	private HorarioDisponivelRepository horarioDisponivelRepository;

	@Autowired
	public HorarioDisponivelServiceImpl(Validador validador, HorarioDisponivelRepository horarioDisponivelRepository) {
		super(validador);
		this.horarioDisponivelRepository = horarioDisponivelRepository;
	}

	@Override
	protected Repository<HorarioDisponivel> getRepository() {
		return this.horarioDisponivelRepository;
	}

}
