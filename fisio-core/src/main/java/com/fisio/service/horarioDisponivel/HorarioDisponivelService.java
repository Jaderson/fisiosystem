package com.fisio.service.horarioDisponivel;

import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.service.Service;

public interface HorarioDisponivelService extends Service<HorarioDisponivel>{
}
