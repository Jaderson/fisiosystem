package com.fisio.service.formaPagamento.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.caixa.FluxoCaixa;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.fluxoCaixa.FluxoCaixaRepository;
import com.fisio.repository.formaPagamento.FormaPagamentoRepository;
import com.fisio.service.formaPagamento.FormaPagamentoService;

@Component
public class FormaPagamentoServiceImpl extends AbstractService<FormaPagamento> implements FormaPagamentoService{

	private FormaPagamentoRepository formaPagamentoRepository;
	private FluxoCaixaRepository fluxoCaixaRepository;

	@Autowired
	public FormaPagamentoServiceImpl(Validador validador, FormaPagamentoRepository formaPagamentoRepository,
			FluxoCaixaRepository fluxoCaixaRepository) {
		super(validador);
		this.formaPagamentoRepository = formaPagamentoRepository;
		this.fluxoCaixaRepository = fluxoCaixaRepository;
	}

	@Override
	protected Repository<FormaPagamento> getRepository() {
		return this.formaPagamentoRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(FormaPagamento entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioExcluir(FormaPagamento entidade) {
		verificaSeExisteFluxoCadastrado(entidade);
	}
	
	private void verificaSeExisteFluxoCadastrado(FormaPagamento entidade) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("formaPagamento.id", entidade.getId()));
		List<FluxoCaixa> listFluxo = this.fluxoCaixaRepository.consultarTodos(new FluxoCaixa(), listaRestricoes, null);
		if(VerificadorUtil.naoEstaNulo(listFluxo)){
			throw new ValidationException("impossivel_excluir_existe_fluxo_de_caixa_vinculado");
		}
	}

	@Override
	protected void regrasNegocioCadastrar(FormaPagamento entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(FormaPagamento entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
