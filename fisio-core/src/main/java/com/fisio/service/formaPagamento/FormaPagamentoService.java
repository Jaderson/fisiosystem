package com.fisio.service.formaPagamento;

import com.fisio.framework.formaPagamento.FormaPagamento;
import com.fisio.framework.service.Service;

public interface FormaPagamentoService extends Service<FormaPagamento>{
}
