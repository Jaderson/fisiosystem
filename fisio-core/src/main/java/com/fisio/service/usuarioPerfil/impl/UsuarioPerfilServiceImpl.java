package com.fisio.service.usuarioPerfil.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.repository.Repository;
import com.fisio.repository.usuarioPerfil.UsuarioPerfilRepository;
import com.fisio.service.usuarioPerfil.UsuarioPerfilService;

@Component
public class UsuarioPerfilServiceImpl extends AbstractService<UsuarioPerfil> implements UsuarioPerfilService{

	private UsuarioPerfilRepository usuarioPerfilRepository;

	@Autowired
	public UsuarioPerfilServiceImpl(Validador validador, UsuarioPerfilRepository usuarioPerfilRepository) {
		super(validador);
		this.usuarioPerfilRepository = usuarioPerfilRepository;
	}

	@Override
	protected Repository<UsuarioPerfil> getRepository() {
		return this.usuarioPerfilRepository;
	}

	@Override
	public List<UsuarioPerfil> consultarPerfilPorUsuario(Pessoa entidade) {
		return usuarioPerfilRepository.consultarPerfilPorUsuario(entidade);
	}

}
