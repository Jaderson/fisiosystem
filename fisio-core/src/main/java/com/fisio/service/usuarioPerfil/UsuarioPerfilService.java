package com.fisio.service.usuarioPerfil;

import java.util.List;

import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.pessoa.Pessoa;

public interface UsuarioPerfilService extends Service<UsuarioPerfil>{
	
	List<UsuarioPerfil> consultarPerfilPorUsuario(Pessoa entidade);
}
