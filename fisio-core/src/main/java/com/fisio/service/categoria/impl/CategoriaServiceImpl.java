package com.fisio.service.categoria.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.categoria.CategoriaRepository;
import com.fisio.service.categoria.CategoriaService;

@Component
public class CategoriaServiceImpl extends AbstractService<Categoria> implements CategoriaService{

	private CategoriaRepository categoriaRepository;

	@Autowired
	public CategoriaServiceImpl(Validador validador, CategoriaRepository categoriaRepository) {
		super(validador);
		this.categoriaRepository = categoriaRepository;
	}

	@Override
	protected Repository<Categoria> getRepository() {
		return this.categoriaRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Categoria entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Categoria entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Categoria entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
