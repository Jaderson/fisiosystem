package com.fisio.service.categoria;

import com.fisio.framework.service.Service;
import com.fisio.framework.tipoCategoria.Categoria;

public interface CategoriaService extends Service<Categoria>{
}
