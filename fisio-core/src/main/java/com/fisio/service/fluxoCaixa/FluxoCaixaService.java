package com.fisio.service.fluxoCaixa;

import java.math.BigDecimal;
import java.util.Date;

import com.fisio.framework.caixa.FluxoCaixa;
import com.fisio.framework.caixa.TipoLancamento;
import com.fisio.framework.service.Service;

public interface FluxoCaixaService extends Service<FluxoCaixa>{

	BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia,Date geraDataComUltimoDia, Integer idEmpresa, TipoLancamento tipoLancamento);
}
