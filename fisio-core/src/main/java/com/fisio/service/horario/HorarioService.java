package com.fisio.service.horario;

import java.util.List;

import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.service.Service;

public interface HorarioService extends Service<Horario>{

	List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade);
}
