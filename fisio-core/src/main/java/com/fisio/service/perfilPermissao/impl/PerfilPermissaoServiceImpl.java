package com.fisio.service.perfilPermissao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.repository.Repository;
import com.fisio.repository.perfilPermissao.PerfilPermissaoRepository;
import com.fisio.service.perfilPermissao.PerfilPermissaoService;

@Component
public class PerfilPermissaoServiceImpl extends AbstractService<PerfilPermissao> implements PerfilPermissaoService{

	private PerfilPermissaoRepository perfilPermissaoRepository;

	@Autowired
	public PerfilPermissaoServiceImpl(Validador validador, PerfilPermissaoRepository perfilPermissaoRepository) {
		super(validador);
		this.perfilPermissaoRepository = perfilPermissaoRepository;
	}

	@Override
	public PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(
			Perfil perfil, Permissao permissao) {
		return this.perfilPermissaoRepository.consultarPerfilPermissaoPorPerfilIhPermissao(perfil, permissao);
	}
	
	@Override
	protected Repository<PerfilPermissao> getRepository() {
		return this.perfilPermissaoRepository;
	}

}
