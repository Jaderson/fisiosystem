package com.fisio.service.perfilPermissao;

import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Permissao;

public interface PerfilPermissaoService extends Service<PerfilPermissao> {
	
	PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(Perfil perfil,	Permissao permissao);
}
