package com.fisio.service.acompanhamento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.acompanhamento.AcompanhamentoPessoa;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.acompanhamento.AcompanhamentoRepository;
import com.fisio.service.acompanhamento.AcompanhamentoService;

@Component
public class AcompanhamentoServiceImpl extends AbstractService<AcompanhamentoPessoa> implements AcompanhamentoService{

	private AcompanhamentoRepository acompanhamentoRepository;

	@Autowired
	public AcompanhamentoServiceImpl(Validador validador, AcompanhamentoRepository acompanhamentoRepository) {
		super(validador);
		this.acompanhamentoRepository = acompanhamentoRepository;
	}

	@Override
	protected Repository<AcompanhamentoPessoa> getRepository() {
		return this.acompanhamentoRepository;
	}
	
	@Override
	protected void regrasNegocioExcluir(AcompanhamentoPessoa entidade) {
		verificarSeExisteAcompanhamentoFilho(entidade);
	}

	private void verificarSeExisteAcompanhamentoFilho(AcompanhamentoPessoa entidade) {
		
	}

}
