package com.fisio.service.acompanhamento;

import com.fisio.framework.acompanhamento.AcompanhamentoPessoa;
import com.fisio.framework.service.Service;

public interface AcompanhamentoService extends Service<AcompanhamentoPessoa>{
}
