package com.fisio.service.usuario;

import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Usuario;

public interface UsuarioService extends Service<Usuario>{

	Usuario login(String username, String senha);

	void verificaSeUsuarioEstaLogado(Usuario entidade);
}
