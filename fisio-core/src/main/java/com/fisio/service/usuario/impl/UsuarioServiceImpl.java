package com.fisio.service.usuario.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.security.authentication.FisioSystem;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.framework.validador.Validador;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.repository.Repository;
import com.fisio.repository.agenda.AgendaRepository;
import com.fisio.repository.usuario.UsuarioRepository;
import com.fisio.repository.usuarioPerfil.UsuarioPerfilRepository;
import com.fisio.service.usuario.UsuarioService;

@Component
public class UsuarioServiceImpl extends AbstractService<Usuario> implements UsuarioService{

private static final String PREFIXO_PERMISSAO = "ROLE_";
	
	private UsuarioRepository usuarioRepository;
	private UsuarioPerfilRepository usuarioPerfilRepository;
	private Md5PasswordEncoder passwordEncoder;
	private AgendaRepository agendaRepository;

	@Autowired
	public UsuarioServiceImpl(Validador validador, UsuarioRepository usuarioRepository, Md5PasswordEncoder passwordEncoder,
			UsuarioPerfilRepository usuarioPerfilRepository, AgendaRepository agendaRepository) {
		super(validador);
		this.usuarioRepository = usuarioRepository;
		this.passwordEncoder = passwordEncoder;
		this.usuarioPerfilRepository = usuarioPerfilRepository;
		this.agendaRepository = agendaRepository;
	}

	@Override
	protected Repository<Usuario> getRepository() {
		return this.usuarioRepository;
	}
	
	@Override
	protected void regrasNegocioCadastrar(Usuario entidade) {
		verificarCamposObrigatorios(entidade);
		verificarSeExisteUsuarioComMesmaLoginParaMesmaEmpresa(entidade);
		salvarPerfisDoUsuario(entidade);
	}
	
	@Override
	protected void regrasNegocioExcluir(Usuario entidade) {
		verificaSeUsuarioEstaLogado(entidade);
		excluirUsuarioPerfil(entidade);
		verificarSeUsuarioTemConsulta(entidade);
	}

	@Override
	protected void regrasNegocioAlterar(Usuario entidade) {
		if(!entidade.isAlterarSenha()){
			verificarCamposObrigatorios(entidade);
			verificarSeExisteUsuarioComMesmaLoginParaMesmaEmpresa(entidade);
			excluirPerfisAntigos(entidade);
			salvarPerfisDoUsuario(entidade);
		}
	}
	
	public void verificarCamposObrigatorios(Usuario entidade) {
		
		StringBuffer msg = new StringBuffer();
		msg.append(verificarSeCampoEstaNulo(entidade.getLogin(), "erro_login_deve_ser_preenchido;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getPessoa(), "erro_pessoa_nao_pode_ser_nula;"));

		if (msg.length() > 0) {
			throw new ValidationException(msg.toString());
		}
	}
	
	private void verificarSeUsuarioTemConsulta(Usuario entidade) {
		List<Restricoes> listaRestricoes =  new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("funcionario.id", entidade.getId()));
		List<Agenda> listUsuarioAgenda = this.agendaRepository.consultarTodos(new Agenda(), listaRestricoes, null);
		if(listUsuarioAgenda.size() > 0){
			throw new ValidationException("impossivel_excluir_usuario_existem_consultas_marcadas");
		}
	}
	
	public void verificaSeUsuarioEstaLogado(Usuario entidade) throws ValidationException {
		UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
		FisioSystem userFisio = (FisioSystem) authentication.getPrincipal();
		if(entidade.getId() == userFisio.getUsuario().getId()) {
			throw new ValidationException("erro_voce_esta_sendo_usando");
		}
	}
	
	private void excluirUsuarioPerfil(Usuario entidade) {
		List<Restricoes> listaRestricoes =  new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("usuario.id", entidade.getId()));
		List<UsuarioPerfil> listUsuarioPerfil = this.usuarioPerfilRepository.consultarTodos(new UsuarioPerfil(), listaRestricoes, null);
		for (UsuarioPerfil usuarioPerfil : listUsuarioPerfil) {
			this.usuarioPerfilRepository.excluir(usuarioPerfil);
		}
	}

	@Override
	public Usuario login(String username, String senha) {
		String senhaEncriptada = this.passwordEncoder.encodePassword(senha, null);
		Usuario usuario = this.usuarioRepository.consultarUsuarioPorLogin(username);
		usuario.setAuthorities(getAuthorities(usuario));
		if(usuarioForNuloOuSenhaNaoForIgual(senhaEncriptada, usuario)) {
			throw new RuntimeException("erro_login_ou_senha_nao_coincide");
		}
		
		return usuario;
	}

	private void salvarPerfisDoUsuario(Usuario entidade) {
		for(UsuarioPerfil usuarioPerfil : entidade.getPerfils()) {
			this.usuarioPerfilRepository.cadastrar(usuarioPerfil);
		}
	}

	private void excluirPerfisAntigos(Usuario entidade) {
		Usuario usuarioConsultado = this.usuarioRepository.consultarPorId(entidade);
		for(UsuarioPerfil usuarioPerfil : usuarioConsultado.getPerfils()) {
			this.usuarioPerfilRepository.excluir(usuarioPerfil);
		}
	}

	private List<String> getAuthorities(Usuario usuario) {
		List<String> listaPermissoes = new ArrayList<String>();
		for (UsuarioPerfil usuarioPerfil : usuario.getPerfils()) {
			for (PerfilPermissao permissao : usuarioPerfil.getPerfil().getPermissoes()) {
				listaPermissoes.add(PREFIXO_PERMISSAO + permissao.getPermissao().getDescricao().toUpperCase());
			}
		}

		return listaPermissoes;
	}

	private boolean usuarioForNuloOuSenhaNaoForIgual(String senhaEncriptada, Usuario usuario) {
		return VerificadorUtil.estaNulo(usuario) || !usuario.getSenha().equals(senhaEncriptada);
	}

	public void encriptarSenha(Usuario entidade) {
		String senhaEncriptada = this.passwordEncoder.encodePassword(entidade.getSenha(), null);
		entidade.setSenha(senhaEncriptada);
		entidade.setTokenAcesso(senhaEncriptada);
	}
	
	private void verificarSeExisteUsuarioComMesmaLoginParaMesmaEmpresa(Usuario entidade) {
		List<Restricoes> listaRestricoes = criarRestricoesPorEmpresaIhLogin(entidade);
		Usuario usuario = this.usuarioRepository.consultarEntidade(entidade, listaRestricoes);
		if(VerificadorUtil.naoEstaNulo(usuario)) {
			throw new ValidationException("erro_ja_existe_um_usuario_com_este_login");
		}
	}

	private List<Restricoes> criarRestricoesPorEmpresaIhLogin(Usuario entidade) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("login", entidade.getLogin()));
		listaRestricoes.add(Restricoes.diferenteDe("id", entidade.getId()));
		return listaRestricoes;
	}

	public static void main(String[] args) {
		String senhaEncriptada = new Md5PasswordEncoder().encodePassword("123456", null);
		System.out.println(senhaEncriptada);
	}
}
