package com.fisio.service.medico;

import com.fisio.framework.medico.Medico;
import com.fisio.framework.service.Service;

public interface MedicoService extends Service<Medico>{
}
