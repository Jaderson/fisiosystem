package com.fisio.service.medico.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.medico.Medico;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.medico.MedicoRepository;
import com.fisio.service.medico.MedicoService;

@Component
public class MedicoServiceImpl extends AbstractService<Medico> implements MedicoService{

	private MedicoRepository medicoRepository;

	@Autowired
	public MedicoServiceImpl(Validador validador, MedicoRepository medicoRepository) {
		super(validador);
		this.medicoRepository = medicoRepository;
	}

	@Override
	protected Repository<Medico> getRepository() {
		return this.medicoRepository;
	}

}
