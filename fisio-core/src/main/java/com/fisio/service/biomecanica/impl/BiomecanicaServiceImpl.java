package com.fisio.service.biomecanica.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.avaliacoes.Biomecanica;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.biomecanica.BiomecanicaRepository;
import com.fisio.service.biomecanica.BiomecanicaService;

@Component
public class BiomecanicaServiceImpl extends AbstractService<Biomecanica> implements BiomecanicaService{

	private BiomecanicaRepository biomecanicaRepository;

	@Autowired
	public BiomecanicaServiceImpl(Validador validador, BiomecanicaRepository biomecanicaRepository) {
		super(validador);
		this.biomecanicaRepository = biomecanicaRepository;
	}

	@Override
	protected Repository<Biomecanica> getRepository() {
		return this.biomecanicaRepository;
	}

}
