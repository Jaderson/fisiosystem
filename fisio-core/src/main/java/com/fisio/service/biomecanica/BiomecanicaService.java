package com.fisio.service.biomecanica;

import com.fisio.framework.avaliacoes.Biomecanica;
import com.fisio.framework.service.Service;

public interface BiomecanicaService extends Service<Biomecanica>{
}
