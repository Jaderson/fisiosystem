package com.fisio.service.fisioterapia;

import com.fisio.framework.avaliacoes.Fisioterapia;
import com.fisio.framework.service.Service;

public interface FisioterapiaService extends Service<Fisioterapia>{
}
