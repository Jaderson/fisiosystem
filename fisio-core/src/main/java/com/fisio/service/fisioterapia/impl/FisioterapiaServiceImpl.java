package com.fisio.service.fisioterapia.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.avaliacoes.Fisioterapia;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.fisioterapia.FisioterapiaRepository;
import com.fisio.service.fisioterapia.FisioterapiaService;

@Component
public class FisioterapiaServiceImpl extends AbstractService<Fisioterapia> implements FisioterapiaService{

	private FisioterapiaRepository fisioterapiaRepository;

	@Autowired
	public FisioterapiaServiceImpl(Validador validador, FisioterapiaRepository fisioterapiaRepository) {
		super(validador);
		this.fisioterapiaRepository = fisioterapiaRepository;
	}

	@Override
	protected Repository<Fisioterapia> getRepository() {
		return this.fisioterapiaRepository;
	}

}
