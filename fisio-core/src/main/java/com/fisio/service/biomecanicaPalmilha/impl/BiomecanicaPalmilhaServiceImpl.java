package com.fisio.service.biomecanicaPalmilha.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.avaliacoes.BiomecanicaPalmilha;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.biomecanicaPalmilha.BiomecanicaPalmilhaRepository;
import com.fisio.service.biomecanicaPalmilha.BiomecanicaPalmilhaService;

@Component
public class BiomecanicaPalmilhaServiceImpl extends AbstractService<BiomecanicaPalmilha> implements BiomecanicaPalmilhaService{

	private BiomecanicaPalmilhaRepository biomecanicaPalmilhaRepository;

	@Autowired
	public BiomecanicaPalmilhaServiceImpl(Validador validador, BiomecanicaPalmilhaRepository biomecanicaPalmilhaRepository) {
		super(validador);
		this.biomecanicaPalmilhaRepository = biomecanicaPalmilhaRepository;
	}

	@Override
	protected Repository<BiomecanicaPalmilha> getRepository() {
		return this.biomecanicaPalmilhaRepository;
	}

}
