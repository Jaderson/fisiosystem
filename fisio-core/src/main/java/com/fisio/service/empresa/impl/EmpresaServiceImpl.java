package com.fisio.service.empresa.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.model.empresa.Empresa;
import com.fisio.repository.Repository;
import com.fisio.repository.empresa.EmpresaRepository;
import com.fisio.service.empresa.EmpresaService;

@Component
public class EmpresaServiceImpl extends AbstractService<Empresa> implements EmpresaService{

	private EmpresaRepository empresaRepository;

	@Autowired
	public EmpresaServiceImpl(Validador validador, EmpresaRepository empresaRepository) {
		super(validador);
		this.empresaRepository = empresaRepository;
	}

	@Override
	protected Repository<Empresa> getRepository() {
		return this.empresaRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Empresa entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Empresa entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Empresa entidade) {
		
		StringBuffer msg = new StringBuffer();
		msg.append(verificarSeCampoEstaNulo(entidade.getNomeFantasia(), "erro_nome_fantasia_nao_pode_ser_nulo;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getEndereco().getCep(), "erro_cep_nao_pode_ser_nulo;"));
		msg.append(verificarSeCampoEstaNulo(entidade.getEndereco().getEndereco(), "erro_endereco_nao_pode_ser_nulo;"));

		if (msg.length() > 0) {
			throw new ValidationException(msg.toString());
		}
	}

}
