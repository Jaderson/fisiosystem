package com.fisio.service.empresa;

import com.fisio.framework.service.Service;
import com.fisio.model.empresa.Empresa;

public interface EmpresaService extends Service<Empresa>{
}
