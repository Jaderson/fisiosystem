package com.fisio.service.palmilha.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.avaliacoes.Palmilha;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.palmilha.PalmilhaRepository;
import com.fisio.service.palmilha.PalmilhaService;

@Component
public class PalmilhaServiceImpl extends AbstractService<Palmilha> implements PalmilhaService{

	private PalmilhaRepository palmilhaRepository;

	@Autowired
	public PalmilhaServiceImpl(Validador validador, PalmilhaRepository palmilhaRepository) {
		super(validador);
		this.palmilhaRepository = palmilhaRepository;
	}

	@Override
	protected Repository<Palmilha> getRepository() {
		return this.palmilhaRepository;
	}

}
