package com.fisio.service.perfil.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.framework.validador.Validador;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.repository.Repository;
import com.fisio.repository.perfil.PerfilRepository;
import com.fisio.repository.perfilPermissao.PerfilPermissaoRepository;
import com.fisio.repository.usuarioPerfil.UsuarioPerfilRepository;
import com.fisio.service.perfil.PerfilService;

@Component
public class PerfilServiceImpl extends AbstractService<Perfil> implements PerfilService{

	private PerfilRepository perfilRepository;
	private PerfilPermissaoRepository perfilPermissaoRepository;
	private UsuarioPerfilRepository usuarioPerfilRepository;

	@Autowired
	public PerfilServiceImpl(Validador validador, PerfilRepository perfilRepository, 
			PerfilPermissaoRepository perfilPermissaoRepository, 
			UsuarioPerfilRepository usuarioPerfilRepository) {
		super(validador);
		this.perfilRepository = perfilRepository;
		this.perfilPermissaoRepository = perfilPermissaoRepository;
		this.usuarioPerfilRepository = usuarioPerfilRepository;
		
	}

	@Override
	protected Repository<Perfil> getRepository() {
		return this.perfilRepository;
	}

	@Override
	public void cadastrar(Perfil entidade) {
		super.cadastrar(entidade);
		salvarPermissoesNovas(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Perfil entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioAlterar(Perfil entidade) {
		verificarCamposObrigatorios(entidade);
		excluirPermissoes(entidade);
		salvarPermissoesNovas(entidade);
	}
	
	@Override
	protected void regrasNegocioExcluir(Perfil entidade) {
		lancarExcecaoSeExistiremUsuariosUtilizandoOhPerfil(entidade);
		excluirPermissoes(entidade);
	}

	private void lancarExcecaoSeExistiremUsuariosUtilizandoOhPerfil(Perfil entidade) {
		List<Restricoes> listaRestricoes = new ArrayList<Restricoes>();
		listaRestricoes.add(Restricoes.igual("perfil.id", entidade.getId()));		
		List<UsuarioPerfil> listaUsuarioPerfil = this.usuarioPerfilRepository.consultarTodos(new UsuarioPerfil(), 
				listaRestricoes, null);
		
		if (!listaUsuarioPerfil.isEmpty()) {
			throw new RuntimeException("existem_usuarios_que_utilizam_este_perfil");
		}
	}
	
	private void salvarPermissoesNovas(Perfil entidade) {
		for (PerfilPermissao perfilPermissao : entidade.getPermissoes()) {
			this.perfilPermissaoRepository.cadastrar(perfilPermissao);
		}
	}

	private void excluirPermissoes(Perfil entidade) {
		Perfil perfilConsultado = getRepository().consultarPorId(entidade);
		for(PerfilPermissao perfilPermissao : perfilConsultado.getPermissoes()) {
			perfilPermissao = this.perfilPermissaoRepository.consultarPorId(perfilPermissao);
			this.perfilPermissaoRepository.excluir(perfilPermissao);
		}
	}
	
	public void verificarCamposObrigatorios(Perfil entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
