package com.fisio.service.perfil;

import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Perfil;

public interface PerfilService extends Service<Perfil>{
}
