package com.fisio.service.objeto;

import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Objeto;

public interface ObjetoService extends Service<Objeto>{
}
