package com.fisio.service.configuracao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.configuracao.Configuracao;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.configuracao.ConfiguracaoRepository;
import com.fisio.service.configuracao.ConfiguracaoService;

@Component
public class ConfiguracaoServiceImpl extends AbstractService<Configuracao> implements ConfiguracaoService{

	private ConfiguracaoRepository configuracaoRepository;

	@Autowired
	public ConfiguracaoServiceImpl(Validador validador, ConfiguracaoRepository configuracaoRepository) {
		super(validador);
		this.configuracaoRepository = configuracaoRepository;
	}

	@Override
	protected Repository<Configuracao> getRepository() {
		return this.configuracaoRepository;
	}

	@Override
	public List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int idSemana, Integer idEmpresa) {
		return this.configuracaoRepository.consultarPessoaPorConfiguracao(isPesquisa, idSemana, idEmpresa);
	}

}
