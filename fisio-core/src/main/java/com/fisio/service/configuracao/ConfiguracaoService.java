package com.fisio.service.configuracao;

import java.util.List;

import com.fisio.framework.configuracao.Configuracao;
import com.fisio.framework.service.Service;

public interface ConfiguracaoService extends Service<Configuracao>{

	List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int idSemana, Integer idEmpresa);

}
