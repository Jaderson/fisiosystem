package com.fisio.service.banco.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.banco.Banco;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.banco.BancoRepository;
import com.fisio.service.banco.BancoService;

@Component
public class BancoServiceImpl extends AbstractService<Banco> implements BancoService{

	private BancoRepository bancoRepository;

	@Autowired
	public BancoServiceImpl(Validador validador, BancoRepository bancoRepository) {
		super(validador);
		this.bancoRepository = bancoRepository;
	}

	@Override
	protected Repository<Banco> getRepository() {
		return this.bancoRepository;
	}
	
	@Override
	protected void regrasNegocioAlterar(Banco entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioCadastrar(Banco entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Banco entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_nome_não_pode_ser_nulo").toString());
		}
	}
}
