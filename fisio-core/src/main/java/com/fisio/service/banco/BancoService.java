package com.fisio.service.banco;

import com.fisio.framework.banco.Banco;
import com.fisio.framework.service.Service;

public interface BancoService extends Service<Banco>{
}
