package com.fisio.service.especialidade;

import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.service.Service;

public interface EspecialidadeService extends Service<Especialidade>{
}
