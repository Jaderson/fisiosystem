package com.fisio.service.especialidade.impl;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.agenda.AgendaRepository;
import com.fisio.repository.especialidade.EspecialidadeRepository;
import com.fisio.service.especialidade.EspecialidadeService;

@Component
public class EspecialidadeServiceImpl extends AbstractService<Especialidade> implements EspecialidadeService{

	private EspecialidadeRepository especialidadeRepository;
	private AgendaRepository agendaRepository;

	@Autowired
	public EspecialidadeServiceImpl(Validador validador, EspecialidadeRepository especialidadeRepository,
			AgendaRepository agendaRepository) {
		super(validador);
		this.especialidadeRepository = especialidadeRepository;
		this.agendaRepository = agendaRepository;
	}

	@Override
	protected Repository<Especialidade> getRepository() {
		return this.especialidadeRepository;
	}
	
	@Override
	protected void regrasNegocioExcluir(Especialidade entidade) {
		verificaAgendaSeExisteEspecialidade(entidade);
	}
	
	private void verificaAgendaSeExisteEspecialidade(Especialidade entidade) {
		this.agendaRepository.consultarSeExisteEspecialidade(entidade);
	}

	@Override
	protected void regrasNegocioCadastrar(Especialidade entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	@Override
	protected void regrasNegocioAlterar(Especialidade entidade) {
		verificarCamposObrigatorios(entidade);
	}
	
	public void verificarCamposObrigatorios(Especialidade entidade) {
		if(VerificadorUtil.estaNuloOuVazio(entidade.getDescricao())){
			throw new ValidationException(verificarSeCampoEstaNulo(entidade.getDescricao(), "erro_descricao_nao_pode_ser_nula").toString());
		}
	}

}
