package com.fisio.service.pessoa;

import java.util.List;

import com.fisio.framework.service.Service;
import com.fisio.model.pessoa.Pessoa;

public interface PessoaService extends Service<Pessoa>{

	List<Pessoa> consultarAniversariante();
}
