package com.fisio.service.contasPagar;

import com.fisio.framework.contasPagar.ContasPagar;
import com.fisio.framework.service.Service;

public interface ContasPagarService extends Service<ContasPagar>{

	void verificarCamposObrigatorios(ContasPagar entidade);

	void excluirContasParceladas(ContasPagar entidade);
}
