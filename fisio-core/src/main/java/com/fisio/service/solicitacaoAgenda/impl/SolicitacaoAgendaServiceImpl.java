package com.fisio.service.solicitacaoAgenda.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.solicitacaoAgenda.SolicitacaoAgendaRepository;
import com.fisio.service.solicitacaoAgenda.SolicitacaoAgendaService;

@Component
public class SolicitacaoAgendaServiceImpl extends AbstractService<SolicitacaoAgenda> implements SolicitacaoAgendaService{

	private SolicitacaoAgendaRepository solicitacaoAgendaRepository;

	@Autowired
	public SolicitacaoAgendaServiceImpl(Validador validador, SolicitacaoAgendaRepository solicitacaoAgendaRepository) {
		super(validador);
		this.solicitacaoAgendaRepository = solicitacaoAgendaRepository;
	}

	@Override
	protected Repository<SolicitacaoAgenda> getRepository() {
		return this.solicitacaoAgendaRepository;
	}

}
