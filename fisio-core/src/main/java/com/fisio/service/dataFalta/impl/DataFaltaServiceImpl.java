package com.fisio.service.dataFalta.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.agenda.DataFalta;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.dataFalta.DataFaltaRepository;
import com.fisio.service.dataFalta.DataFaltaService;

@Component
public class DataFaltaServiceImpl extends AbstractService<DataFalta> implements DataFaltaService{

	private DataFaltaRepository dataFaltaRepository;

	@Autowired
	public DataFaltaServiceImpl(Validador validador, DataFaltaRepository dataFaltaRepository) {
		super(validador);
		this.dataFaltaRepository = dataFaltaRepository;
	}

	@Override
	protected Repository<DataFalta> getRepository() {
		return this.dataFaltaRepository;
	}

}
