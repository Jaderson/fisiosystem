package com.fisio.service.acompanhamentoFilho.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.framework.service.impl.AbstractService;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;
import com.fisio.repository.acompanhamentoFilho.AcompanhamentoFilhoRepository;
import com.fisio.service.acompanhamentoFilho.AcompanhamentoFilhoService;

@Component
public class AcompanhamentoFilhoServiceImpl extends AbstractService<AcompanhamentoFilho> implements AcompanhamentoFilhoService{

	private AcompanhamentoFilhoRepository acompanhamentoFilhoRepository;

	@Autowired
	public AcompanhamentoFilhoServiceImpl(Validador validador, AcompanhamentoFilhoRepository acompanhamentoFilhoRepository) {
		super(validador);
		this.acompanhamentoFilhoRepository = acompanhamentoFilhoRepository;
	}

	@Override
	protected Repository<AcompanhamentoFilho> getRepository() {
		return this.acompanhamentoFilhoRepository;
	}

}
