package com.fisio.framework.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.fisio.framework.consulta.restricao.Restricoes;
import com.fisio.framework.contexto.Alterar;
import com.fisio.framework.contexto.Cadastrar;
import com.fisio.framework.service.Service;
import com.fisio.framework.validador.Validador;
import com.fisio.repository.Repository;

public abstract class AbstractService<E> implements Service<E> {

	private Validador validador;

	protected abstract Repository<E> getRepository();

	public AbstractService(Validador validador) {
		this.validador = validador;
	}

	@Override
	public void cadastrar(E entidade) {
		validarEntidadeNoContextoInformado(entidade, Cadastrar.class);
		regrasNegocioCadastrar(entidade);
		getRepository().cadastrar(entidade);
	}

	@Override
	public void alterar(E entidade) {
		validarEntidadeNoContextoInformado(entidade, Alterar.class);
		regrasNegocioAlterar(entidade);
		getRepository().alterar(entidade);
	}

	@Override
	public void excluir(E entidade) {
		regrasNegocioExcluir(entidade);
		excluirEntidadeOuLancarExcecaoCasoPossuaAssociacao(entidade);
	}

	@Override
	public E consultarPorId(E entidade) {
		return getRepository().consultarPorId(entidade);
	}

	@Override
	public E consultarPorId(Integer id) {
		return getRepository().consultarPorId(id);
	}

	@Override
	public E consultarEntidade(E entidade, List<Restricoes> listaRestricoes) {
		return getRepository().consultarEntidade(entidade, listaRestricoes);
	}
	
	@Override
	public List<E> consultarTodos(E entidade) {
		return getRepository().consultarTodos(entidade);
	}
	
	@Override
	public List<E> consultarTodosDesc(E entidade, List<Restricoes> listaRestricoes, String campoOrdenacao) {
		return getRepository().consultarTodosDesc(entidade, listaRestricoes, campoOrdenacao);
	}
	
	@Override
	public List<E> consultarTodos(E entidade, List<Restricoes> listaRestricoes, String campoOrdenacao) {
		return getRepository().consultarTodos(entidade, listaRestricoes, campoOrdenacao);
	}
	
	public List<E> consultarTodosPaginado(E entidade, int first, int pageSize, List<Restricoes> listaRestricoes, String campoOrdenacao) {
		return getRepository().consultarTodosPaginado(entidade, first, pageSize, listaRestricoes, campoOrdenacao);
	}
	
	@Override
	public Integer consultarQuantidadeRegistros(E entidade, List<Restricoes> listaRestricoes, String campoOrdenacao) {
		return getRepository().consultarQuantidadeRegistros(entidade, listaRestricoes, campoOrdenacao);
	}
	
	@Override
	public List<E> filtro(E entidade) {
		return getRepository().filtro(entidade);
	}
	
	@Override
	public List<E> filtro(List<Restricoes> listaRestricoes, E entidade,String campoOrdenacao) {
		return getRepository().filtro(listaRestricoes, entidade,campoOrdenacao);
	}
	
	protected void validarEntidadeNoContextoInformado(E entidade, Class<?> contexto) {
		validador.validar(entidade, contexto);
	}

	protected void excluirEntidadeOuLancarExcecaoCasoPossuaAssociacao(E entidade) {
		lancarExcecaoCasoEntidadePossuaAssociacao(entidade);
		getRepository().excluir(entidade);
	}
	
	public StringBuffer verificarSeCampoEstaNulo(Object entidade,String mensagem) {
		StringBuffer msg = new StringBuffer();

		if (entidade instanceof BigDecimal) {
			if (entidade.equals(BigDecimal.ZERO)) {
				msg.append(mensagem);
			}
		}
		if (entidade == null || entidade.equals("")) {
			msg.append(mensagem);
		}
		return msg;
	}
	

	protected void lancarExcecaoCasoEntidadePossuaAssociacao(E entidade) {}

	protected void regrasNegocioCadastrar(E entidade){}
	protected void regrasNegocioAlterar(E entidade){}
	protected void regrasNegocioExcluir(E entidade){}

}