package com.fisio.framework.security.authentication;

import org.springframework.security.core.GrantedAuthority;

public class FisioSystemGrantedAuthority implements GrantedAuthority{

	private static final long serialVersionUID = 1L;

	private String authority;
	
	public FisioSystemGrantedAuthority(String authority) {
		this.authority = authority;
	}
	
	@Override
	public String getAuthority() {
		return this.authority;
	}

}
