package com.fisio.framework.security.provider;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.fisio.framework.security.authentication.FisioSystem;
import com.fisio.framework.security.exception.FisioSystemAuthenticationException;
import com.fisio.framework.security.service.FisioAuthenticationService;
import com.fisio.framework.util.VerificadorUtil;

public class FisioAuthenticationProvider implements AuthenticationProvider{
	
	private static final String MENSAGEM_USUARIO_NAO_ENCONTRADO_OU_USUARIO_NAO_POSSUI_NENHUMA_PERMISSAO_ASSOCIADA = "Usuário não encontrado ou usuário não possui nenhuma permissão associada";
	private static final String MENSAGEM_NAO_FOI_POSSIVEL_EFETUAR_LOGIN_DO_USUARIO_SENHA_NAO_CONFERE = "Não foi possível efetuar o login do usuário. A senha não confere";
	
	private FisioAuthenticationService authenticationService;
	private PasswordEncoder passwordEncoder;
	
	@Override
	public Authentication authenticate(Authentication authentication)	throws AuthenticationException {
		UsernamePasswordAuthenticationToken thramosAuthentication = (UsernamePasswordAuthenticationToken) authentication;
		FisioSystem usuarioRetornado = (FisioSystem) authenticationService.loadUserByUsername(authentication.getName());
		String senha = passwordEncoder.encodePassword(thramosAuthentication.getCredentials().toString(), null);
		
		lancarExcecaoSeUsuarioForNulo(usuarioRetornado);
		verificaSeSenhasSaoEquivalentesLancandoExcecaoSeNaoForem(usuarioRetornado.getPassword(), senha);
		return new UsernamePasswordAuthenticationToken(usuarioRetornado, usuarioRetornado.getPassword(), usuarioRetornado.getAuthorities());
		
	}

	private void lancarExcecaoSeUsuarioForNulo(FisioSystem usuarioRetornado) {
		if(VerificadorUtil.estaNulo(usuarioRetornado)) {
			throw new FisioSystemAuthenticationException(MENSAGEM_USUARIO_NAO_ENCONTRADO_OU_USUARIO_NAO_POSSUI_NENHUMA_PERMISSAO_ASSOCIADA);
		}
	}

	@Override
	public boolean supports(Class<?> classe) {
		return true;
	}

	public void setAuthenticationService(FisioAuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}
	
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
	
	private void verificaSeSenhasSaoEquivalentesLancandoExcecaoSeNaoForem(String senhaDoUsuarioCadastrado, String senhaPassadaNoFormulario) {
		if(senhasNaoSaoIguais(senhaDoUsuarioCadastrado, senhaPassadaNoFormulario)) {
			throw new FisioSystemAuthenticationException(MENSAGEM_NAO_FOI_POSSIVEL_EFETUAR_LOGIN_DO_USUARIO_SENHA_NAO_CONFERE);
		}
	}

	private boolean senhasNaoSaoIguais(String senhaDoUsuarioCadastrado,
			String senhaPassadaNoFormulario) {
		return !StringUtils.equals(senhaDoUsuarioCadastrado, senhaPassadaNoFormulario);
	}
	
}
