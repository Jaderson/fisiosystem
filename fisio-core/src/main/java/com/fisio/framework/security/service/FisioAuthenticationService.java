
package com.fisio.framework.security.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.fisio.framework.security.authentication.FisioSystem;
import com.fisio.framework.security.authentication.FisioSystemGrantedAuthority;
import com.fisio.framework.util.VerificadorUtil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.repository.usuario.UsuarioRepository;

public class FisioAuthenticationService implements UserDetailsService{

	private static final String PREFIXO_PERMISSAO = "ROLE_";


	@Autowired UsuarioRepository usuarioRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.consultarUsuarioPorLogin(username);
		if(usuarioExiste(usuario)) {
			Collection<GrantedAuthority> authorities = criarAuthoritiesIhSetarNoUusario(usuario);
			return new FisioSystem(usuario, usuario.getLogin(), usuario.getSenha(), authorities);
		}
		
		return null;
	}


	private Collection<GrantedAuthority> criarAuthoritiesIhSetarNoUusario(Usuario usuario) {
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (UsuarioPerfil usuarioPerfil : usuario.getPerfils()) {
			for (PerfilPermissao permissao : usuarioPerfil.getPerfil().getPermissoes()) {
				authorities.add(new FisioSystemGrantedAuthority(PREFIXO_PERMISSAO + permissao.getPermissao().getDescricaoSistema().toUpperCase()));
			}
		}
		
		return authorities;
	}

	private boolean usuarioExiste(Usuario usuarioLogado) {
		if(VerificadorUtil.estaNulo(usuarioLogado)) {
			return false;
		}
		
		return true;
	}
}
