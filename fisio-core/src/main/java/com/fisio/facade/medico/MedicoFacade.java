package com.fisio.facade.medico;

import com.fisio.framework.facade.Facade;
import com.fisio.framework.medico.Medico;

public interface MedicoFacade extends Facade<Medico>{

}
