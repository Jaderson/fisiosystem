package com.fisio.facade.medico.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.medico.MedicoFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.medico.Medico;
import com.fisio.framework.service.Service;
import com.fisio.service.medico.MedicoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class MedicoFacadeImpl extends AbstractFacade<Medico> implements MedicoFacade{

	private MedicoService medicoService;

	@Autowired
	public MedicoFacadeImpl(MedicoService medicoService) {
		this.medicoService = medicoService;
	}
	
	@Override
	protected Service<Medico> getService() {
		return this.medicoService;
	}

}
