package com.fisio.facade.horario;

import java.util.List;

import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.facade.Facade;

public interface HorarioFacade extends Facade<Horario>{

	List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade);

}
