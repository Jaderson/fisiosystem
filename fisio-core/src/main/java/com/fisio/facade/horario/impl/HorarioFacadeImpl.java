package com.fisio.facade.horario.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.horario.HorarioFacade;
import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.agenda.Horario;
import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.horario.HorarioService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class HorarioFacadeImpl extends AbstractFacade<Horario> implements HorarioFacade{

	private HorarioService horarioService;

	@Autowired
	public HorarioFacadeImpl(HorarioService horarioService) {
		this.horarioService = horarioService;
	}
	
	@Override
	protected Service<Horario> getService() {
		return this.horarioService;
	}

	@Override
	public List<HorarioDisponivel> consultarHorarioPorConfiguracao(Agenda entidade) {
		return this.horarioService.consultarHorarioPorConfiguracao(entidade);
	}

}
