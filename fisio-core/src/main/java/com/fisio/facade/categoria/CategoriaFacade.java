package com.fisio.facade.categoria;

import com.fisio.framework.facade.Facade;
import com.fisio.framework.tipoCategoria.Categoria;

public interface CategoriaFacade extends Facade<Categoria>{

}
