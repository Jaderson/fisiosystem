package com.fisio.facade.categoria.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.categoria.CategoriaFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.framework.tipoCategoria.Categoria;
import com.fisio.service.categoria.CategoriaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class CategoriaFacadeImpl extends AbstractFacade<Categoria> implements CategoriaFacade{

	private CategoriaService categoriaService;

	@Autowired
	public CategoriaFacadeImpl(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@Override
	protected Service<Categoria> getService() {
		return this.categoriaService;
	}

}
