package com.fisio.facade.acompanhamento;

import com.fisio.framework.acompanhamento.AcompanhamentoPessoa;
import com.fisio.framework.facade.Facade;

public interface AcompanhamentoFacade extends Facade<AcompanhamentoPessoa>{

}
