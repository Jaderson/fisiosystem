package com.fisio.facade.acompanhamento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.acompanhamento.AcompanhamentoFacade;
import com.fisio.framework.acompanhamento.AcompanhamentoPessoa;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.acompanhamento.AcompanhamentoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class AcompanhamentoFacadeImpl extends AbstractFacade<AcompanhamentoPessoa> implements AcompanhamentoFacade{

	private AcompanhamentoService acompanhamentoService;

	@Autowired
	public AcompanhamentoFacadeImpl(AcompanhamentoService acompanhamentoService) {
		this.acompanhamentoService = acompanhamentoService;
	}
	
	@Override
	protected Service<AcompanhamentoPessoa> getService() {
		return this.acompanhamentoService;
	}

}
