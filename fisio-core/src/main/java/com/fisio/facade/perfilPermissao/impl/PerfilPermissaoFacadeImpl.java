package com.fisio.facade.perfilPermissao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.perfilPermissao.PerfilPermissaoFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.service.perfilPermissao.PerfilPermissaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PerfilPermissaoFacadeImpl extends AbstractFacade<PerfilPermissao> implements PerfilPermissaoFacade{

	private PerfilPermissaoService perfilPermissaoService;

	@Autowired
	public PerfilPermissaoFacadeImpl(PerfilPermissaoService perfilPermissaoService) {
		this.perfilPermissaoService = perfilPermissaoService;
	}
	
	@Override
	public PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(
			Perfil perfil, Permissao permissao) {
		return this.perfilPermissaoService.consultarPerfilPermissaoPorPerfilIhPermissao(perfil, permissao);
	}
	
	@Override
	protected Service<PerfilPermissao> getService() {
		return this.perfilPermissaoService;
	}
}
