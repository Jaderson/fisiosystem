package com.fisio.facade.perfilPermissao;

import com.fisio.framework.facade.Facade;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.model.autenticacao.PerfilPermissao;
import com.fisio.model.autenticacao.Permissao;

public interface PerfilPermissaoFacade extends Facade<PerfilPermissao>{

	PerfilPermissao consultarPerfilPermissaoPorPerfilIhPermissao(
			Perfil perfil, Permissao permissao);
}
