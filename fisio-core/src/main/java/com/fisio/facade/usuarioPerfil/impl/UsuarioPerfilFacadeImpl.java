package com.fisio.facade.usuarioPerfil.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.usuarioPerfil.UsuarioPerfilFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.service.usuarioPerfil.UsuarioPerfilService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class UsuarioPerfilFacadeImpl extends AbstractFacade<UsuarioPerfil> implements UsuarioPerfilFacade{

	private UsuarioPerfilService usuarioPerfilService;

	@Autowired
	public UsuarioPerfilFacadeImpl(UsuarioPerfilService usuarioPerfilService) {
		this.usuarioPerfilService = usuarioPerfilService;
	}
	
	@Override
	protected Service<UsuarioPerfil> getService() {
		return this.usuarioPerfilService;
	}

	@Override
	public List<UsuarioPerfil> consultarPerfilPorUsuario(Pessoa entidade) {
		return usuarioPerfilService.consultarPerfilPorUsuario(entidade);
	}

}
