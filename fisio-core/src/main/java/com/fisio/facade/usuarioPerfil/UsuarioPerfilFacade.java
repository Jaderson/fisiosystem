package com.fisio.facade.usuarioPerfil;

import java.util.List;

import com.fisio.framework.facade.Facade;
import com.fisio.model.autenticacao.UsuarioPerfil;
import com.fisio.model.pessoa.Pessoa;

public interface UsuarioPerfilFacade extends Facade<UsuarioPerfil>{

	List<UsuarioPerfil> consultarPerfilPorUsuario(Pessoa entidade);
}
