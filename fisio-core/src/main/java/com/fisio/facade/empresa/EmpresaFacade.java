package com.fisio.facade.empresa;

import com.fisio.framework.facade.Facade;
import com.fisio.model.empresa.Empresa;

public interface EmpresaFacade extends Facade<Empresa>{

}
