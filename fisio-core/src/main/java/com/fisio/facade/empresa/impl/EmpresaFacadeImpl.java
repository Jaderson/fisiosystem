package com.fisio.facade.empresa.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.empresa.EmpresaFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.empresa.Empresa;
import com.fisio.service.empresa.EmpresaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class EmpresaFacadeImpl extends AbstractFacade<Empresa> implements EmpresaFacade{

	private EmpresaService empresaService;

	@Autowired
	public EmpresaFacadeImpl(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	protected Service<Empresa> getService() {
		return this.empresaService;
	}

}
