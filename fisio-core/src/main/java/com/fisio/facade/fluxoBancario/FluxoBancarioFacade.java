package com.fisio.facade.fluxoBancario;

import com.fisio.framework.facade.Facade;
import com.fisio.framework.fluxoBancario.FluxoBancario;

public interface FluxoBancarioFacade extends Facade<FluxoBancario>{

}
