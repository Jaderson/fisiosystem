package com.fisio.facade.fisioterapia.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.fisioterapia.FisioterapiaFacade;
import com.fisio.framework.avaliacoes.Fisioterapia;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.fisioterapia.FisioterapiaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class FisioterapiaFacadeImpl extends AbstractFacade<Fisioterapia> implements FisioterapiaFacade{

	private FisioterapiaService fisioterapiaService;

	@Autowired
	public FisioterapiaFacadeImpl(FisioterapiaService fisioterapiaService) {
		this.fisioterapiaService = fisioterapiaService;
	}
	
	@Override
	protected Service<Fisioterapia> getService() {
		return this.fisioterapiaService;
	}

}
