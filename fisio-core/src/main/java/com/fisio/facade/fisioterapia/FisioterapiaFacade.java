package com.fisio.facade.fisioterapia;

import com.fisio.framework.avaliacoes.Fisioterapia;
import com.fisio.framework.facade.Facade;

public interface FisioterapiaFacade extends Facade<Fisioterapia>{

}
