package com.fisio.facade.acompanhamentoFilho;

import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.framework.facade.Facade;

public interface AcompanhamentoFilhoFacade extends Facade<AcompanhamentoFilho>{

}
