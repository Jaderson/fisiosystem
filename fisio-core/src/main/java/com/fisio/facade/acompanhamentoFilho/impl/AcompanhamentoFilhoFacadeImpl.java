package com.fisio.facade.acompanhamentoFilho.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.acompanhamentoFilho.AcompanhamentoFilhoFacade;
import com.fisio.framework.acompanhamento.AcompanhamentoFilho;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.acompanhamentoFilho.AcompanhamentoFilhoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class AcompanhamentoFilhoFacadeImpl extends AbstractFacade<AcompanhamentoFilho> implements AcompanhamentoFilhoFacade{

	private AcompanhamentoFilhoService acompanhamentoFilhoService;

	@Autowired
	public AcompanhamentoFilhoFacadeImpl(AcompanhamentoFilhoService acompanhamentoFilhoService) {
		this.acompanhamentoFilhoService = acompanhamentoFilhoService;
	}
	
	@Override
	protected Service<AcompanhamentoFilho> getService() {
		return this.acompanhamentoFilhoService;
	}

}
