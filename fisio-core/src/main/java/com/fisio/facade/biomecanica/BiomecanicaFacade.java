package com.fisio.facade.biomecanica;

import com.fisio.framework.avaliacoes.Biomecanica;
import com.fisio.framework.facade.Facade;

public interface BiomecanicaFacade extends Facade<Biomecanica>{

}
