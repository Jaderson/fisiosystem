package com.fisio.facade.biomecanica.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.biomecanica.BiomecanicaFacade;
import com.fisio.framework.avaliacoes.Biomecanica;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.biomecanica.BiomecanicaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class BiomecanicaFacadeImpl extends AbstractFacade<Biomecanica> implements BiomecanicaFacade{

	private BiomecanicaService biomecanicaService;

	@Autowired
	public BiomecanicaFacadeImpl(BiomecanicaService biomecanicaService) {
		this.biomecanicaService = biomecanicaService;
	}
	
	@Override
	protected Service<Biomecanica> getService() {
		return this.biomecanicaService;
	}

}
