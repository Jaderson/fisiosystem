package com.fisio.facade.dataFalta;

import com.fisio.framework.agenda.DataFalta;
import com.fisio.framework.facade.Facade;

public interface DataFaltaFacade extends Facade<DataFalta>{
}
