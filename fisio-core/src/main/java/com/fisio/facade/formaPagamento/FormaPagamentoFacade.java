package com.fisio.facade.formaPagamento;

import com.fisio.framework.facade.Facade;
import com.fisio.framework.formaPagamento.FormaPagamento;

public interface FormaPagamentoFacade extends Facade<FormaPagamento>{

}
