package com.fisio.facade.pessoa;

import java.util.List;

import com.fisio.framework.facade.Facade;
import com.fisio.model.pessoa.Pessoa;

public interface PessoaFacade extends Facade<Pessoa>{

	List<Pessoa> consultarAniversariante();
}
