package com.fisio.facade.pessoa.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.pessoa.PessoaFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.pessoa.Pessoa;
import com.fisio.service.pessoa.PessoaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PessoaFacadeImpl extends AbstractFacade<Pessoa> implements PessoaFacade{

	private PessoaService pessoaService;

	@Autowired
	public PessoaFacadeImpl(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	
	@Override
	protected Service<Pessoa> getService() {
		return this.pessoaService;
	}

	@Override
	public List<Pessoa> consultarAniversariante() {
		return this.pessoaService.consultarAniversariante();
	}
}
