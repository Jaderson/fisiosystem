package com.fisio.facade.profissao;

import com.fisio.framework.facade.Facade;
import com.fisio.framework.profissao.Profissao;

public interface ProfissaoFacade extends Facade<Profissao>{

}
