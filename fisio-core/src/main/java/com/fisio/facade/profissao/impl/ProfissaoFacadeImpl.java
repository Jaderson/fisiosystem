package com.fisio.facade.profissao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.profissao.ProfissaoFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.profissao.Profissao;
import com.fisio.framework.service.Service;
import com.fisio.service.profissao.ProfissaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class ProfissaoFacadeImpl extends AbstractFacade<Profissao> implements ProfissaoFacade{

	private ProfissaoService profissoesService;

	@Autowired
	public ProfissaoFacadeImpl(ProfissaoService profissoesService) {
		this.profissoesService = profissoesService;
	}
	
	@Override
	protected Service<Profissao> getService() {
		return this.profissoesService;
	}

}
