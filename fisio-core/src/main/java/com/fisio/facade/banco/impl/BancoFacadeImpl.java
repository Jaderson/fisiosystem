package com.fisio.facade.banco.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.banco.BancoFacade;
import com.fisio.framework.banco.Banco;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.banco.BancoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class BancoFacadeImpl extends AbstractFacade<Banco> implements BancoFacade{

	private BancoService bancoService;

	@Autowired
	public BancoFacadeImpl(BancoService bancoService) {
		this.bancoService = bancoService;
	}
	
	@Override
	protected Service<Banco> getService() {
		return this.bancoService;
	}

}
