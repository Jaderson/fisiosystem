package com.fisio.facade.banco;

import com.fisio.framework.banco.Banco;
import com.fisio.framework.facade.Facade;

public interface BancoFacade extends Facade<Banco>{

}
