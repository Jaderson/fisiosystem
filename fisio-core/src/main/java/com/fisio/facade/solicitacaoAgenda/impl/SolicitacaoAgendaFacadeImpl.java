package com.fisio.facade.solicitacaoAgenda.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.solicitacaoAgenda.SolicitacaoAgendaFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.framework.solicitacaoAgenda.SolicitacaoAgenda;
import com.fisio.service.solicitacaoAgenda.SolicitacaoAgendaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class SolicitacaoAgendaFacadeImpl extends AbstractFacade<SolicitacaoAgenda> implements SolicitacaoAgendaFacade{

	private SolicitacaoAgendaService solicitacaoAgendaService;

	@Autowired
	public SolicitacaoAgendaFacadeImpl(SolicitacaoAgendaService solicitacaoAgendaService) {
		this.solicitacaoAgendaService = solicitacaoAgendaService;
	}
	
	@Override
	protected Service<SolicitacaoAgenda> getService() {
		return this.solicitacaoAgendaService;
	}

}
