package com.fisio.facade.solicitacaoAgenda;

import com.fisio.framework.facade.Facade;
import com.fisio.framework.solicitacaoAgenda.SolicitacaoAgenda;

public interface SolicitacaoAgendaFacade extends Facade<SolicitacaoAgenda>{

}
