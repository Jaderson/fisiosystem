package com.fisio.facade.palmilha.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.palmilha.PalmilhaFacade;
import com.fisio.framework.avaliacoes.Palmilha;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.palmilha.PalmilhaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PalmilhaFacadeImpl extends AbstractFacade<Palmilha> implements PalmilhaFacade{

	private PalmilhaService palmilhaService;

	@Autowired
	public PalmilhaFacadeImpl(PalmilhaService palmilhaService) {
		this.palmilhaService = palmilhaService;
	}
	
	@Override
	protected Service<Palmilha> getService() {
		return this.palmilhaService;
	}

}
