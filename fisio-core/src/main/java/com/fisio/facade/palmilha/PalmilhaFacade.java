package com.fisio.facade.palmilha;

import com.fisio.framework.avaliacoes.Palmilha;
import com.fisio.framework.facade.Facade;

public interface PalmilhaFacade extends Facade<Palmilha>{

}
