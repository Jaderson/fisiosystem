package com.fisio.facade.especialidade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.especialidade.EspecialidadeFacade;
import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.especialidade.EspecialidadeService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class EspecialidadeFacadeImpl extends AbstractFacade<Especialidade> implements EspecialidadeFacade{

	private EspecialidadeService especialidadeService;

	@Autowired
	public EspecialidadeFacadeImpl(EspecialidadeService especialidadeService) {
		this.especialidadeService = especialidadeService;
	}
	
	@Override
	protected Service<Especialidade> getService() {
		return this.especialidadeService;
	}

}
