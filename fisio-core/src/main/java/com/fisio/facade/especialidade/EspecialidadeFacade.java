package com.fisio.facade.especialidade;

import com.fisio.framework.especialidade.Especialidade;
import com.fisio.framework.facade.Facade;

public interface EspecialidadeFacade extends Facade<Especialidade>{

}
