package com.fisio.facade.acao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.acao.AcaoFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Acao;
import com.fisio.service.acao.AcaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class AcaoFacadeImpl extends AbstractFacade<Acao> implements AcaoFacade{

	private AcaoService acaoService;

	@Autowired
	public AcaoFacadeImpl(AcaoService acaoService) {
		this.acaoService = acaoService;
	}
	
	@Override
	protected Service<Acao> getService() {
		return this.acaoService;
	}

}
