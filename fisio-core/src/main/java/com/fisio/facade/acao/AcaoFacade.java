package com.fisio.facade.acao;

import com.fisio.framework.facade.Facade;
import com.fisio.model.autenticacao.Acao;

public interface AcaoFacade extends Facade<Acao>{

}
