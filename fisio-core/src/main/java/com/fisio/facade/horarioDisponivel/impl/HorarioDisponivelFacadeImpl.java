package com.fisio.facade.horarioDisponivel.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.horarioDisponivel.HorarioDisponivelFacade;
import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.horarioDisponivel.HorarioDisponivelService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class HorarioDisponivelFacadeImpl extends AbstractFacade<HorarioDisponivel> implements HorarioDisponivelFacade{

	private HorarioDisponivelService horarioDisponivelService;

	@Autowired
	public HorarioDisponivelFacadeImpl(HorarioDisponivelService horarioDisponivelService) {
		this.horarioDisponivelService = horarioDisponivelService;
	}
	
	@Override
	protected Service<HorarioDisponivel> getService() {
		return this.horarioDisponivelService;
	}

}
