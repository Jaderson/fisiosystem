package com.fisio.facade.horarioDisponivel;

import com.fisio.framework.agenda.HorarioDisponivel;
import com.fisio.framework.facade.Facade;

public interface HorarioDisponivelFacade extends Facade<HorarioDisponivel>{

}
