package com.fisio.facade.permissao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.permissao.PermissaoFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Permissao;
import com.fisio.service.permissao.PermissaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PermissaoFacadeImpl extends AbstractFacade<Permissao> implements PermissaoFacade{

	private PermissaoService permissaoService;

	@Autowired
	public PermissaoFacadeImpl(PermissaoService permissaoService) {
		this.permissaoService = permissaoService;
	}
	
	@Override
	protected Service<Permissao> getService() {
		return this.permissaoService;
	}

}
