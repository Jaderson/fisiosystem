package com.fisio.facade.permissao;

import com.fisio.framework.facade.Facade;
import com.fisio.model.autenticacao.Permissao;

public interface PermissaoFacade extends Facade<Permissao>{

}
