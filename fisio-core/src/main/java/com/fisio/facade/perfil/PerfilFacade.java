package com.fisio.facade.perfil;

import com.fisio.framework.facade.Facade;
import com.fisio.model.autenticacao.Perfil;

public interface PerfilFacade extends Facade<Perfil>{

}
