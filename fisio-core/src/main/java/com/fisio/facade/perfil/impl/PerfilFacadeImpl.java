package com.fisio.facade.perfil.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.perfil.PerfilFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Perfil;
import com.fisio.service.perfil.PerfilService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class PerfilFacadeImpl extends AbstractFacade<Perfil> implements PerfilFacade{

	private PerfilService perfilService;

	@Autowired
	public PerfilFacadeImpl(PerfilService perfilService) {
		this.perfilService = perfilService;
	}
	
	@Override
	protected Service<Perfil> getService() {
		return this.perfilService;
	}

}
