package com.fisio.facade.usuario;

import com.fisio.framework.facade.Facade;
import com.fisio.model.autenticacao.Usuario;

public interface UsuarioFacade extends Facade<Usuario>{

}
