package com.fisio.facade.usuario.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.usuario.UsuarioFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Usuario;
import com.fisio.service.usuario.UsuarioService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class UsuarioFacadeImpl extends AbstractFacade<Usuario> implements UsuarioFacade{

	private UsuarioService usuarioService;

	@Autowired
	public UsuarioFacadeImpl(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	protected Service<Usuario> getService() {
		return this.usuarioService;
	}

}
