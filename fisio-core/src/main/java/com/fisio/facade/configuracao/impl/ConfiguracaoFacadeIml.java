package com.fisio.facade.configuracao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.configuracao.ConfiguracaoFacade;
import com.fisio.framework.configuracao.Configuracao;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.configuracao.ConfiguracaoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class ConfiguracaoFacadeIml extends AbstractFacade<Configuracao> implements ConfiguracaoFacade{
	
	private ConfiguracaoService configuracaoService;

	@Autowired
	public ConfiguracaoFacadeIml(ConfiguracaoService configuracaoService) {
		this.configuracaoService = configuracaoService;
	}

	@Override
	protected Service<Configuracao> getService() {
		return this.configuracaoService;
	}

	@Override
	public List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int verificarDiaSemanaPorData, Integer idEmpresa) {
		return this.configuracaoService.consultarPessoaPorConfiguracao(isPesquisa, verificarDiaSemanaPorData, idEmpresa);
	}

}
