package com.fisio.facade.configuracao;

import java.util.List;

import com.fisio.framework.configuracao.Configuracao;
import com.fisio.framework.facade.Facade;

public interface ConfiguracaoFacade extends Facade<Configuracao>{

	List<Configuracao> consultarPessoaPorConfiguracao(boolean isPesquisa, int verificarDiaSemanaPorData, Integer idEmpresa);

}
