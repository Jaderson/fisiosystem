package com.fisio.facade.agenda;

import java.util.Date;
import java.util.List;

import com.fisio.framework.agenda.Agenda;
import com.fisio.framework.facade.Facade;

public interface AgendaFacade extends Facade<Agenda>{

	List<Agenda> consultarFaltaMes(Date pegarPrimerioDataMesCorrente,
			Date pegarUltimaDataMesCorrente);

	List<Object[]> consultaTipoEspecialidade(Date pegarPrimerioDataMesCorrente, Date pegarUltimaDataMesCorrente, Integer idEmpresass);

}
