package com.fisio.facade.objeto;

import com.fisio.framework.facade.Facade;
import com.fisio.model.autenticacao.Objeto;

public interface ObjetoFacade extends Facade<Objeto>{

}
