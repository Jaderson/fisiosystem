package com.fisio.facade.objeto.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.objeto.ObjetoFacade;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.model.autenticacao.Objeto;
import com.fisio.service.objeto.ObjetoService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class ObjetoFacadeImpl extends AbstractFacade<Objeto> implements ObjetoFacade{

	private ObjetoService objetoService;

	@Autowired
	public ObjetoFacadeImpl(ObjetoService objetoService) {
		this.objetoService = objetoService;
	}
	
	@Override
	protected Service<Objeto> getService() {
		return this.objetoService;
	}

}
