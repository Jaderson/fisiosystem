package com.fisio.facade.fluxoCaixa;

import java.math.BigDecimal;
import java.util.Date;

import com.fisio.framework.caixa.FluxoCaixa;
import com.fisio.framework.caixa.TipoLancamento;
import com.fisio.framework.facade.Facade;

public interface FluxoCaixaFacade extends Facade<FluxoCaixa>{

	BigDecimal consultarDataLancamento(Date geraDataComPrimeiroDia,Date geraDataComUltimoDia, Integer idEmpresa, TipoLancamento tipoLancamento);

}
