package com.fisio.facade.biomecanicaPalmilha;

import com.fisio.framework.avaliacoes.BiomecanicaPalmilha;
import com.fisio.framework.facade.Facade;

public interface BiomecanicaPalmilhaFacade extends Facade<BiomecanicaPalmilha>{

}
