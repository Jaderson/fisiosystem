package com.fisio.facade.biomecanicaPalmilha.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fisio.facade.biomecanicaPalmilha.BiomecanicaPalmilhaFacade;
import com.fisio.framework.avaliacoes.BiomecanicaPalmilha;
import com.fisio.framework.facade.impl.AbstractFacade;
import com.fisio.framework.service.Service;
import com.fisio.service.biomecanicaPalmilha.BiomecanicaPalmilhaService;

@Component
@Transactional(propagation = Propagation.REQUIRED)
public class BiomecanicaPalmilhaFacadeImpl extends AbstractFacade<BiomecanicaPalmilha> implements BiomecanicaPalmilhaFacade{

	private BiomecanicaPalmilhaService biomecanicaPalmilhaService;

	@Autowired
	public BiomecanicaPalmilhaFacadeImpl(BiomecanicaPalmilhaService biomecanicaPalmilhaService) {
		this.biomecanicaPalmilhaService = biomecanicaPalmilhaService;
	}
	
	@Override
	protected Service<BiomecanicaPalmilha> getService() {
		return this.biomecanicaPalmilhaService;
	}

}
