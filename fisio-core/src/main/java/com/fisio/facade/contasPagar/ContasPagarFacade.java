package com.fisio.facade.contasPagar;

import com.fisio.framework.contasPagar.ContasPagar;
import com.fisio.framework.facade.Facade;

public interface ContasPagarFacade extends Facade<ContasPagar>{

	void excluirContasParceladas(ContasPagar entidade);

}
