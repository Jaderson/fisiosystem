package com.fisio.framework.consulta;

public enum TipoOrderBy {

	ASC, DESC;
}
