package com.fisio.framework.consulta.restricao;

import static com.fisio.framework.consulta.Consulta.DOIS_PONTOS;

public class RestricaoComParametro extends AbstractRestricao{

	@Override
	public void montarRetricaoParticular(Restricoes restricoes) {
		restricoes.seAtributoCompostoRetiraPonto();
		getSelect().append(DOIS_PONTOS)
		   .append(restricoes.getNome());
	}

}
