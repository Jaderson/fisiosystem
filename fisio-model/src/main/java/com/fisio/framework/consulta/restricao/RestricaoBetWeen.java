package com.fisio.framework.consulta.restricao;

import com.fisio.framework.consulta.TipoConectivo;

import static com.fisio.framework.consulta.Consulta.*;

public class RestricaoBetWeen extends AbstractRestricao{
	
	@Override
	public void montarRetricaoParticular(Restricoes restricoes) {
		restricoes.seAtributoCompostoRetiraPonto();
		getSelect().append(DOIS_PONTOS)
		.append(restricoes.getNome()+INICIAL)
        .append(ESPACO)
        .append(TipoConectivo.AND.toString())
        .append(ESPACO)
        .append(DOIS_PONTOS)
		.append(restricoes.getNome()+FINAL);
	}
}
