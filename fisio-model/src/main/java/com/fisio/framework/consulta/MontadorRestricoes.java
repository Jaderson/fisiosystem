package com.fisio.framework.consulta;

import com.fisio.framework.consulta.restricao.Restricoes;

public interface MontadorRestricoes {

	String montarRestricao(Restricoes restricoes);
	
}
