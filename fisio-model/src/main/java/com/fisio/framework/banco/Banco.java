package com.fisio.framework.banco;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fisio.framework.contexto.Alterar;
import com.fisio.framework.contexto.Cadastrar;
import com.fisio.model.annotations.relatorio.Relatorio;
import com.fisio.model.empresa.Empresa;
import com.fisio.model.empresa.SetadorEmpresa;
import com.fisio.model.identidade.Identidade;
import com.fisio.model.ordenacao.Ordenacao;

@Entity
@Audited
@Table(name = "banco")
public class Banco implements Serializable, Identidade, Ordenacao, SetadorEmpresa {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String descricao;
	private BigDecimal saldo = BigDecimal.ZERO;
	private Empresa empresa;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="descricao", nullable=false)
	@NotNull(message = "erro_descricao_nao_pode_ser_nula", groups = {Cadastrar.class, Alterar.class})
	@Relatorio(name="descricao", tamanho=250, titulo="Nome", ordenacao=1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Column(name="saldo")
	@Relatorio(name="saldo", tamanho=250, titulo="Saldo", ordenacao=2)
	public BigDecimal getSaldo() {
		return saldo;
	}
	
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	@ManyToOne
	@JoinColumn(name = "empresa_id", nullable = false)
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Transient
	@Override
	public String getCampoOrderBy() {
		return "id";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Banco))
			return false;
		Banco other = (Banco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
